ZouriT
======

ZouriT est une application permettant de gérer les utilisateurs et groupes de différents services libres.

Les services actuellement couverts sont :

* Zimbra
* Nextcloud
* Jitsi Meet
* Etherpad (en développement)

## Pré-requis

Avant de pouvoir utiliser ZouriT, vous devez avoir installé les briques suivantes :

* un serveur Ldap (*Openldap* par exemple) avec les schémas `zourit-ldap-schema` inclus
* un serveur *Zimbra*
* un serveur *Nextcloud*

Et éventuellement :

* un serveur jitsi-meet
* un serveur etherpad

Vous devez avoir un environnement ruby. 
Nous vous conseillons d'utiliser un manager tel que rvm : https://rvm.io/

## Installation

voir documentation pour [developpeur](doc/developers.md)

