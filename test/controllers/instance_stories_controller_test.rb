require 'test_helper'

class InstanceStoriesControllerTest < ActionController::TestCase
  setup do
    @instance_story = instance_stories(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:instance_stories)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create instance_story" do
    assert_difference('InstanceStory.count') do
      post :create, instance_story: { end_date: @instance_story.end_date, message: @instance_story.message, start_date: @instance_story.start_date }
    end

    assert_redirected_to instance_story_path(assigns(:instance_story))
  end

  test "should show instance_story" do
    get :show, id: @instance_story
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @instance_story
    assert_response :success
  end

  test "should update instance_story" do
    patch :update, id: @instance_story, instance_story: { end_date: @instance_story.end_date, message: @instance_story.message, start_date: @instance_story.start_date }
    assert_redirected_to instance_story_path(assigns(:instance_story))
  end

  test "should destroy instance_story" do
    assert_difference('InstanceStory.count', -1) do
      delete :destroy, id: @instance_story
    end

    assert_redirected_to instance_stories_path
  end
end
