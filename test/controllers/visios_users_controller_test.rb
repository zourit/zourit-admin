require 'test_helper'

class VisiosUsersControllerTest < ActionController::TestCase
  setup do
    @visios_user = visios_users(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:visios_users)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create visios_user" do
    assert_difference('VisiosUser.count') do
      post :create, visios_user: { mail: @visios_user.mail, visio_id: @visios_user.visio_id }
    end

    assert_redirected_to visios_user_path(assigns(:visios_user))
  end

  test "should show visios_user" do
    get :show, id: @visios_user
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @visios_user
    assert_response :success
  end

  test "should update visios_user" do
    patch :update, id: @visios_user, visios_user: { mail: @visios_user.mail, visio_id: @visios_user.visio_id }
    assert_redirected_to visios_user_path(assigns(:visios_user))
  end

  test "should destroy visios_user" do
    assert_difference('VisiosUser.count', -1) do
      delete :destroy, id: @visios_user
    end

    assert_redirected_to visios_users_path
  end
end
