require 'test_helper'

class VisiosGroupsControllerTest < ActionController::TestCase
  setup do
    @visios_group = visios_groups(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:visios_groups)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create visios_group" do
    assert_difference('VisiosGroup.count') do
      post :create, visios_group: { group_id: @visios_group.group_id, visio_id: @visios_group.visio_id }
    end

    assert_redirected_to visios_group_path(assigns(:visios_group))
  end

  test "should show visios_group" do
    get :show, id: @visios_group
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @visios_group
    assert_response :success
  end

  test "should update visios_group" do
    patch :update, id: @visios_group, visios_group: { group_id: @visios_group.group_id, visio_id: @visios_group.visio_id }
    assert_redirected_to visios_group_path(assigns(:visios_group))
  end

  test "should destroy visios_group" do
    assert_difference('VisiosGroup.count', -1) do
      delete :destroy, id: @visios_group
    end

    assert_redirected_to visios_groups_path
  end
end
