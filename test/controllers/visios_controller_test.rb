require 'test_helper'

class VisiosControllerTest < ActionController::TestCase
  setup do
    @visio = visios(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:visios)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create visio" do
    assert_difference('Visio.count') do
      post :create, visio: { comment: @visio.comment, date_start: @visio.date_start, duration: @visio.duration, name: @visio.name, number_of_participants: @visio.number_of_participants, owner_id: @visio.owner_id }
    end

    assert_redirected_to visio_path(assigns(:visio))
  end

  test "should show visio" do
    get :show, id: @visio
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @visio
    assert_response :success
  end

  test "should update visio" do
    patch :update, id: @visio, visio: { comment: @visio.comment, date_start: @visio.date_start, duration: @visio.duration, name: @visio.name, number_of_participants: @visio.number_of_participants, owner_id: @visio.owner_id }
    assert_redirected_to visio_path(assigns(:visio))
  end

  test "should destroy visio" do
    assert_difference('Visio.count', -1) do
      delete :destroy, id: @visio
    end

    assert_redirected_to visios_path
  end
end
