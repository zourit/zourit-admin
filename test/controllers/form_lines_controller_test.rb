require 'test_helper'

class FormLinesControllerTest < ActionController::TestCase
  setup do
    @form_line = form_lines(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:form_lines)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create form_line" do
    assert_difference('FormLine.count') do
      post :create, form_line: { form_id: @form_line.form_id, order: @form_line.order, type: @form_line.type, value: @form_line.value }
    end

    assert_redirected_to form_line_path(assigns(:form_line))
  end

  test "should show form_line" do
    get :show, id: @form_line
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @form_line
    assert_response :success
  end

  test "should update form_line" do
    patch :update, id: @form_line, form_line: { form_id: @form_line.form_id, order: @form_line.order, type: @form_line.type, value: @form_line.value }
    assert_redirected_to form_line_path(assigns(:form_line))
  end

  test "should destroy form_line" do
    assert_difference('FormLine.count', -1) do
      delete :destroy, id: @form_line
    end

    assert_redirected_to form_lines_path
  end
end
