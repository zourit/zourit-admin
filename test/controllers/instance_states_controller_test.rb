require 'test_helper'

class InstanceStatesControllerTest < ActionController::TestCase
  setup do
    @instance_state = instance_states(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:instance_states)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create instance_state" do
    assert_difference('InstanceState.count') do
      post :create, instance_state: { importance: @instance_state.importance, title: @instance_state.title }
    end

    assert_redirected_to instance_state_path(assigns(:instance_state))
  end

  test "should show instance_state" do
    get :show, id: @instance_state
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @instance_state
    assert_response :success
  end

  test "should update instance_state" do
    patch :update, id: @instance_state, instance_state: { importance: @instance_state.importance, title: @instance_state.title }
    assert_redirected_to instance_state_path(assigns(:instance_state))
  end

  test "should destroy instance_state" do
    assert_difference('InstanceState.count', -1) do
      delete :destroy, id: @instance_state
    end

    assert_redirected_to instance_states_path
  end
end
