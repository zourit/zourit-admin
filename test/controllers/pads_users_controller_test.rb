require 'test_helper'

class PadsUsersControllerTest < ActionController::TestCase
  setup do
    @pads_user = pads_users(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:pads_users)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create pads_user" do
    assert_difference('PadsUser.count') do
      post :create, pads_user: { pad_id: @pads_user.pad_id, user_id: @pads_user.user_id }
    end

    assert_redirected_to pads_user_path(assigns(:pads_user))
  end

  test "should show pads_user" do
    get :show, id: @pads_user
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @pads_user
    assert_response :success
  end

  test "should update pads_user" do
    patch :update, id: @pads_user, pads_user: { pad_id: @pads_user.pad_id, user_id: @pads_user.user_id }
    assert_redirected_to pads_user_path(assigns(:pads_user))
  end

  test "should destroy pads_user" do
    assert_difference('PadsUser.count', -1) do
      delete :destroy, id: @pads_user
    end

    assert_redirected_to pads_users_path
  end
end
