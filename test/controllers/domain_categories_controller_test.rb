require 'test_helper'

class DomainCategoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @domain_category = domain_categories(:one)
  end

  test "should get index" do
    get domain_categories_url
    assert_response :success
  end

  test "should get new" do
    get new_domain_category_url
    assert_response :success
  end

  test "should create domain_category" do
    assert_difference('DomainCategory.count') do
      post domain_categories_url, params: { domain_category: { category: @domain_category.category } }
    end

    assert_redirected_to domain_category_url(DomainCategory.last)
  end

  test "should show domain_category" do
    get domain_category_url(@domain_category)
    assert_response :success
  end

  test "should get edit" do
    get edit_domain_category_url(@domain_category)
    assert_response :success
  end

  test "should update domain_category" do
    patch domain_category_url(@domain_category), params: { domain_category: { category: @domain_category.category } }
    assert_redirected_to domain_category_url(@domain_category)
  end

  test "should destroy domain_category" do
    assert_difference('DomainCategory.count', -1) do
      delete domain_category_url(@domain_category)
    end

    assert_redirected_to domain_categories_url
  end
end
