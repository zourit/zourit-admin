require 'test_helper'

class PadsGroupsControllerTest < ActionController::TestCase
  setup do
    @pads_group = pads_groups(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:pads_groups)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create pads_group" do
    assert_difference('PadsGroup.count') do
      post :create, pads_group: { group_id: @pads_group.group_id, pad_id: @pads_group.pad_id }
    end

    assert_redirected_to pads_group_path(assigns(:pads_group))
  end

  test "should show pads_group" do
    get :show, id: @pads_group
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @pads_group
    assert_response :success
  end

  test "should update pads_group" do
    patch :update, id: @pads_group, pads_group: { group_id: @pads_group.group_id, pad_id: @pads_group.pad_id }
    assert_redirected_to pads_group_path(assigns(:pads_group))
  end

  test "should destroy pads_group" do
    assert_difference('PadsGroup.count', -1) do
      delete :destroy, id: @pads_group
    end

    assert_redirected_to pads_groups_path
  end
end
