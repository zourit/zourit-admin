require 'test_helper'

class FormlinesControllerTest < ActionController::TestCase
  setup do
    @formline = formlines(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:formlines)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create formline" do
    assert_difference('Formline.count') do
      post :create, formline: { description: @formline.description, form_id: @formline.form_id, order: @formline.order, show_title: @formline.show_title, title: @formline.title, type: @formline.type, value: @formline.value }
    end

    assert_redirected_to formline_path(assigns(:formline))
  end

  test "should show formline" do
    get :show, id: @formline
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @formline
    assert_response :success
  end

  test "should update formline" do
    patch :update, id: @formline, formline: { description: @formline.description, form_id: @formline.form_id, order: @formline.order, show_title: @formline.show_title, title: @formline.title, type: @formline.type, value: @formline.value }
    assert_redirected_to formline_path(assigns(:formline))
  end

  test "should destroy formline" do
    assert_difference('Formline.count', -1) do
      delete :destroy, id: @formline
    end

    assert_redirected_to formlines_path
  end
end
