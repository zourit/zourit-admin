require "application_system_test_case"

class DomainCategoriesTest < ApplicationSystemTestCase
  setup do
    @domain_category = domain_categories(:one)
  end

  test "visiting the index" do
    visit domain_categories_url
    assert_selector "h1", text: "Domain Categories"
  end

  test "creating a Domain category" do
    visit domain_categories_url
    click_on "New Domain Category"

    fill_in "Category", with: @domain_category.category
    click_on "Create Domain category"

    assert_text "Domain category was successfully created"
    click_on "Back"
  end

  test "updating a Domain category" do
    visit domain_categories_url
    click_on "Edit", match: :first

    fill_in "Category", with: @domain_category.category
    click_on "Update Domain category"

    assert_text "Domain category was successfully updated"
    click_on "Back"
  end

  test "destroying a Domain category" do
    visit domain_categories_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Domain category was successfully destroyed"
  end
end
