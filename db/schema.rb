# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2025_01_14_082730) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "config_modules", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "title"
    t.string "icon"
    t.text "resume"
    t.boolean "active", default: false
    t.string "url"
    t.string "url_local"
    t.boolean "new_page", default: false
    t.boolean "beta", default: false
    t.string "font"
    t.integer "order", default: 0
    t.string "domain_datas"
    t.boolean "core"
    t.string "instance_datas"
    t.string "user_datas"
    t.string "color"
    t.boolean "data_loss_on_deactivation", default: true
    t.boolean "new_window", default: false
  end

  create_table "domain_categories", force: :cascade do |t|
    t.string "category"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "domain_details", id: :serial, force: :cascade do |t|
    t.string "domain_id"
    t.string "full_name"
    t.text "address"
    t.string "phone"
    t.date "start_date"
    t.boolean "debtor", default: false
    t.string "country"
    t.string "contact"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "email"
    t.text "comment"
    t.string "bill_to"
    t.string "billing_address"
    t.bigint "domain_category_id"
    t.index ["domain_category_id"], name: "index_domain_details_on_domain_category_id"
    t.index ["domain_id"], name: "index_domain_details_on_domain_id", unique: true
  end

  create_table "domain_infos", id: :serial, force: :cascade do |t|
    t.string "domain_id"
    t.integer "config_module_id"
    t.boolean "active", default: true
    t.integer "instance_id"
    t.index ["config_module_id"], name: "index_domain_infos_on_config_module_id"
    t.index ["domain_id", "config_module_id"], name: "index_domain_infos_on_domain_id_and_config_module_id", unique: true
    t.index ["instance_id"], name: "index_domain_infos_on_instance_id"
  end

  create_table "domain_options", id: :serial, force: :cascade do |t|
    t.string "value", null: false
    t.integer "domain_info_id"
    t.integer "meta_option_id"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["domain_info_id"], name: "index_domain_options_on_domain_info_id"
    t.index ["meta_option_id"], name: "index_domain_options_on_meta_option_id"
  end

  create_table "events", id: :serial, force: :cascade do |t|
    t.integer "instance_id"
    t.integer "instance_state_id"
    t.string "message"
    t.datetime "start_date", precision: nil
    t.datetime "end_date", precision: nil
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.bigint "events_state_id", default: 3, null: false
    t.index ["events_state_id"], name: "index_events_on_events_state_id"
    t.index ["instance_id"], name: "index_events_on_instance_id"
    t.index ["instance_state_id"], name: "index_events_on_instance_state_id"
  end

  create_table "events_states", force: :cascade do |t|
    t.string "title"
  end

  create_table "formlines", id: :serial, force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.integer "form_id"
    t.string "field_type"
    t.text "value"
    t.integer "order"
    t.boolean "show_title"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["form_id"], name: "index_formlines_on_form_id"
  end

  create_table "forms", id: :serial, force: :cascade do |t|
    t.string "title"
    t.text "comment"
    t.string "owner_id"
    t.date "end_date"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "instance_options", id: :serial, force: :cascade do |t|
    t.string "value", null: false
    t.integer "instance_id"
    t.integer "meta_option_id"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["instance_id"], name: "index_instance_options_on_instance_id"
    t.index ["meta_option_id"], name: "index_instance_options_on_meta_option_id"
  end

  create_table "instance_states", id: :serial, force: :cascade do |t|
    t.string "title"
    t.string "importance"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "instances", id: :serial, force: :cascade do |t|
    t.integer "config_module_id"
    t.string "name"
    t.text "description"
    t.boolean "active", default: true
    t.string "datas"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.integer "instance_state_id", default: 1
    t.index ["config_module_id"], name: "index_instances_on_config_module_id"
    t.index ["instance_state_id"], name: "index_instances_on_instance_state_id"
  end

  create_table "invitation_groups", id: :serial, force: :cascade do |t|
    t.integer "module_id"
    t.string "group_id"
    t.string "module_tag"
  end

  create_table "invitation_users", id: :serial, force: :cascade do |t|
    t.integer "module_id"
    t.string "uuid"
    t.string "email"
    t.boolean "internal"
    t.string "module_tag"
  end

  create_table "links", id: :serial, force: :cascade do |t|
    t.string "domain_id"
    t.string "title"
    t.text "resume"
    t.string "url"
    t.string "font"
    t.string "color"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "symbol"
    t.string "user_id"
    t.index ["domain_id"], name: "index_links_on_domain_id"
  end

  create_table "memo_archives", force: :cascade do |t|
    t.bigint "memo_id", null: false
    t.string "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["memo_id"], name: "index_memo_archives_on_memo_id"
  end

  create_table "memos", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "comment"
    t.string "uuid"
    t.string "owner_id"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "messages", id: :serial, force: :cascade do |t|
    t.string "message", null: false
    t.string "user_id", null: false
    t.integer "module_id"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "module_tag"
    t.integer "event_id"
    t.index ["module_id"], name: "index_messages_on_module_id"
  end

  create_table "meta_options", id: :serial, force: :cascade do |t|
    t.string "field", null: false
    t.string "key", null: false
    t.string "description"
    t.string "field_type", null: false
    t.string "placeholder"
    t.string "default"
    t.boolean "mandatory"
    t.integer "config_module_id"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.boolean "computerized", default: false
    t.index ["config_module_id"], name: "index_meta_options_on_config_module_id"
  end

  create_table "pad_archives", force: :cascade do |t|
    t.bigint "pad_id", null: false
    t.string "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["pad_id"], name: "index_pad_archives_on_pad_id"
  end

  create_table "pads", id: :serial, force: :cascade do |t|
    t.string "name"
    t.text "comment"
    t.string "uuid"
    t.string "owner_id"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "full_url"
  end

  create_table "statistics", id: :serial, force: :cascade do |t|
    t.string "user_id"
    t.string "domain_id"
    t.string "service_name"
    t.string "instance_name"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "survey_answers", id: :serial, force: :cascade do |t|
    t.integer "survey_option_id"
    t.string "uuid"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "survey_options", id: :serial, force: :cascade do |t|
    t.integer "survey_id"
    t.string "choice"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "surveys", id: :serial, force: :cascade do |t|
    t.datetime "end_date", precision: nil
    t.string "question"
    t.string "comment"
    t.string "uuid"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "owner_id"
    t.boolean "allow_multiple"
    t.boolean "anonymous", default: false
  end

  create_table "ticket_categories", id: :serial, force: :cascade do |t|
    t.string "libelle"
    t.string "mail"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "ticket_statuses", id: :serial, force: :cascade do |t|
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "next_statuses", default: "[]"
  end

  create_table "tickets", id: :serial, force: :cascade do |t|
    t.string "user_id", null: false
    t.integer "ticket_status_id"
    t.integer "ticket_category_id"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "domain_id"
    t.integer "instance_id"
    t.string "title"
    t.index ["instance_id"], name: "index_tickets_on_instance_id"
    t.index ["ticket_category_id"], name: "index_tickets_on_ticket_category_id"
    t.index ["ticket_status_id"], name: "index_tickets_on_ticket_status_id"
  end

  create_table "user_infos", force: :cascade do |t|
    t.string "user_id"
    t.string "theme"
    t.string "locale"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "darkmode", default: false
  end

  create_table "user_options", force: :cascade do |t|
    t.string "user_id"
    t.bigint "meta_option_id", null: false
    t.string "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "domain_id"
    t.index ["meta_option_id"], name: "index_user_options_on_meta_option_id"
  end

  create_table "user_themes", force: :cascade do |t|
    t.string "user_id"
    t.string "theme"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "visio_archives", force: :cascade do |t|
    t.bigint "visio_id", null: false
    t.string "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["visio_id"], name: "index_visio_archives_on_visio_id"
  end

  create_table "visios", id: :serial, force: :cascade do |t|
    t.string "name"
    t.text "comment"
    t.string "owner_id"
    t.datetime "date_start", precision: nil
    t.integer "duration"
    t.integer "number_of_participants"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "uuid"
    t.string "friendly_id"
    t.boolean "allow_recording"
    t.boolean "anyone_can_start"
    t.boolean "mute_on_start"
    t.boolean "anyone_join_as_moderator"
    t.boolean "moderator_approval"
  end

  add_foreign_key "domain_details", "domain_categories"
  add_foreign_key "domain_infos", "config_modules"
  add_foreign_key "domain_infos", "instances"
  add_foreign_key "domain_options", "domain_infos"
  add_foreign_key "domain_options", "meta_options"
  add_foreign_key "events", "events_states"
  add_foreign_key "events", "instance_states"
  add_foreign_key "events", "instances"
  add_foreign_key "formlines", "forms"
  add_foreign_key "instance_options", "instances"
  add_foreign_key "instance_options", "meta_options"
  add_foreign_key "instances", "config_modules"
  add_foreign_key "instances", "instance_states"
  add_foreign_key "memo_archives", "memos"
  add_foreign_key "meta_options", "config_modules"
  add_foreign_key "pad_archives", "pads"
  add_foreign_key "tickets", "instances"
  add_foreign_key "tickets", "ticket_categories"
  add_foreign_key "tickets", "ticket_statuses"
  add_foreign_key "user_options", "meta_options"
  add_foreign_key "visio_archives", "visios"
end
