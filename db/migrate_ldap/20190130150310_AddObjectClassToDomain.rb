module AddObjectClassToDomain

  def change
	  include DomainsHelper
	  domains = list_domains(with_quota=false)
	  domains.each do |domain|
		  replace = { objectclass: ["top","organization","zouritDomain"] }
		  if update_domain domain.name, replace
			  puts "migration domain #{domain.name}"
		  end
	  end
  end

end
