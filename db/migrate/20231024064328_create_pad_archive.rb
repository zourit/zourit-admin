class CreatePadArchive < ActiveRecord::Migration[7.0]
  def change
    create_table :pad_archives do |t|
      t.references :pad, null: false, foreign_key: true
      t.string :user_id

      t.timestamps
    end
  end
end
