class DropDomainCategoryLine < ActiveRecord::Migration[6.0]
  def change
    drop_table :domain_category_lines
  end
end
