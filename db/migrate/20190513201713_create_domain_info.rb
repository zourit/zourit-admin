class CreateDomainInfo < ActiveRecord::Migration[4.2]
  def change
    create_table :domain_infos do |t|
      t.string :domain_id
      t.references :config_module, index: true, foreign_key: true
      t.string :data
    end
  end
end
