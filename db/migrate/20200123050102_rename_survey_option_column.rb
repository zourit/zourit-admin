class RenameSurveyOptionColumn < ActiveRecord::Migration[4.2]
  def change
    rename_column :survey_answers, :option_id, :survey_option_id
  end
end
