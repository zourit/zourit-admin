# frozen_string_literal: true

class CreateInstanceOption < ActiveRecord::Migration[4.2]
  def change
    create_table :instance_options do |t|
      t.string :key, null: false
      t.string :value, null: false

      t.references :instance, index: true, foreign_key: true
      t.references :meta_option, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
