class AddActiveToDomainInfo < ActiveRecord::Migration[4.2]
  def change
    add_column :domain_infos, :active, :boolean, default: true
  end
end
