class AddTitleEnToConfigModule < ActiveRecord::Migration[4.2]
  def change
    add_column :config_modules, :title_en, :string
    add_column :config_modules, :resume_en, :text
    rename_column :config_modules, :title, :title_fr
    rename_column :config_modules, :resume, :resume_fr
    mod = ConfigModule.where(name: 'Zimbra').first
    if mod
      mod.title_en = 'My emails'
      mod.resume_en = 'I consult my mails, my shared diaries, my contacts, my task in my Zimbra electronic mail service'
      mod.save
    end
    mod2 = ConfigModule.where(name: 'Nextcloud').first
    if mod2
      mod2.title_en = 'My files'
      mod2.resume_en = 'Thanks to Nextcloud service I can store files,find them and share them with my colleagues'
      mod2.save
    end
    mod3 = ConfigModule.where(name: 'EtherPAD').first
    if mod3
      mod3.title_en = 'My PADs'
      mod3.resume_en = "With this module I can create new PADs, list those I already created and see my colleagues shared PADs'"
      mod3.save
    end
    mod4 = ConfigModule.where(name: 'Framadate').first
    if mod4
      mod4.title_en = 'My polls'
      mod4.resume_en = "With this module I can create new polls date type or classic, now I can't see my polls list yet or invite my colleague to respond to them"
      mod4.save
    end
    mod5 = ConfigModule.where(name: 'Jitsi Meet').first
    if mod5
      mod5.title_en = 'My videoconferences'
      mod5.resume_en = 'With this module I can create videoconference rooms and invite my colleagues'
      mod5.save
    end
  end
end
