class CreateUserInfo < ActiveRecord::Migration[7.0]
  def change
    create_table :user_infos do |t|
      t.string :user_id
      t.string :theme
      t.string :locale

      t.timestamps
    end
  end
end
