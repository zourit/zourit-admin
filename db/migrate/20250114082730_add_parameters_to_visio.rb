class AddParametersToVisio < ActiveRecord::Migration[7.0]
  def change
    add_column :visios, :allow_recording, :boolean
    add_column :visios, :anyone_can_start, :boolean
    add_column :visios, :mute_on_start, :boolean
    add_column :visios, :anyone_join_as_moderator, :boolean
    add_column :visios, :moderator_approval, :boolean
  end
end
