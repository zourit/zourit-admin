class PopulateInstanceToTicket < ActiveRecord::Migration[4.2]
  def change
    domains_info = DomainInfo.all
    Ticket.all.each do |ticket|
      domains_info.each do |domain_info|
        if (ticket.config_module_id == domain_info.config_module_id) && (ticket.domain_id == domain_info.domain_id)
          ticket.instance_id = domain_info.instance_id
          ticket.save 
        end
      end  
    end
    Ticket.where(instance_id: nil).destroy_all
  end
end
