class DeleteEmptyInstancesFromDomainInfo < ActiveRecord::Migration[4.2]
  def change
    DomainInfo.all.each do |di|
      di.destroy unless di.instance_id
    end
  end
end
