class CreateOptions < ActiveRecord::Migration[4.2]
  def change
    create_table :options do |t|
      t.integer :survey_id
      t.string :choice

      t.timestamps null: false
    end
  end
end
