class AddUserToLinks < ActiveRecord::Migration[6.0]
  def change
    add_column :links, :user_id, :string
  end
end
