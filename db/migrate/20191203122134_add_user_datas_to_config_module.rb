class AddUserDatasToConfigModule < ActiveRecord::Migration[4.2]
  def change
    add_column :config_modules, :user_datas, :string
  end
end
