class AddFontToConfigModule < ActiveRecord::Migration[4.2]
  def change
    add_column :config_modules, :font, :string
  end
end
