class AddSymbolToLinks < ActiveRecord::Migration[4.2]
  def change
    add_column :links, :symbol, :string
  end
end
