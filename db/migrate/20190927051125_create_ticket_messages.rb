class CreateTicketMessages < ActiveRecord::Migration[4.2]
  def change
    create_table :ticket_messages do |t|
      t.string :message, null:false
      t.string :user_id, null:false
      t.references :ticket, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
