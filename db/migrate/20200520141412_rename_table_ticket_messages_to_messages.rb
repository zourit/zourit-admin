class RenameTableTicketMessagesToMessages < ActiveRecord::Migration[4.2]
  def change
    rename_table :ticket_messages, :messages
    rename_column :messages, :ticket_id, :module_id
  end
end
