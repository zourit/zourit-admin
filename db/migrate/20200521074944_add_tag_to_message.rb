class AddTagToMessage < ActiveRecord::Migration[4.2]
  def change
    add_column :messages, :module_tag, :string
    Message.all.each do |msg|
      msg.module_tag = 'ticket'
      msg.save
    end
  end
end
