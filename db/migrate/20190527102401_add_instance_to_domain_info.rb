class AddInstanceToDomainInfo < ActiveRecord::Migration[4.2]
  def change
    add_reference :domain_infos, :instance, index: true, foreign_key: true
  end
end
