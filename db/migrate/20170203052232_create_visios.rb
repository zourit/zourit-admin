class CreateVisios < ActiveRecord::Migration[4.2]
  def change
    create_table :visios do |t|
      t.string :name
      t.text :comment
      t.string :owner_id
      t.datetime :date_start
      t.integer :duration
      t.integer :number_of_participants

      t.timestamps null: false
    end
  end
end
