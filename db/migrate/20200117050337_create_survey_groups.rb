class CreateSurveyGroups < ActiveRecord::Migration[4.2]
  def change
    create_table :survey_groups do |t|
      t.string :group_id
      t.integer :survey_id

      t.timestamps null: false
    end
  end
end
