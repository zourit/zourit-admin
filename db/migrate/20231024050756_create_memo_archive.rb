class CreateMemoArchive < ActiveRecord::Migration[7.0]
  def change
    create_table :memo_archives do |t|
      t.references :memo, null: false, foreign_key: true
      t.string :user_id

      t.timestamps
    end
  end
end
