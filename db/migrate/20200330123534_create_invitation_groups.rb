class CreateInvitationGroups < ActiveRecord::Migration[4.2]
  def change
    create_table :invitation_groups do |t|
      t.integer :module_id
      t.string :group_id
      t.string :module_tag
    end
  end
end
