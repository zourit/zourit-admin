class CreateInstanceStates < ActiveRecord::Migration[4.2]
  def change
    create_table :instance_states do |t|
      t.string :title
      t.string :importance

      t.timestamps null: false
    end
    InstanceState.create(title: 'Opérationnel', importance: 'success')
    InstanceState.create(title: 'Panne partielle', importance: 'warning')
    InstanceState.create(title: 'Panne majeure', importance: 'danger')
  end
end
