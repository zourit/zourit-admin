class CreateVisioArchive < ActiveRecord::Migration[7.0]
  def change
    create_table :visio_archives do |t|
      t.references :visio, null: false, foreign_key: true
      t.string :user_id

      t.timestamps
    end
  end
end
