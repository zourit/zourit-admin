class CreateVisiosGroups < ActiveRecord::Migration[4.2]
  def change
    create_table :visios_groups do |t|
      t.string :group_id
      t.references :visio, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
