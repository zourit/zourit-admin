class RenameKindToTypeOfTableFromlines < ActiveRecord::Migration[6.0]
  def change
    rename_column :formlines, :kind, :field_type
  end
end
