class CreatePadsUsers < ActiveRecord::Migration[4.2]
  def change
    create_table :pads_users do |t|
      t.string :user_id
      t.references :pad, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
