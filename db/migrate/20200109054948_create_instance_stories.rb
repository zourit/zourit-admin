class CreateInstanceStories < ActiveRecord::Migration[4.2]
  def change
    create_table :instance_stories do |t|
      t.references :instance, index: true, foreign_key: true
      t.references :instance_state, index: true, foreign_key: true
      t.string :message
      t.datetime :start_date
      t.datetime :end_date

      t.timestamps null: false
    end
  end
end
