class CreateAnswers < ActiveRecord::Migration[4.2]
  def change
    create_table :answers do |t|
      t.integer :option_id
      t.string :uuid

      t.timestamps null: false
    end
  end
end
