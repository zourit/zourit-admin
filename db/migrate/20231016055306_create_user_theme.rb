class CreateUserTheme < ActiveRecord::Migration[7.0]
  def change
    create_table :user_themes do |t|
      t.string :user_id
      t.string :theme

      t.timestamps
    end
  end
end
