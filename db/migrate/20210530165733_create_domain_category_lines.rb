class CreateDomainCategoryLines < ActiveRecord::Migration[6.0]
  def change
    create_table :domain_category_lines do |t|
      t.string :domain_id
      t.references :domain_category, null: false, foreign_key: true
    end
  end
end
