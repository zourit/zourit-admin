class AddUniqueDomainAndConfigModuleToDomainInfos < ActiveRecord::Migration[7.0]
  def change
    add_index :domain_infos, %i[domain_id config_module_id], unique: true
  end
end
