class CreateDomainDetails < ActiveRecord::Migration[4.2]
  def change
    create_table :domain_details do |t|
      t.string :domain_id
      t.string :full_name
      t.text :address
      t.string :phone
      t.date :start_date
      t.boolean :debtor, default: false
      t.string :country
      t.string :contact

      t.timestamps null: false

      t.index :domain_id, unique: true
    end
  end
end
