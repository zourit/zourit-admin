class AddDomainToUserOption < ActiveRecord::Migration[6.0]
  def change
    add_column :user_options, :domain_id, :string
  end
end
