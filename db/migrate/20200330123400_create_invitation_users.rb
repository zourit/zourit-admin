class CreateInvitationUsers < ActiveRecord::Migration[4.2]
  def change
    create_table :invitation_users do |t|
      t.integer :module_id
      t.string :uuid
      t.string :email
      t.boolean :internal
      t.string :module_tag
    end
  end
end
