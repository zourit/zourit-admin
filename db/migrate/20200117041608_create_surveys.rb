class CreateSurveys < ActiveRecord::Migration[4.2]
  def change
    create_table :surveys do |t|
      t.datetime :end_date
      t.string :question
      t.string :description
      t.string :uuid

      t.timestamps null: false
    end
  end
end
