class PopulateComputerizedAuthKeyDomainOption < ActiveRecord::Migration[4.2]
  def up
    zimbra = ConfigModule.where(name: 'Zimbra').first
    raise('Zimbra ConfigModule not found') unless zimbra

    authkey_meta_option = MetaOption.where(config_module: zimbra, key: 'authkey').first
    unless authkey_meta_option
      puts 'No MetaOption domain for Zimbra found!'
      puts
      puts 'Hint: you should probably run: `rake modules:populate` now,'
      puts '      ...then reapply `rake db:migrate`'
      puts
      puts 'EXITING.'
      exit 2
    end

    DomainInfo.where(config_module: zimbra).each do |di|
      data = eval(di.data)
      DomainOption.create(
        domain_info: di,
        meta_option: authkey_meta_option,
        value: data[:authkey]
      )
    end

    change_table :domain_infos do |t|
      t.remove :data
    end
  end
end
