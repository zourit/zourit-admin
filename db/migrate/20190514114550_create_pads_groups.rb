class CreatePadsGroups < ActiveRecord::Migration[4.2]
  def change
    create_table :pads_groups do |t|
      t.string :group_id
      t.references :pad, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
