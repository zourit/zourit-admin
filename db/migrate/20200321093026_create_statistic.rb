class CreateStatistic < ActiveRecord::Migration[4.2]
  def change
    create_table :statistics do |t|
      t.string :user_id
      t.string :domain_id
      t.string :service_name
      t.string :instance_name

      t.timestamps null: false
    end
  end
end
