class CreateMemos < ActiveRecord::Migration[4.2]
  def change
    create_table :memos do |t|
      t.string :name
      t.string :comment
      t.string :uuid
      t.string :owner_id

      t.timestamps null: false
    end
  end
end
