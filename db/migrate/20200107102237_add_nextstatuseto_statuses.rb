class AddNextstatusetoStatuses < ActiveRecord::Migration[4.2]
  def change
    add_column :ticket_statuses, :next_statuses, :string
  end
end
