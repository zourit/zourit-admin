class AddDefaultNextstatusesToTicketStatuse < ActiveRecord::Migration[4.2]
  def change
    change_column :ticket_statuses, :next_statuses, :string, default: "[]"
  end
end
