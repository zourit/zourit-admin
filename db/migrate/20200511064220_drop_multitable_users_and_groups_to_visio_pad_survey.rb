class DropMultitableUsersAndGroupsToVisioPadSurvey < ActiveRecord::Migration[4.2]
  def change
    drop_table :visios_users
    drop_table :visios_groups
    drop_table :pads_users
    drop_table :pads_groups
    drop_table :survey_users
    drop_table :survey_groups
    rename_column :surveys, :description, :comment
  end
end
