class CreateConfigModule < ActiveRecord::Migration[4.2]
  def change
    create_table :config_modules do |t|
      t.string :name
      t.string :title
      t.string :icon
      t.text :resume
      t.boolean :active, default: false
      t.string :url
      t.string :url_local
      t.boolean :new_page, default: false
      t.boolean :beta, default: false
    end
    ConfigModule.create name: 'Zimbra',
                        title: 'Mes e-mails',
                        icon: 'fa-envelope',
                        resume: 'Je consulte mes mails, mes agendas partagés, mes contacts, mes tâches dans ma messagerie Zimbra',
                        new_page: true
    ConfigModule.create name: 'Nextcloud', title: 'Mes fichiers', icon: 'fa-cloud', resume: 'Grâce au service NextCloud je peux stocker des fichiers, les retrouver et les partager avec mes collègues', new_page: true
    ConfigModule.create name: 'EtherPAD', title: 'Mes PADs', icon: 'fa-file-text', resume: "Avec ce module, je peux créer de nouveaux PADs, lister ceux que j'ai déjà créé et voir les PADs que mes collègues partagent", beta: true, url_local: '/pads'
    ConfigModule.create name: 'Framadate', title: 'Mes Sondages', icon: 'fa-comments', resume: "Avec ce module, je peux créer de nouveaux sondages de type date ou classique... pour l'instant je ne peux pas encore voir la liste de mes sondages ou inviter mes collègues à y répondre", beta: true, url_local: '/dates'
    ConfigModule.create name: 'JitsiMeet', title: 'Mes Visioconférences', icon: 'fa-video-camera', resume: 'Avec ce module, je peux créer des salons de visioconférence et inviter mes collègues à y participer', url_local: '/visios'
  end
end
