class ModifyInstanceOption < ActiveRecord::Migration[4.2]
  def change
    change_table :instance_options do |t|
      t.remove :key
    end
  end
end
