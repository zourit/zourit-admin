class RemoveEmailToPadUSers < ActiveRecord::Migration[4.2]
  def change
    remove_column :pads_users, :user_id, :string
  end
end
