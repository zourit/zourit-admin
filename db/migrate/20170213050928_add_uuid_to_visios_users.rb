class AddUuidToVisiosUsers < ActiveRecord::Migration[4.2]
  def change
    add_column :visios_users, :uuid, :string
  end
end
