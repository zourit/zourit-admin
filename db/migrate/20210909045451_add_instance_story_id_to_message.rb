class AddInstanceStoryIdToMessage < ActiveRecord::Migration[6.0]
  def change
    add_column :messages, :instance_story_id, :integer
  end
end
