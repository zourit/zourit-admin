class AddPToPadUrl < ActiveRecord::Migration[4.2]
  include ApplicationHelper
  def change
    configmodule = ConfigModule.where(name: configmodule_name(Core::Etherpad)).first
    instances = Instance.where(config_module_id: configmodule.id)
    instances.each do |instance|
      instance.option_by_name('url')
      mo = MetaOption.instance_option(configmodule, 'url')
      io = instance.instance_options.where(instance_id: instance.id, meta_option_id: mo).first
      unless io.nil? || io.value.end_with?('/p')
        io.value += '/p'
        io.save
      end
    end
  end
end
