class PopulateDefaultValinNextStatusestoStatuses < ActiveRecord::Migration[4.2]
  def change
    annule = TicketStatus.where(libelle: 'annulé').first
    ferme = TicketStatus.where(libelle: 'fermé').first

    annule.next_statuses = "[]"
    ferme.next_statuses = "[]"
    annule.save
    ferme.save
  end
end
