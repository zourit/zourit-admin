class AddOrdreToConfigModule < ActiveRecord::Migration[4.2]
  def change
    add_column :config_modules, :order, :integer, default: 0
  end
end
