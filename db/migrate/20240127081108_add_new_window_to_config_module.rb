class AddNewWindowToConfigModule < ActiveRecord::Migration[7.0]
  def change
    add_column :config_modules, :new_window, :boolean, default: false
  end
end
