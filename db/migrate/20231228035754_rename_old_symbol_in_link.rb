class RenameOldSymbolInLink < ActiveRecord::Migration[7.0]
  def change
    Link.all.each do |link|
      if link.symbol.end_with?('-o')
        link.symbol = link.symbol.chop.chop
        link.save
      end
    end
  end
end
