class ChangeMessageInstanceStoryIdToEventId < ActiveRecord::Migration[6.0]
  def change
    rename_column :messages, :instance_story_id, :event_id
  end
  Message.where(module_tag: 'instance_story').each do |message|
    message.module_tag = 'event'
    message.save
  end
end
