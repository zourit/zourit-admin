class RemoveForeignKeyTicketToMessages < ActiveRecord::Migration[4.2]
  def change
    remove_foreign_key :messages, column: :module_id
  rescue
    puts "Table 'messages' has no foreign key for {:column=>:module_id}"
  end
end
