class DeleteFramadateInConfigModule < ActiveRecord::Migration[4.2]
  def change
    ConfigModule.where(name: 'Framadate').destroy_all
  end
end
