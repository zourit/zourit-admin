class DeleteEtherPadFromConfigModule < ActiveRecord::Migration[4.2]
  def change
    ConfigModule.where(name: 'EtherPAD').destroy_all
  end
end
