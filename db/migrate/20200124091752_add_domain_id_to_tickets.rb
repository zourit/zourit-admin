class AddDomainIdToTickets < ActiveRecord::Migration[4.2]
  def change
    add_column :tickets, :domain_id, :string
    @tickets = Ticket.all
    @tickets.each do |ticket|
      ticket.domain_id = ticket.user.domain.id
      ticket.save
    end
  end
end
