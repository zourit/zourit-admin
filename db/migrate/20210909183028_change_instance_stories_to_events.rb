class ChangeInstanceStoriesToEvents < ActiveRecord::Migration[6.0]
  def change
    rename_table :instance_stories, :events
  end
end
