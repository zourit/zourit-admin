class AddOwnerToSurvey < ActiveRecord::Migration[4.2]
  def change
    add_column :surveys, :owner_id, :string
  end
end
