class AddAnonynousToSurvey < ActiveRecord::Migration[4.2]
  def change
    add_column :surveys, :anonymous, :boolean, default: false
  end
end
