class DeleteLibelleAndActionInTicketStatus < ActiveRecord::Migration[4.2]
  def change
    remove_column :ticket_statuses, :libelle, :string
    remove_column :ticket_statuses, :action, :string
  end
end
