class CreateTicketCategories < ActiveRecord::Migration[4.2]
  def change
    create_table :ticket_categories do |t|
      t.string :libelle
      t.string :mail

      t.timestamps null: false
    end

    ["bug","demande d'amélioration"].each do |category|
      TicketCategory.create libelle: category
    end
  end
end
