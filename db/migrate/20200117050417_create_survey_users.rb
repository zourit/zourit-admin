class CreateSurveyUsers < ActiveRecord::Migration[4.2]
  def change
    create_table :survey_users do |t|
      t.integer :survey_id
      t.string :uuid
      t.string :mail

      t.timestamps null: false
    end
  end
end
