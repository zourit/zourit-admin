class CreateFormlines < ActiveRecord::Migration[4.2]
  def change
    create_table :formlines do |t|
      t.string :title
      t.text :description
      t.references :form, index: true, foreign_key: true
      t.string :kind
      t.text :value
      t.integer :order
      t.boolean :show_title

      t.timestamps null: false
    end
  end
end
