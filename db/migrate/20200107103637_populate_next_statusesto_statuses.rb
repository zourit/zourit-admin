class PopulateNextStatusestoStatuses < ActiveRecord::Migration[4.2]
  def change
    ouvert = TicketStatus.where(libelle: 'ouvert').first
    encours = TicketStatus.where(libelle: 'en cours').first
    annule = TicketStatus.where(libelle: 'annulé').first
    ferme = TicketStatus.where(libelle: 'fermé').first

    ouvert.next_statuses = "[#{encours.id} , #{annule.id}]"
    encours.next_statuses = "[#{ferme.id} , #{annule.id}]"
    ouvert.save
    encours.save
  end
end
