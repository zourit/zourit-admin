class AddBillToDomainDetail < ActiveRecord::Migration[6.0]
  def change
    add_column :domain_details, :bill_to, :string
    add_column :domain_details, :billing_address, :string
  end
end
