class RenameOldTableToNewTable < ActiveRecord::Migration[4.2]
  def change
    rename_table :options, :survey_options
    rename_table :answers, :survey_answers
    rename_column :survey_users, :mail, :email
  end
end
