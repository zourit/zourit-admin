class CreateVisiosUsers < ActiveRecord::Migration[4.2]
  def change
    create_table :visios_users do |t|
      t.string :mail
      t.references :visio, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
