class AddInfosToDomainDetail < ActiveRecord::Migration[4.2]
  def change
    add_column :domain_details, :email, :string
    add_column :domain_details, :comment, :text
  end
end
