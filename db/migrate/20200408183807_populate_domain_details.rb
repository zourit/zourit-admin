class PopulateDomainDetails < ActiveRecord::Migration[4.2]
  def change
    Domain.all(with_quota = false).each do |domain|
      puts "Création des détails pour le domaine #{domain.name} : #{domain.id}"
      DomainDetail.create domain_id: domain.id
    end
  end
end
