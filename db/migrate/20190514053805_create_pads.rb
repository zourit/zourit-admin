class CreatePads < ActiveRecord::Migration[4.2]
  def change
    create_table :pads do |t|
      t.string :name
      t.text :comment
      t.string :uuid
      t.string :owner_id

      t.timestamps null: false
    end
  end
end
