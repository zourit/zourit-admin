class ModifyInstanceStateTitle < ActiveRecord::Migration[6.0]
  def change
    { success: 'operational', warning: 'partial_breakdown', danger: 'major_breakdown' }.each_pair do |key, value|
      state = InstanceState.where(importance: key.to_s).first
      state.title = value
      state.save
    end
  end
end
