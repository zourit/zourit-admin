class PopulateInvitationUsers < ActiveRecord::Migration[4.2]
  def change
    @pads_users = PadsUser.all
    @visios_users = VisiosUser.all
    @surveys_users= SurveyUser.all
    
    @pads_users.each do |item| 
      InvitationUser.create(uuid: item.uuid,email: item.mail, module_id: item.pad_id, module_tag: "pad", internal: userinternal(item.uuid))
      
    end
    @visios_users.each do |item| 
      InvitationUser.create(uuid: item.uuid,email: item.mail, module_id: item.visio_id, module_tag: "visio", internal: userinternal(item.uuid))
      
    end
    @surveys_users.each do |item| 
      InvitationUser.create(uuid: item.uuid,email: item.email, module_id: item.survey_id, module_tag: "survey", internal: userinternal(item.uuid))
      
    end



  end

  def userinternal(uuid)
    internal = true
    if uuid.nil? or uuid.start_with?("zourit-")
      internal = false
    end
    internal
  end
end
