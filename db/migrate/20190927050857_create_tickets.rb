class CreateTickets < ActiveRecord::Migration[4.2]
  def change
    create_table :tickets do |t|
      t.string :user_id, null:false
      t.references :ticket_status, index: true, foreign_key: true
      t.references :ticket_category, index: true, foreign_key: true
      t.references :config_module, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
