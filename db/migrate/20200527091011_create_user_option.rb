class CreateUserOption < ActiveRecord::Migration[6.0]
  def change
    create_table :user_options do |t|
      t.string :user_id
      t.references :meta_option, null: false, foreign_key: true
      t.string :value

      t.timestamps null: false
    end
  end
end
