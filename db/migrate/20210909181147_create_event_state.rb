class CreateEventState < ActiveRecord::Migration[6.0]
  def change
    create_table :events_states do |t|
      t.string :title
    end
    EventsState.create title: 'planned'
    EventsState.create title: 'in_progress'
    EventsState.create title: 'completed'
  end
end
