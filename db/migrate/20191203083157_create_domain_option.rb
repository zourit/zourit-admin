class CreateDomainOption < ActiveRecord::Migration[4.2]
  def change
    create_table :domain_options do |t|
      t.string :value, null: false

      t.references :domain_info, index: true, foreign_key: true
      t.references :meta_option, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
