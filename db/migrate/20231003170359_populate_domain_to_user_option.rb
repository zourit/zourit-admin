class PopulateDomainToUserOption < ActiveRecord::Migration[7.0]
  def change
    UserOption.where(domain_id: nil).each do |uo|
      user = User.find(uo.user_id)
      next unless user

      uo.domain_id = user.domain.id
      uo.save
    end
  end
end
