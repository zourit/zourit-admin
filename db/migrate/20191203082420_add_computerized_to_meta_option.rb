class AddComputerizedToMetaOption < ActiveRecord::Migration[4.2]
  def change
    add_column :meta_options, :computerized, :boolean, default: false
  end
end
