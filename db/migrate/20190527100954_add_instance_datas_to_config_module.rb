class AddInstanceDatasToConfigModule < ActiveRecord::Migration[4.2]
  def change
    add_column :config_modules, :instance_datas, :string
    rename_column :config_modules, :datas, :domain_datas
  end
end
