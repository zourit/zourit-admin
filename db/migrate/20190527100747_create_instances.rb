class CreateInstances < ActiveRecord::Migration[4.2]
  def change
    create_table :instances do |t|
      t.references :config_module, index: true, foreign_key: true
      t.string :name
      t.text :description
      t.boolean :active, default: true
      t.string :datas

      t.timestamps null: false
    end
  end
end
