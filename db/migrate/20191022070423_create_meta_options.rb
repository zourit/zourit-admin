# frozen_string_literal: true

class CreateMetaOptions < ActiveRecord::Migration[4.2]
  def change
    create_table :meta_options do |t|
      t.string  :field, null: false
      t.string  :key, null: false
      t.string  :description
      t.string  :field_type, null: false
      t.string  :placeholder
      t.string  :default
      t.boolean :mandatory
      t.references :config_module, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
