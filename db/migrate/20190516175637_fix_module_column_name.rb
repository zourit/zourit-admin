class FixModuleColumnName < ActiveRecord::Migration[4.2]
  def change
    rename_column :config_modules, :title_fr, :title
    rename_column :config_modules, :resume_fr, :resume
    remove_column :config_modules, :title_en
    remove_column :config_modules, :resume_en
  end
end
