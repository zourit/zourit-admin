class AddEmailAndUuidToPadUSers < ActiveRecord::Migration[4.2]
  def change
    add_column :pads_users, :uuid, :string
    add_column :pads_users, :mail, :string
  end
end
