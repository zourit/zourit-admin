class AddFullUrlToPad < ActiveRecord::Migration[7.0]
  include ApplicationHelper
  def change
    add_column :pads, :full_url, :string

    Pad.all.each do |pad|
      mod = ConfigModule.where(name: configmodule_name(Core::Etherpad)).first
      instance = Instance.where(config_module_id: mod.id).first
      url = instance.option_by_name('url')

      full_url = url
      full_url += '/' unless full_url[-1] == '/'
      full_url += pad.uuid

      pad.full_url = full_url
      pad.save
    end
  end
end
