class CreateLinks < ActiveRecord::Migration[4.2]
  def change
    create_table :links do |t|
      t.string :domain_id, index: true
      t.string :title
      t.text :resume
      t.string :url
      t.string :font
      t.string :color

      t.timestamps null: false
    end
  end
end
