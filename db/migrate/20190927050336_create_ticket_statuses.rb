class CreateTicketStatuses < ActiveRecord::Migration[4.2]
  def change
    create_table :ticket_statuses do |t|
      t.string :libelle

      t.timestamps null: false
    end

    ["ouvert","en cours","fermé","annulé"].each do |status|
      TicketStatus.create libelle: status
    end
  end
end
