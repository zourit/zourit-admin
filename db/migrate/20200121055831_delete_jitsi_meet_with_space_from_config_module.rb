class DeleteJitsiMeetWithSpaceFromConfigModule < ActiveRecord::Migration[4.2]
  def change
    ConfigModule.where(name: 'Jitsi Meet').destroy_all
  end
end
