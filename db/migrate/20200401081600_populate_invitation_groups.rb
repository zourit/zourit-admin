class PopulateInvitationGroups < ActiveRecord::Migration[4.2]
  def change
    @pads_groups = PadsGroup.all
    @visios_groups = VisiosGroup.all
    @surveys_groups = SurveyGroup.all

    @pads_groups.each do |item|
      InvitationGroup.create(group_id: item.group_id, module_id: item.pad_id, module_tag: 'pad')
    end
    @visios_groups.each do |item|
      InvitationGroup.create(group_id: item.group_id, module_id: item.visio_id, module_tag: 'visio')
    end
    @surveys_groups.each do |item|
      InvitationGroup.create(group_id: item.group_id, module_id: item.survey_id, module_tag: 'survey')
    end
  end
end
