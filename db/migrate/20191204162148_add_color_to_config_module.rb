class AddColorToConfigModule < ActiveRecord::Migration[4.2]
  def change
    return if ApplicationRecord.connection.column_exists?(:config_modules, :color)

    add_column :config_modules, :color, :string
  end
end
