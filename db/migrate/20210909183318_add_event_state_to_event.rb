class AddEventStateToEvent < ActiveRecord::Migration[6.0]
  def change
    add_reference :events, :events_state, null: false, foreign_key: true, default: 3
  end
end
