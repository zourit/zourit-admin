class AddTypeToSurvey < ActiveRecord::Migration[4.2]
  def change
    add_column :surveys, :allow_multiple, :boolean
  end
end
