class AddActionToStatuses < ActiveRecord::Migration[4.2]
  def change
    add_column :ticket_statuses, :action, :string

    #FIXME ce morceau de code semble obsolète, peux-t-on le supprimer définitivement @steven ?
    
    # encours = TicketStatus.where(libelle: 'en cours').first
    # annule = TicketStatus.where(libelle: 'annulé').first
    # ferme = TicketStatus.where(libelle: 'fermé').first
    # encours.action = 'passer en cours '
    # annule.action = 'annuler'
    # ferme.action = 'fermer'
    # encours.save
    # annule.save
    # ferme.save

  end
end
