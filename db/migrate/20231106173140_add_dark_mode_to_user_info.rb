class AddDarkModeToUserInfo < ActiveRecord::Migration[7.0]
  def change
    add_column :user_infos, :darkmode, :boolean, default: false
  end
end
