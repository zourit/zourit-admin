class AddDataLossOnDeactivationToConfigModule < ActiveRecord::Migration[7.0]
  def change
    add_column :config_modules, :data_loss_on_deactivation, :boolean, default: true
  end
end
