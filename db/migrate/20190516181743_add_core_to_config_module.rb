class AddCoreToConfigModule < ActiveRecord::Migration[4.2]
  def change
    add_column :config_modules, :core, :boolean
  end
end
