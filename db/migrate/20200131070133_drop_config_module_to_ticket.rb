class DropConfigModuleToTicket < ActiveRecord::Migration[4.2]
  def change
    remove_column :tickets, :config_module_id, :string
  end
end
