class AddInstanceStateToInstance < ActiveRecord::Migration[4.2]
  def change
    add_reference(:instances, :instance_state, index: true, foreign_key: true, default: 1)
  end
end
