class AddFriendlyIdToVisios < ActiveRecord::Migration[7.0]
  def change
    add_column :visios, :friendly_id, :string
  end
end
