TODO
====

* [ ] Feature Flag
  * [ ] Continuous Integration

* [ ] UX
  * [ ] harmoniser les icones des modules (bandeau du haut + services numériques)
    * [ ] image cloud en SVG + icone font-awesome
  * [ ] CSS / LESS à épurer

* [X] Config populate META-options
* [X] afficher le formulaire en fonction des META-options
  * [X] imbriquer les instances dans les config modules
* [X] stocker les données dans une table -> INSTANCE-options

* [ ] factoriser le controlleur de modules
  * [X] sur le modèle de INSTANCE-options, faire DOMAIN-options
  * [X] basculer en production
  * [ ] les controllers cloud, mail, compta devrait être un seul controlleur générique
    * [X] migration Owncloud -> Nextcloud
    * [X] appeler toujours le module get_login_url(ServiceContext)
      * [X] manque Sympa
      * [ ] manque EtherPAD
      * [ ] manque sondages
    * [ ] supprimer code mort
  
* [ ] synchroniser les META-OPTIONS et le fichier Yaml (surtout si on supprime)
  * [ ] suppression Yaml doit supprimer en base quand on joue `rake module:populate`
