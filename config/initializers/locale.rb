# tell the I18n library to load additional locales from app/lib/core
I18n.load_path += Dir[Rails.root.join(
  'app', 'lib', 'core', '*', 'locales', '*.{rb,yml}'
)]

LOCALES = { fr: 'Français', en: 'English', es: 'Español', it: 'Italiano' }
I18n.available_locales = LOCALES.map { |k, _v| k.to_s }
I18n.default_locale = :fr
