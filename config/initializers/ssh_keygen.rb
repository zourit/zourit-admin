# ne se charge pas si lancer depuis la console avec TAILWINDCSS, cf. Procfile.dev
return if ENV.fetch('TAILWINDCSS', nil)

logger = SemanticLogger['initializers::ssh_keygen']

# permet d'initialiser la clef publique/privée de Zourit pour se connecter aux instances
ZOURIT_SSH_FOLDER = File.join(Rails.root, 'config', 'ssh')
ZOURIT_SSH_PRIVATE_KEY = "#{ZOURIT_SSH_FOLDER}/zourit_rsa".freeze
ZOURIT_SSH_PUBLIC_KEY = "#{ZOURIT_SSH_FOLDER}/zourit_rsa.pub".freeze

def generate_once(logger)
  if Dir.exist?(ZOURIT_SSH_FOLDER)
    logger.info("Zourit PUBLIC SSH KEY found: #{ZOURIT_SSH_PUBLIC_KEY}")
    return
  end

  machine = `hostname -f`.chop
  comment = Rails.env.production? ? '' : "#{`whoami`.chomp}@"
  comment += "ZOURIT-#{Rails.env}-#{machine}"

  Dir.mkdir(ZOURIT_SSH_FOLDER)
  logger.info 'Generating SSH keys for the first time (as PEM certificate)....'

  # PEM is useful for legacy SSH Connection used by Ruby
  `cat /dev/zero | ssh-keygen -m PEM -q -N "" -f #{ZOURIT_SSH_PRIVATE_KEY} -C #{comment}`

  logger.info 'Here is your Zourit PUBLIC SSH KEY'
  File.open(ZOURIT_SSH_PUBLIC_KEY, 'r') do |f|
    f.each_line do |line|
      logger.info line
    end
  end
end

begin
  generate_once(logger)
rescue StandardError => e
  logger.error "cannot generate SSH keys. This is mandatory : #{e.message}"
  exit(-1)
end
