# ne se charge pas si lancer depuis la console avec TAILWINDCSS, cf. Procfile.dev
return if ENV.fetch('TAILWINDCSS', nil)

# Initialisation de la connexion à la base LDAP
require 'yaml'

config = YAML.load_file(Rails.root.join('config', 'ldap.yml'))

SUPERLDAP = Net::LDAP.new
SUPERLDAP.host = config['host']
SUPERLDAP.port = config['port']

logger = SemanticLogger['initializers::ldap']

logger.info("authenticating to LDAP #{SUPERLDAP.host}:#{SUPERLDAP.port} as #{config['admin']} ...")
SUPERLDAP.auth config['admin'], config['password']
begin
  SUPERLDAP.bind
  logger.info('bind LDAP successful')
rescue StandardError => e
  logger.error 'LDAP Connexion failed', e
end
