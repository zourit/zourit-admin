# ne se charge pas si lancer depuis la console avec TAILWINDCSS, cf. Procfile.dev
return if ENV.fetch('TAILWINDCSS', nil)

logger = SemanticLogger['initializers::monkey_patch']
patches = Rails.root.join('lib', 'monkey_patches', '**', '*.rb')
count = Dir.glob(patches).count

# Chargement de tous les monkey patches dans lib/
Dir.glob(patches).sort.each do |file|
  logger.info "Patching... #{file}"
  require file
end

case count
when 0 then logger.info  'No patch found =='
when 1 then logger.info  '1 successful patch applied =='
else logger.info "#{count} successful patch applied =="
end
