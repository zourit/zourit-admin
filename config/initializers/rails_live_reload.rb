if defined?(RailsLiveReload)
  RailsLiveReload.configure do |config|
    # config.url = "/rails/live/reload"

    # Default watched folders & files
    config.watch %r{app/views/.+\.(erb|haml|slim)$}
    config.watch %r{(app|vendor)/(assets|javascript)/\w+/(.+\.(css|js|html|png|jpg|ts|jsx)).*}, reload: :always

    # USEFUL for tailwind changes!!!!
    config.watch %r{app/assets/builds/tailwind.css}, reload: :always

    config.enabled = Rails.env.development?
  end
end
