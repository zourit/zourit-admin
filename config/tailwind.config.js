const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  darkMode: 'class',
  content: [
    './public/*.html',
    './app/helpers/**/*.rb',
    './app/javascript/**/*.js',
    './app/views/**/*.{erb,haml,html,slim}'
  ],
  theme: {
    extend: {
      fontFamily: {
        sans: ['Inter var', ...defaultTheme.fontFamily.sans],
      },
      colors: {
        zourit: {
          1: '#ad0c78', // main_colour => purple
          2: '#535786', // special violet
          3: '#9ca3af', // special gray for background (old: #374151)
          100: '#CD0E8E',
          200: '#ad0c78',
          400: '#6B70A3',
          500: '#535786',         
        },
      },
      backgroundImage: {
        'fond': "url('commun/cheap_diagonal_fabric.png');",
        'fond-dark': "url('commun/black-woven-fabric-pattern.jpg');"
      },
      keyframes: {
        "fade-in": {
          '0%': { opacity: '0%' },
          '100%': { opacity: '100%' },
        }
      },
      animation: {
        "fade-in": 'fade-in 0.2s ease-in-out',
      }
    },
  },
  plugins: [
    require('@tailwindcss/forms'),
    require('@tailwindcss/aspect-ratio'),
    require('@tailwindcss/typography'),
    require('@tailwindcss/container-queries'),
  ]
}
