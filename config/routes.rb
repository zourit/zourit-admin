Rails.application.routes.draw do
  resources :domain_categories
  resources :forms do
    resources :formlines
  end
  resources :memos
  resources :links, except: %i[show]
  resources :survey_users
  resources :survey_groups
  resources :survey_answers
  resources :survey_options
  resources :surveys
  resources :instance_stories
  resources :instance_states
  resources :tickets
  resources :ticket_categories
  resources :ticket_statuses

  resources :pads_users
  resources :pads_groups
  resources :pads
  resources :visios
  resources :domains
  resources :users

  post 'domains/:id/users/:user_id', to: 'domains#update_user'
  get 'domains/:id/users/:user_id/edit', to: 'domains#edit_user', as: 'edit_domain_user'
  get 'domains/:id/users/new', to: 'domains#new_user', as: 'new_domain_user'
  post 'domains/:id/users', to: 'domains#create_user'
  delete 'domains/:id/users/:user_id', to: 'domains#delete_user', as: 'delete_domain_user'

  post 'memos/:id/archive', to: 'memos#archive', as: 'archive_memo'
  post 'visios/:id/archive', to: 'visios#archive', as: 'archive_visio'
  post 'pads/:id/archive', to: 'pads#archive', as: 'archive_pad'

  get 'users/:id/options' => 'users#options'
  post 'users/:id/options' => 'users#set_options'
  post 'users/:id/password' => 'users#set_password'

  get 'users/:id/groups' => 'users#groups'

  get '/visios_export', to: 'visios#export', as: 'visios_export'
  get '/pads_export', to: 'pads#export', as: 'pads_export'
  get '/memos_export', to: 'memos#export', as: 'memos_export'
  get '/surveys_export', to: 'surveys#export', as: 'surveys_export'
  get '/domains_export', to: 'domains#export', as: 'domains_export'
  get '/domains/:id/users_export', to: 'domains#users_export', as: 'domain_users_export'

  resources :config_modules do
    resources :instances
  end

  root 'services#index'
  resources :services, only: %i[index show]

  get 'users/:id/alias' => 'users#alias'
  delete '/alias/delete.json' => 'users#delete_alias'
  post '/alias/create.json' => 'users#create_alias'

  get 'domains/:id/administrators' => 'domains#administrators'

  get 'domains/:id/groups', to: 'groups#index', as: 'groups'
  post 'domains/:id/groups', to: 'groups#create'
  get 'domains/:id/groups/:group_id', to: 'groups#show', as: 'group'
  delete 'domains/:id/groups/:group_id', to: 'groups#destroy'
  patch 'domains/:id/groups/:group_id', to: 'groups#update', as: 'group_update'
  post 'login' => 'users#login'
  get 'logout' => 'users#logout', as: :logout

  get 'domains/:id/resources' => 'resources#index'
  post 'domains/:id/resources' => 'resources#create'
  delete 'domains/:id/resources' => 'resources#destroy'

  get 'domains/:id/infos' => 'domain_infos#index'
  get 'domains/:id/infos/:mod_id' => 'domain_infos#new'
  post 'domains/:id/infos/:mod_id' => 'domain_infos#create'

  get 'domains/:id/details' => 'domain_details#edit'
  post 'domains/:id/details' => 'domain_details#update'

  get 'visios' => 'visios#index'
  get 'visios/:id/users' => 'visios#users'
  get 'about' => 'about#index', as: 'about'
  get 'current_user' => 'users#current_user'
  post 'visios/:id/mails' => 'visios#mails'
  delete 'visios/:id/users' => 'visios#destroy_users'

  post '/visios_groups' => 'visios_groups#create'
  delete 'visios_groups/:id' => 'visios_groups#destroy'
  post '/visios_users' => 'visios_users#create'
  delete 'visios_users/:id' => 'visios_users#destroy'

  get '/visios/:id/button_to_bbb', to: 'visios#button_to_bbb', as: 'button_to_bbb'
  get '/visios/:id/recordings', to: 'visio_recordings#index', as: 'visio_recordings'
  get '/visios/:id/recordings_list', to: 'visio_recordings#visio_recordings', as: 'visio_recordings_list'
  get 'visios/:id/recordings/after_destroy', to: 'visio_recordings#after_destroy', as: 'visio_recordings_after_destroy'
  delete 'visios/:id/recordings/:record_id' => 'visio_recordings#destroy', as: 'visio_recordings_destroy'
  get '/visios/:id/settings', to: 'visio_settings#index', as: 'visio_settings'
  post '/visios/:id/settings', to: 'visio_settings#update_setting', as: 'visio_settings_update_setting'
  post '/visios/:id/settings/generate_viewer_access_code', to: 'visio_settings#generate_viewer_access_code',
                                                           as: 'visio_settings_generate_viewer_access_code'
  post '/visios/:id/settings/generate_moderator_access_code', to: 'visio_settings#generate_moderator_access_code',
                                                              as: 'visio_settings_generate_moderator_access_code'
  get 'visios/:id/recordings/:record_id/download', to: 'visio_recordings#download', as: 'visio_recording_download'

  post '/pads_groups' => 'pads_groups#create'
  delete 'pads_groups/:id' => 'pads_groups#destroy'

  post '/pads_users' => 'pads_users#create'
  delete 'pads_users/:id' => 'pads_users#destroy'

  get 'pads/:id/invitation' => 'pads#invitation'
  post 'pads/:id/mails' => 'pads#mails'
  post 'pads/:id/sharespad' => 'pads#sharespad'
  get 'pads/:id/users' => 'pads#users'
  delete 'pads/:id/users' => 'pads#destroy_users'

  post 'tickets/:id/new_message' => 'tickets#new_message'
  post 'surveys/:id/new_message' => 'surveys#new_message'
  get 'dates' => 'dates#index'
  get 'pads' => 'pads#show'
  get 'compta' => 'compta#index'
  get 'sympa' => 'sympa#index'

  get 'config_modules/:domain_id/infos' => 'config_modules#infos'

  get 'status' => 'events#index'
  get 'status/:id' => 'events#show', as: :show_instance_status
  get 'status/:id/events/:event_id' => 'events#messages', as: :show_instance_event
  post 'status/:id/events/:event_id' => 'events#new_message', as: :new_instance_event
  patch 'status/:id/events/:event_id' => 'events#update', as: :update_instance_event
  get 'status/:id/new' => 'events#new', as: :new_event
  post 'status/:id/new' => 'events#create', as: :create_instance_event

  post 'surveys/:id/share' => 'surveys#share'

  get 'statistics' => 'statistics#index'
  get 'statistics/instances' => 'statistics#instances'

  get '/invitations/:tag/:id', to: 'invitations#index', as: 'invitations'

  get 'forms/:id/formlines' => 'formlines#index'
  post '/forms/:id/formlines/new' => 'formlines#new'

  post '/invitations/:tag/:id/invite' => 'invitations#invite'
  get '/invitation/destroy/:type_id' => 'invitations#destroy'
  get '/invitation/send_email/:type_id' => 'invitations#share_invitation_to_email'
  get 'import' => 'import#launch_import'
  get 'end_visio', to: 'external_pages#end_visio', as: 'end_visio'
end
