require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module ZouritAdmin
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.0

    DEFAULT_THEME = 'theme3'.freeze
    TITLE = 'Zourit.net'.freeze
    PASSWORD_PATTERN = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/
    PASSWORD_POLICY = 'Le mot de passe doit contenir à minima 8 caractères, une majuscule et un chiffre'.freeze

    # load semver git tag if any
    begin
      VERSION = File.read('.semver_git_tag').freeze
    rescue StandardError
      VERSION = 'major.minor.patch'.freeze
    end

    NOTIFICATION_SENDER = YAML.load_file(Rails.root.join('config', 'smtp.yml'))['notification_sender']

    # Efficient logging with Semantic Logger
    require Rails.root.join('lib', 'formatters', 'basic_formatter')
    config.rails_semantic_logger.add_file_appender = false
    config.rails_semantic_logger.semantic = false
    config.rails_semantic_logger.started = true
    config.rails_semantic_logger.processing = true
    config.rails_semantic_logger.rendered = true

    # Langue française par défaut
    config.i18n.fallbacks = [:fr]
  end
end
