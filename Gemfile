source 'http://rubygems.org'
ruby '3.1.2'

gem 'rest-client'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~>7.0'
# Use sqlite3 as the database for Active Record
gem 'sqlite3'

gem 'pg'
# Use SCSS for stylesheets
gem 'sass-rails'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
# gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder'
gem 'json'

gem 'importmap-rails'
gem 'turbo-rails'

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development
gem 'font_awesome5_rails'

gem 'datetime_picker_rails'
gem 'momentjs-rails'

gem 'bootsnap'
gem 'etherpad-lite'
gem 'hover-rails'
gem 'http_accept_language'
gem 'i18n'
gem 'ihover-rails'
gem 'justgage-rails'
gem 'net-ldap'
gem 'openssl'
gem 'rb-readline'
gem 'uuid'
gem 'uuidtools'

# SSH related
gem 'net-ssh'

gem 'local_time'

gem 'groupdate'

# ASSETS
# gem 'sprockets', '~> 3.1'
gem 'sprockets'

# Color manipulation
gem 'chroma'

gem 'progress_bar'

# Logger
gem 'amazing_print'
gem 'rails_semantic_logger'

gem 'puma'
gem 'time_difference'

# gem 'redis'

gem 'tailwindcss-rails'

group :production do
  gem 'syslog_protocol'
end

group :development do
  gem 'debug'
  gem 'error_highlight'
  gem 'htmlbeautifier'
  gem 'rails_live_reload'
  gem 'rubocop'
  gem 'rubocop-packaging'
  gem 'rubocop-performance'
  gem 'rubocop-rspec'
  gem 'rubocop-shopify'
  gem 'rubocop-thread_safety'
  gem 'ruby-lsp-rails'
end

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
  gem 'listen'

  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
end
