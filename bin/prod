#!/usr/bin/bash
set -Eeuo pipefail

# CONSTANT DEFINITION
APP_NAME='zourit-admin'
PRODUCTION_PORT=5000
REPO_URL='https://gitlab.com/zourit/zourit-admin.git'
DEPLOY_FOLDER="/opt/$APP_NAME"
SHARED_CONFIG_FOLDER="/var/lib/$APP_NAME"

# CHECK WHETHER CURRENT USER BELONG TO SUDO GROUP
if groups | grep -q sudo; then
    sudo --validate --prompt='sudo privilege required for further processing: '
else
    >&2 echo 'current user not in the `sudo` group, aborted!'
    exit 10
fi

# FETCH LATEST VERSION AND SAVE TO DEPLOY_FOLDER
latest_tag=$(git -c 'versionsort.suffix=-' ls-remote --tags --sort='-v:refname' "$REPO_URL" | grep -v 'refs/tags/v' | head -n1 | cut -f2 | cut -d'/' -f3)
if [[ ! -d "$DEPLOY_FOLDER" ]]; then
    echo "clone latest tag <$latest_tag> from repo <$REPO_URL> into $DEPLOY_FOLDER"
    sudo git config --global advice.detachedHead false
    sudo git clone --branch "$latest_tag" --depth 1 $REPO_URL $DEPLOY_FOLDER
else
    current_tag=$(cat "$DEPLOY_FOLDER/.semver_git_tag")
    if [[ "$current_tag" == "$latest_tag" ]]; then
        echo "already up-to-date!"
        echo "version: $latest_tag"
        exit 0
    else
        echo "refreshing current repository to latest_tag <$latest_tag>"
        cd "$DEPLOY_FOLDER"
        sudo git fetch --tags
        sudo git pull origin "$latest_tag"
    fi
fi

# CHECK WHETHER service.conf exists
if [[ ! -f "/etc/$APP_NAME/service.conf" ]]; then
    sudo mkdir -p "/etc/$APP_NAME"
    echo "generate new SECRET_KEY_BASE"
    SECRET_KEY_BASE=$( SIZE=32 ; head /dev/urandom | tr -dc A-Za-z0-9 | head -c $SIZE )
    echo "build service.conf"
    sudo tee "/etc/$APP_NAME/service.conf" &>/dev/null <<EOF
RAILS_PORT=$PRODUCTION_PORT
SECRET_KEY_BASE=$SECRET_KEY_BASE
SEMANTIC_LOGGER_APP=$APP_NAME
EOF
    sudo chmod 600 /etc/$APP_NAME/service.conf
fi

# COPY configuration files from SHARED_CONFIG_FOLDER
for i in database ldap smtp; do
    if [[ ! -f "/etc/$APP_NAME/$i.yml" ]]; then
        if [[ -f "$SHARED_CONFIG_FOLDER/$i.yml" ]]; then
            sudo cp "$SHARED_CONFIG_FOLDER/$i.yml" "/etc/$APP_NAME/$i.yml"
            sudo chmod 600 "/etc/$APP_NAME/$i.yml"
        else
           >&2 echo "$SHARED_CONFIG_FOLDER/$i.yml undefined, please provide your own config file and save /etc/$APP_NAME/$i.yml, aborted!"
           exit 20
        fi 
    fi
    sudo ln -sf "/etc/$APP_NAME/$i.yml" "$DEPLOY_FOLDER/config/$i.yml"
done

# INSTALL NEW SYSTEMD SERVICE EACH TIME
echo "installing systemd services"
sudo tee /etc/systemd/system/$APP_NAME.service &>/dev/null <<EOF
[Unit]
Description=$APP_NAME
After=network.target

[Service]
Type=simple
Environment="RAILS_ENV=production"
EnvironmentFile=/etc/$APP_NAME/service.conf
WorkingDirectory=$DEPLOY_FOLDER
SyslogIdentifier=$APP_NAME
PermissionsStartOnly=true
ExecStart=/usr/local/bin/rails server --port \$RAILS_PORT

[Install]
WantedBy=multi-user.target
EOF

# STOP PREVIOUS RELEASE IF ANY
sudo systemctl daemon-reload
sudo systemctl enable $APP_NAME.service
sudo systemctl stop $APP_NAME.service

# INSTALL, MIGRATE and POPULATE
secret=$(sudo grep SECRET_KEY_BASE /etc/zourit-admin/service.conf | cut -d '=' -f2)
cd $DEPLOY_FOLDER
sudo bundle install
sudo RAILS_ENV=production SECRET_KEY_BASE=$secret rails db:migrate
sudo RAILS_ENV=production SECRET_KEY_BASE=$secret rails modules:populate
sudo RAILS_ENV=production SECRET_KEY_BASE=$secret rails assets:precompile

# START NEW RELEASE
sudo systemctl start $APP_NAME.service

if systemctl is-active --quiet $APP_NAME.service; then
    echo "$APP_NAME successfully installed, up-and-running!"
else
    >&2 echo "something wrong prevents $APP_NAME to run successfully, please have a look: `sudo journalctf -n100 -u $APP_NAME`, aborted!"
    sudo systemctl status $APP_NAME.service
    exit 30
fi
