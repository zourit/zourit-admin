# 1.0 from 0.9
* [x] Passage en rails 6.0
* [x] Gestion des alias en base pour accélérer leur affichage
* [x] Pouvoir faire une recherche sur les alias
* [x] Ajout des Lines personnel en plus des liens du domaine

## A exécuter en production
* bundle install
* rails modules:populate
* rails user_options:populate_from_ldap
* rails user_options:populate_domain_id
* rails alias:populate