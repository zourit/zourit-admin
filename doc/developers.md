DEV
===

pour faciliter le développement, les services LDAP, Zimbra, Nextcloud et cie sont pré-installés sur un environnement de test. Il est utile d'avoir un accès VPN (demander autorisation) pour utiliser ses services côté dev.

Environnement de développement
------------------------------

* GNU/Linux
  * au choix [Debian](#debian-first-install) ou [Arch](#arch-first-install)
* Vs Codium
* Ruby
  * Rails 7
* Postgres SQL

REUSE SSH CONTAINER
-------------------

* # First: ASK FOR a ssh GRANT ACCESS!!!
* cp .env.sample .env # then, change RAILS_PORT (default: 3000) and RDEBUG_PORT (default: 30000) in case you run multiple instances

DEBIAN First install
--------------------

* sudo apt -y install curl gawk g++ \
make libreadline6-dev zlib1g-dev libssl-dev \
libyaml-dev libsqlite3-dev sqlite3 autoconf \
libgdbm-dev libncurses5-dev libtool bison nodejs \
pkg-config libffi-dev libgmp-dev libgmp-dev git \
dirmngr postgresql postgresql-server-dev-all \
ldap-utils sqlitebrowser
* gpg --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
* curl -sSL https://get.rvm.io | bash
* # logout/login
* rvm get stable --auto-dotfiles
* rvm install 2.7.3
* rvm alias create default ruby-2.7.3

* # LDAP apache directory studio
* sudo apt install -y openjdk-11-jre
* wget -O ApacheDirectoryStudio.tgz https://downloads.apache.org/directory/studio/2.0.0.v20210213-M16/ApacheDirectoryStudio-2.0.0.v20210213-M16-linux.gtk.x86_64.tar.gz
* tar -xzvf ApacheDirectoryStudio.tgz
* cd ApacheDirectoryStudio
  * ./ApacheDirectoryStudio
  * # help yourself with [ldap.yml](../config/ldap.yml)

* # vpn
* sudo apt install -y network-manager-openvpn

ARCH First install
------------------

### Installez les dépendances :

* sudo pacman -Syu ruby2.7 base-devel sqlite shared-mime-info libpqxx postgresql postgresql-libs git nodejs
* nano ~/.bashrc
```
export GEM_HOME="$(ruby-2.7 -e 'puts Gem.user_dir')"
export PATH="$PATH:$GEM_HOME/bin"  
```
* source ~/.bashrc
* sudo ln -s $(which ruby-2.7) /usr/local/bin/ruby
* sudo ln -s $(which gem-2.7) /usr/local/bin/gem
* sudo ln -s $(which irb-2.7) /usr/local/bin/irb

* gem install bundler

Installation des outils en mode développeur
--------------------------------------------

### Récupérer le code

* git clone https://gitlab.com/zourit/zourit-admin

* cd zourit-admin
  * bundle install
  * yard gems # for solargraph online documentation

### Debian 11 problème avec ffi version 7 au lieu de 6

* curl -LO http://archive.ubuntu.com/ubuntu/pool/main/libf/libffi/libffi6_3.2.1-8_amd64.deb
* sudo dpkg -i libffi6_3.2.1-8_amd64.deb

### Configurer LDAP et DATABASE

Créer les fichiers config/ldap.yml et config/database.yml en vous inspirant des fichiers sample associés.

  * cd config
    * #demander la configuration pour un LDAP local, puis:
    * cp ldap.yml.sample ldap.yml
    * cp smtp.yml.sample smtp.yml
    * cp database.yml.sample database.yml
    * cd ..
### Lancer les migrations

* rails db:migrate # should fail once! DON'T WORRY
* rails modules:populate
* rails db:migrate # SUCCESS

### Démarrer le serveur en version de **développement**

* rails server

RQ: En production, préférez une solution type *Nginx* avec *Passenger*.

debugger
--------

à noter que j'ai rajouté dans launch.json, une option pour supprimer les warning $SAFE rapport à ruby3.0
attention, si jamais on passe à Ruby3.0, il ne faudra pas oublier d'enlever

```
"env": {
    "RUBYOPT": "-W0"
}
```

SURCHARGER LA CONFIGURATION LOCALE
----------------------------------

Pour changer la configuration de l'environnement **DEV** uniquement, il est possible de rajouter le fichier
`config/environments/development_local.rb`

Voici un exemple pour désactiver les logs de *ActiveRecord*
```
Rails.application.configure do

  #disable activerecord logging
  config.active_record.logger = nil
end
```

Dans le cas d'une création d'un connecteur
-------------

### Méthodes à surcharger
* get_login_url(service_context)
* get_logout_url(service_context)
* create_domain(domain_info, domain)
* destroy_domain(domain_info, domain)
* create_user(user)
* update_user_option(meta_option, user, value)

Debian Bookworm (normal user)
-----------------------------

* sudo apt install ruby-dev libpq-dev nodejs build-essential
* vim $HOME/.bashrc
```
GEM_HOME=$HOME/.gem
PATH=$GEM_HOME/bin:$PATH
export GEM_HOME PATH
```
* source $HOME/.bashrc
* sudo gem install bundler solargraph
* bundle