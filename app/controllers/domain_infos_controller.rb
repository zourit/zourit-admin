class DomainInfosController < ApplicationController
  before_action :set_domain
  before_action :set_module, except: [:index]

  before_action do
    super_admin_only
    @icon = 'globe'
  end

  def index
    @infos = DomainInfo.where(domain_id: @domain.id)
    @modules_to_link = ConfigModule.all
  end

  # GET /domains/1/infos/2/new
  def new
    @domainInfo = DomainInfo.new
  end

  # POST /domains/1/infos/2
  def create
    if params[:domain_info][:instance_id] == ''
      redirect_to "/domains/#{@domain.id}/infos",
                  notice: (I18n.t 'domain.noajout').to_s
    else
      @domainInfo = DomainInfo.new
      @domainInfo.domain_id = @domain.id
      @domainInfo.config_module_id = @mod.id
      @domainInfo.instance_id = params[:domain_info][:instance_id]
      if @domainInfo.save
        logger.info "Création du DomainInfo #{@domain.id} - module #{@mod.id}"
        redirect_to "/domains/#{@domain.id}/infos",
                    notice: (I18n.t 'domain.ajout').to_s
      else
        logger.warn "Erreur lors de la création du DomainInfo #{@domain.id} - module #{@mod.id}"
        render :new
      end
    end
  end

  private

  def set_domain
    @domain = Domain.find(params[:id])
  end

  def set_module
    @mod = ConfigModule.find(params[:mod_id])
  end
end
