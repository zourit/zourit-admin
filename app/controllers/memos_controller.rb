class MemosController < ApplicationController
  before_action do
    only_authorized_users('Framemo')
  end
  before_action :set_dataset, only: %i[index export]
  before_action :set_memo, only: %i[show edit update destroy archive]
  before_action only: %i[edit update destroy] do
    only_owner!(@memo.owner_id, memos_path)
  end

  # GET /memos
  def index; end

  # GET /memos/1
  def show
    @type_page = 'iframe'
    is_owner = (@memo.owner_id == @current_user.id)
    user_invitation = InvitationUser.where(module_id: @memo.id, module_tag: 'memo',
                                           uuid: @current_user.id).count.positive?
    group_invitation = InvitationGroup.where(module_id: @memo.id, module_tag: 'memo',
                                             group_id: @current_user.groups_uuid).count.positive?
    unless is_owner || user_invitation || group_invitation
      redirect_to memos_path, notice: (I18n.t 'modules.framemo.noparticipe')
    end
    configmodule = ConfigModule.where(name: configmodule_name(Core::Framemo)).first
    domain_info = DomainInfo.where(domain_id: current_user.domain.id, config_module_id: configmodule.id).first
    url_server = domain_info.instance.option_by_name('url')
    if url_server
      @url_embedded = url_server
      @url_embedded += '/' unless @url_embedded[-1] == '/'
      @url_embedded += @memo.uuid
      log_statistics(configmodule.name, domain_info.instance.name)
    else
      redirect_to services_path, notice: (I18n.t 'modules.framemo.noactif').to_s
    end
  end

  # GET /memos/new
  def new
    @memo = Memo.new
  end

  # GET /memos/1/edit
  def edit; end

  # POST /memos
  def create
    @memo = Memo.new(memo_params)
    @memo.uuid = random_uuid
    @memo.owner_id = @current_user.id

    if @memo.save
      redirect_to @memo, notice: I18n.t('modules.framemo.noticecreate', memo_name: @memo.name).to_s
    else
      render :new, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /memos/1
  def update
    if @memo.update(memo_params)
      redirect_to memos_path, notice: I18n.t('modules.framemo.noticeupdate', memo_name: @memo.name).to_s
    else
      render :edit
    end
  end

  # DELETE /memos/1
  def destroy
    @memo.destroy
    redirect_to memos_url, notice: I18n.t('modules.framemo.verif').to_s
  end

  # POST /memos/1/archive
  def archive
    @memo.archived?(@current_user) ? @memo.unarchive(@current_user) : @memo.archive(@current_user)
    redirect_to memos_url
  end

  def export
    send_data generate_csv(@dataset),
              type: 'text/csv; charset=utf-8; header=present',
              disposition: 'attachment; filename=memos.csv'
  end

  private

  def set_dataset
    id_memo_invite = InvitationUser.where(module_tag: 'memo',
                                          uuid: @current_user.id).distinct.collect(&:module_id)
    id_memo_invite += InvitationGroup.where(module_tag: 'memo',
                                            group_id: @current_user.groups_uuid).distinct.collect(&:module_id)

    memos_all = Memo.where('owner_id = ? or memos.id in (?)', @current_user.id,
                           id_memo_invite).distinct
    memos = if index_params[:archived] == '1'
              memos_all.joins(:memo_archives)
                       .where('memo_archives.user_id = ?', @current_user.id)
                       .distinct.to_a
            else
              memos_all.reject { |memo| memo.archived?(@current_user) }
            end

    @authors = authors_from_memos(memos_all)

    memos = memos_filters_and_order(memos, index_params)
    @dataset = dataset_pagination(memos, index_params)
  end

  def generate_csv(dataset)
    CSV.generate do |csv|
      csv << [t('modules.framemo.memoname'),
              t('modules.framemo.memodesc'),
              t('modules.framemo.author'),
              t('modules.framemo.createdate')]

      dataset[:items].each do |data|
        csv << [
          data.name,
          data.comment,
          data.owner&.name,
          data.created_at
        ]
      end
    end
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_memo
    @memo = Memo.find(params[:id])
  rescue StandardError => e
    flash.alert = "#{I18n.t('modules.framemo.dontexist')}|#{e}"
    redirect_to memos_path, index_params
  else
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def memo_params
    params.require(:memo).permit(:name, :comment, :uuid, :owner_id)
  end

  def index_params
    params.permit(:author, :order, :query, :page, :items_per_page, :archived)
  end

  def authors_from_memos(memos)
    authors = memos.collect { |memo| [memo.owner&.name, memo.owner_id] if memo.owner }
    authors.uniq.compact
  end

  def memos_filters_and_order(memos, params)
    if params[:author].present?
      memos.select! do |memo|
        memo.owner_id == params[:author]
      end
    end

    if params[:query].present?
      memos.select! do |memo|
        memo.name.downcase.include?(params[:query].downcase) or memo.comment.downcase.include?(params[:query].downcase)
      end
    end

    if params[:order].to_s.empty?
      memos.sort! { |a, b| b.created_at <=> a.created_at }
    else
      order_column, order_direction = params[:order].split(',')
      order_column = 'name' unless %w[name created_at].include?(order_column)
      order_direction = 'DESC' unless %w[ASC DESC].include?(order_direction)
      if order_column == 'name'
        memos.sort! { |a, b| b.name.downcase <=> a.name.downcase } if order_direction == 'ASC'
        memos.sort! { |a, b| a.name.downcase <=> b.name.downcase } if order_direction == 'DESC'
      else
        if order_direction == 'ASC'
          memos.sort! do |a, b|
            b.created_at <=> a.created_at
          end
        end
        if order_direction == 'DESC'
          memos.sort! do |a, b|
            a.created_at <=> b.created_at
          end
        end
      end
    end

    memos
  end
end
