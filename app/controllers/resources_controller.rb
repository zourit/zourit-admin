class ResourcesController < ApplicationController
  before_action :set_domain
  before_action :admin_only

  # GET /domains/1/resources
  # GET /domains/1/resources.json
  def index
    @resources = request.format.html? ? nil : Resource.find_all(@domain.name)
  end

  # GET /domains/1/resources/1
  # GET /domains/1/resources/1.json
  def show; end

  # POST /domains/1/resources
  # POST /domains/1/resources.json
  def create
    name = "#{group_params[:name].gsub(/\s+/, '')}@#{@domain.name}"
    resource = Resource.new name, group_params[:type]
    result, message, output = resource.save
    respond_to do |format|
      if result
        format.json { head :no_content }
      else
        format.json { render json: "#{I18n.t 'ressources.noressource'} #{name}\n#{message}", status: :unprocessable_entity }
      end
    end
  end

  # DELETE /domains/1/resources
  # DELETE /domains/1/resources
  def destroy
    result, message, output = Resource.destroy(group_params[:name])
    respond_to do |format|
      if result
        format.json { head :no_content }
      else
        format.json { render json: "#{I18n.t 'ressources.nodelete'} #{name}\n#{message}", status: :unprocessable_entity }
      end
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_domain
    @domain = Domain.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def group_params
    params.require(:resource).permit(:name, :type)
  end
end
