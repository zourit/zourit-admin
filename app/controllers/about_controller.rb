# frozen_string_literal: true

class AboutController < ApplicationController
  # GET /about
  def index
    @teams = [
      { name: 'Jean-Noël ROUCHON', role: 'about.devbackend' },
      { name: 'Pascal GASCOIN', role: 'about.former' },
      { name: 'Camille ARNAUD', role: 'about.webdesigner' },
      { name: 'Armand DENISET', role: 'about.webmaster' },
      { name: 'Steven ROBERT', role: 'about.devbackend' },
      { name: 'Samuel GENRE', role: 'about.translater' },
      { name: 'Philippe VINCENT', role: 'about.devbackend' },
      { name: 'Nicolas LAURET', role: 'about.devbackend' },
      { name: 'Mathilde RAZAFIMAHATRATRA', role: 'about.devbackend' },
      { name: 'François AUDIRAC', role: 'about.testing_and_documentation' },
      { name: 'Marion ROMERO', role: 'about.documentation' },
      { name: 'Kévin HOARAU', role: 'about.devbackend' },
      { name: 'Clément Antoine XAVIER', role: 'about.devbackend' },
      { name: 'Killian KEMPS', role: 'about.devbackend' },
      { name: 'Guillaume VANDENHOVE', role: 'about.tester' },
      { name: 'Denise BITCA', role: 'about.tester' },
      { name: 'Patrick Labarrière', role: 'about.tester' },
      { name: 'Olivier Humbert', role: 'about.testing_and_documentation' },
      { name: 'Flavien Degoulet', role: 'about.tester' },
      { name: 'Aline', role: 'about.tester' },
      { name: 'Laurent Bessonnet', role: 'about.testing_and_documentation' },
      { name: 'Morgane Peroche', role: 'about.testing_and_documentation' },
      { name: 'Munier Jérôme', role: 'about.devbackend' }
    ]
  end
end
