class StatisticsController < ApplicationController
  before_action :super_admin_only

  COLORS = [
    'rgb(71, 85, 105)',   # Slate
    'rgb(75, 85, 96)',    # Gray
    'rgb(82, 82, 91)',    # Zinc
    'rgb(82, 82, 82)',    # Neutral
    'rgb(87, 83, 78)',    # Stone
    'rgb(220, 38, 38)',   # Red
    'rgb(234, 88, 12)',   # Orange
    'rgb(202, 138, 4)',   # Yellow
    'rgb(101, 163, 13)',  # Lime
    'rgb(22, 165, 86)',   # Green
    'rgb(6, 150, 107)',   # Emerald
    'rgb(13, 148, 133)',  # Teal
    'rgb(37, 99, 235)'    # Blue
  ].freeze

  # GET /statistics
  def index
    @colors = COLORS

    @unique = params['unique'] == 'true'

    @date_start = params['from'].present? ? DateTime.parse(params['from']) : DateTime.now - 7.days
    @date_end = params['to'].present? ? DateTime.parse(params['to']) : DateTime.now

    statistics_base = Statistic.where(created_at: @date_start..@date_end)
    @statistics = @unique ? statistics_base.select(:user_id).distinct : statistics_base
    @domains = @statistics.group(:domain_id).count.transform_keys do |k|
      Domain.find(k)&.name
    end

    @services = statistics_base.select(:service_name).distinct.map(&:service_name).map do |service|
      data = @statistics.where(service_name: service).group(:domain_id).count.transform_keys do |k|
        Domain.find(k)&.name
      end
      { name: service, data: }
    end
    @domains_count = statistics_base.select(:domain_id).distinct.count
  end

  # GET /statistics/instances
  def instances; end
end
