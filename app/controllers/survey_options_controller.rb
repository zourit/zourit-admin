class SurveyOptionsController < ApplicationController
  before_action :set_survey_option, only: %i[show edit update destroy]
  before_action :super_admin_only # TODO: Controleur jamais utilisé, possibilité de tout supprimer

  # GET /options
  # GET /options.json
  def index
    @survey_options = SurveyOption.all
  end

  # GET /options/1
  # GET /options/1.json
  def show; end

  # GET /options/new
  def new
    @survey_option = SurveyOption.new
  end

  # GET /options/1/edit
  def edit; end

  # POST /options
  # POST /options.json
  def create
    @survey_option = SurveyOption.new(survey_option_params)

    respond_to do |format|
      if @survey_option.save
        format.html { redirect_to @survey_option, notice: (I18n.t 'modules.survey.optioncreate').to_s }
        format.json { render :show, status: :created, location: @survey_option }
      else
        format.html { render :new }
        format.json { render json: @survey_option.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /options/1
  # PATCH/PUT /options/1.json
  def update
    respond_to do |format|
      if @survey_option.update(survey_option_params)
        format.html { redirect_to @survey_option, notice: (I18n.t 'modules.survey.optioncreate').to_s }
        format.json { render :show, status: :ok, location: @survey_option }
      else
        format.html { render :edit }
        format.json { render json: @survey_option.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /options/1
  # DELETE /options/1.json
  def destroy
    @survey_option.destroy
    respond_to do |format|
      format.html { redirect_to survey_options_url, notice:  (I18n.t 'modules.survey.optiondestroy').to_s }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_survey_option
    @survey_option = SurveyOption.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def survey_option_params
    params.require(:survey_option).permit(:survey_id, :choice)
  end
end
