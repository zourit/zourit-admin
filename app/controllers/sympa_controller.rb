class SympaController < ApplicationController
  before_action :only_logged_users

  # GET /sympa
  def index
    configmodule = ConfigModule.where(name: configmodule_name(Core::Sympa)).first
    if configmodule
      domaininfo = DomainInfo.where(config_module_id: configmodule.id,
                                    domain_id: current_user.domain.id).first
      if domaininfo&.instance_id
        @url_embedded = Core::Sympa::Helper.get_login_url current_user
      else
        redirect_to services_path,
                    notice: (I18n.t 'modules.survey.noticenotavailable').to_s
      end
    else
      redirect_to services_path, notice: (I18n.t 'modules.survey.noticenoactive').to_s
    end
  end
end
