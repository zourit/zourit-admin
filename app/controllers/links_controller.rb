class LinksController < ApplicationController
  include LinksHelper

  before_action :set_domain
  before_action :only_logged_users
  before_action :set_link, only: %i[show edit update destroy]
  before_action :set_fa_list, only: %i[edit new]
  before_action :only_owner_or_admin!, only: %i[edit update destroy]

  # GET /links
  # GET /links.json
  def index
    params[:global] = 'false' unless @current_user.admin
    links = if params[:global] == 'true'
              Link.where(domain_id: @current_user.domain.id, user_id: nil)
            else
              Link.where(user_id: @current_user.id)
            end
    # links += Link.where(domain_id: current_user.domain.id, user_id: nil) if current_user.admin

    links = links_filters_and_order(links, index_params)
    @dataset = dataset_pagination(links, index_params)

    if turbo_frame_request?
      render partial: 'links', locals: { links: }
    else
      render :index
    end
  end

  # GET /links/new
  def new
    @link = Link.new
    @link.symbol = 'link'
  end

  # GET /links/1/edit
  def edit
    if @link.user_id
      if @link.user_id != @current_user.id
        redirect_to links_path,
                    notice: (I18n.t 'status.autorize').to_s
      end
    elsif !authorized_access?(@domain.id)
      redirect_to links_path,
                  notice: (I18n.t 'status.autorize').to_s
    end
  end

  # POST /links
  # POST /links.json
  def create
    @link = Link.new(link_params)
    @link.domain_id = @current_user.domain.id
    @link.user_id = @current_user.id unless params['global_link'] == 'true' && @current_user.admin

    if @link.save
      redirect_to links_path(global: params[:global_link] == 'true'), notice: I18n.t('links.successfull_create')
    else
      redirect_to new_link_path(global: params[:global_link] == 'true'), alert: 'oh non !!!'
    end
  end

  # PATCH/PUT /links/1
  # PATCH/PUT /links/1.json
  def update
    @link.user_id = if params['global_link'] == 'true' && current_user.admin
                      nil
                    else
                      current_user.id
                    end
    @link.update(link_params)
    if @link.save
      redirect_to links_path(global: params['global_link']), notice: I18n.t('links.successfull_update')
    else
      render :edit
    end
  end

  # DELETE /links/1
  # DELETE /links/1.json
  def destroy
    if current_user.domain.id == @link.domain_id
      @link.destroy
      respond_to do |format|
        format.html { redirect_to links_url, notice: I18n.t('links.successfull_delete') }
        format.json { head :no_content }
      end
    else
      redirect_to links_url, notice: 'Vous n\'avez pas la permission de supprimer ce lien'
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_link
    @link = Link.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def link_params
    params.require(:link).permit(:title, :resume, :url, :font, :color, :symbol)
  end

  def set_domain
    @domain = current_user.domain
  end

  def set_fa_list
    icons = icons_list_model(params[:query])
    icons_params = index_params
    @fa_list = dataset_pagination(icons, icons_params, 100)
  end

  def index_params
    params.permit(:order, :query, :page, :items_per_page)
  end

  def links_filters_and_order(links, params)
    if params[:query].present?
      links = links.where('title ILIKE ? or resume ILIKE ?', "%#{params[:query]}%",
                          "%#{params[:query]}%")
    end

    if params[:order]
      _, order_direction = params[:order].split(',')
      order_direction = 'DESC' unless %w[ASC DESC].include?(order_direction)
    else
      order_direction = 'DESC'
    end
    order_column = 'title'

    links.order("#{order_column} #{order_direction}")
  end

  def only_owner_or_admin!
    return if @current_user.admin && @current_user.domain.id == @domain.id
    return if @link.user_id == @current_user.id

    redirect_to links_path(global: params[:global]),
                alert: I18n.t('application.law').to_s
  end
end
