class VisiosController < ApplicationController
  before_action do
    only_authorized_users('BigBlueButton')
  end
  before_action :set_visio, only: %i[show edit update destroy mails archive show button_to_bbb]
  before_action :set_url_local, only: %i[index create update destroy mails button_to_bbb]
  before_action :set_bbb_session, only: %i[create update destroy mails button_to_bbb]
  before_action :set_dataset, only: %i[index export]
  before_action only: %i[edit update destroy] do
    only_owner!(@visio.owner_id, visios_path)
  end
  before_action :only_owner_or_invite!, only: %i[show button_to_bbb]

  # GET /visios
  # GET /visios.json
  def index; end

  # GET /visios/1
  # GET /visios/1.json
  def show
    # redirect_to services_path, notice: (I18n.t 'modules.bigbluebutton.modulevisible').to_s unless @url_visios
    # log_statistics(@sc.domain_info.config_module.name, @sc.domain_info.instance.name)
    # display_name = "#{@current_user.first_name} #{@current_user.last_name}"
    # begin
    #  @visio_link = Core::BigBlueButton::Main.start_meeting(@sc, @session, @visio.friendly_id, display_name)
    # rescue RestClient::NotAcceptable
    #  redirect_to visios_path, notice: (I18n.t 'modules.bigbluebutton.visioconferencedoesnotexists').to_s
    # end
  end

  def button_to_bbb
    @type_page = 'noheader'
    display_name = "#{@current_user.first_name} #{@current_user.last_name}"
    # begin
    @visio_link = Core::BigBlueButton::Main.start_meeting(@sc, @session, @visio.friendly_id, display_name)
    # rescue RestClient::NotAcceptable
    #  redirect_to visios_path, notice: (I18n.t 'modules.bigbluebutton.visioconferencedoesnotexists').to_s
    # end
  end

  # GET /visios/new
  def new
    @visio = Visio.new
    @visio.date_start = DateTime.now
    @visio.allow_recording = true
  end

  # GET /visios/1/edit
  def edit; end

  # POST /visios
  # POST /visios.json
  def create
    @visio = Visio.new(visio_params)
    created_room = Core::BigBlueButton::Main.create_room(@sc, @session, visio_params[:name])
    @visio.uuid = created_room['id']
    @visio.friendly_id = created_room['friendly_id']
    @visio.owner_id = @current_user.id
    if @visio.save
      @visio.send_configuration(@sc, @session)
      redirect_to visios_path, notice: I18n.t('modules.bigbluebutton.successfulcreate', visio_name: @visio.name)
    else
      render :new
    end
  end

  # PATCH/PUT /visios/1
  # PATCH/PUT /visios/1.json
  def update
    if @visio.update(@sc, @session, visio_params)
      redirect_to @visio, notice: I18n.t('modules.bigbluebutton.successfulupdate', visio_name: @visio.name).to_s
    else
      logger.error "visio #{@visio.id} update failed"
      render :edit
    end
  end

  # DELETE /visios/1
  # DELETE /visios/1.json
  def destroy
    if @visio.destroy(@sc, @session)
      redirect_to visios_path, notice: I18n.t('modules.bigbluebutton.successfuldestroy')
    else
      redirect_to visios_path, error: I18n.t('modules.bigbluebutton.cannotdeletevisio')
    end
  end

  # POST /visios/1/archive
  def archive
    @visio.archived?(@current_user) ? @visio.unarchive(@current_user) : @visio.archive(@current_user)
    redirect_to visios_url
  end

  def export
    send_data generate_csv(@dataset),
              type: 'text/csv; charset=utf-8; header=present',
              disposition: 'attachment; filename=visios.csv'
  end

  private

  def set_dataset
    id_visio_invite = InvitationUser.where(module_tag: 'visio', uuid: @current_user.id).distinct.collect(&:module_id)
    id_visio_invite += InvitationGroup.where(module_tag: 'visio',
                                             group_id: @current_user.groups_uuid).distinct.collect(&:module_id)

    visios_all = Visio.where('owner_id = ? or visios.id in (?)', @current_user.id, id_visio_invite).distinct
    visios = if index_params[:archived] == '1'
               visios_all.joins(:visio_archives)
                         .where('visio_archives.user_id = ?', @current_user.id)
                         .distinct.to_a
             else
               visios_all.reject { |visio| visio.archived?(@current_user) }
             end

    @authors = authors_from_visios(visios_all)

    visios = visios_filters_and_order(visios, index_params)
    @dataset = dataset_pagination(visios, index_params)
  end

  def generate_csv(dataset)
    CSV.generate do |csv|
      csv << [I18n.t('modules.bigbluebutton.visioname'),
              I18n.t('modules.bigbluebutton.comment'),
              I18n.t('modules.bigbluebutton.creator'),
              I18n.t('modules.bigbluebutton.startdate'),
              I18n.t('modules.bigbluebutton.duration'),
              I18n.t('modules.bigbluebutton.participant')]

      dataset[:items].each do |data|
        csv << [data.name,
                data.comment,
                data.owner&.name,
                data.date_start,
                data.duration,
                data.number_of_participants]
      end
    end
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_visio
    @visio = Visio.find(params[:id])
  rescue StandardError => e
    flash.alert = "#{I18n.t('modules.bigbluebutton.dontexist')}|#{e}"
    redirect_to visios_path
  else
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def visio_params
    params.require(:visio).permit(:name, :comment, :date_start, :duration, :number_of_participants,
                                  :owner_id, :url_visios, :allow_recording, :moderator_approval,
                                  :anyone_can_start, :anyone_join_as_moderator, :mute_on_start)
  end

  def set_url_local
    @config_module = ConfigModule.where(name: configmodule_name(Core::BigBlueButton)).first
    domaininfo = DomainInfo.where(config_module_id: @config_module.id,
                                  domain_id: current_user.domain.id).first
    @sc = ServiceContext.new(domaininfo, current_user.domain, current_user)
    @url_visios = Core::BigBlueButton::Main.get_login_url(@sc)
  end

  def set_bbb_session
    @session = Core::BigBlueButton::Main.get_session(@sc)
  end

  def authors_from_visios(visios)
    authors = visios.collect { |p| [p.owner&.name, p.owner_id] if p.owner }
    authors.uniq.compact
  end

  def index_params
    params.permit(:author, :order, :query, :page, :items_per_page, :archived)
  end

  def visios_filters_and_order(visios, params)
    if params[:author].present?
      visios.select! do |visio|
        visio.owner_id == params[:author]
      end
    end

    if params[:query].present?
      visios.select! do |visio|
        visio.name.downcase.include?(params[:query].downcase) or visio.comment.downcase.include?(params[:query].downcase)
      end
    end

    if params[:order].to_s.empty?
      visios.sort! { |a, b| b.date_start <=> a.date_start }
    else
      order_column, order_direction = params[:order].split(',')
      order_column = 'name' unless %w[name date_start].include?(order_column)
      order_direction = 'DESC' unless %w[ASC DESC].include?(order_direction)
      if order_column == 'name'
        visios.sort! { |a, b| b.name.downcase <=> a.name.downcase } if order_direction == 'ASC'
        visios.sort! { |a, b| a.name.downcase <=> b.name.downcase } if order_direction == 'DESC'
      else
        if order_direction == 'ASC'
          visios.sort! do |a, b|
            b.date_start <=> a.date_start
          end
        end
        if order_direction == 'DESC'
          visios.sort! do |a, b|
            a.date_start <=> b.date_start
          end
        end
      end
    end

    visios
  end

  def only_owner_or_invite!
    if (@visio.owner_id != @current_user.id) &&
       InvitationUser.where(module_id: params[:id], module_tag: 'visio', uuid: @current_user.id).count.zero? &&
       InvitationGroup.where(module_id: params[:id], module_tag: 'visio',
                             group_id: @current_user.groups_uuid).count.zero?
      redirect_to visios_path, notice: (I18n.t 'modules.bigbluebutton.authorizedaccesvisioconference').to_s
    end
  end
end
