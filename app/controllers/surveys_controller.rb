class SurveysController < ApplicationController
  before_action :set_survey, only: %i[show edit update destroy share new_message]
  before_action except: %i[show new_message] do
    only_authorized_users('Survey')
  end
  before_action :set_dataset, only: %i[index export]
  before_action :super_admin_only, only: %i[edit update]
  before_action :set_owner_survey, only: %i[destroy share invite_users invite_groups invite_emails]
  before_action only: %i[edit update destroy] do
    only_owner!(@survey.owner_id, surveys_path)
  end

  # GET /surveys
  # GET /surveys.json
  def index; end

  # GET /surveys/1
  # GET /surveys/1.json
  def show
    survey_options = SurveyOption.where(survey_id: @survey.id)
    @token = params[:token]
    @uuid = if @current_user
              @current_user.id
            elsif @token
              @token
            end
    has_invite = InvitationUser.where(module_id: @survey.id, module_tag: 'survey', uuid: @uuid)
    unless @survey.owner_id.eql?(@uuid) || has_invite&.count&.positive?
      redirect_to services_url, notice: (I18n.t 'modules.survey.noticeinvite').to_s
    end
    # @has_answered = SurveyAnswer.joins(:survey_option)
    # .where('survey_options.survey_id=? and uuid=?', @survey.id, @uuid)
    # .count.positive?

    awnsers = SurveyAnswer.joins(:survey_option)
                          .where('survey_options.survey_id': @survey.id)
    participants = awnsers.collect do |awnser|
      if awnser[:uuid].include?(UUID_FOR_EXTERNAL_SURVEY)

        user =  InvitationUser.where(module_id: @survey.id, module_tag: 'survey', uuid: awnser[:uuid]).first
        { uuid: awnser[:uuid], name: user.email, internal: false }
      else
        user = User.find(awnser[:uuid])
        { uuid: awnser[:uuid], name: user.name, internal: true }
      end
    end.uniq

    options = SurveyOption.where(survey_id: @survey.id)
    @answers_table = [options.collect { |option| option[:choice] }.unshift(nil)]
    participants.each do |participant|
      reponses = [participant[:name]]
      options.each do |option|
        reponses << SurveyAnswer.where(survey_option_id: option.id, uuid: participant[:uuid])
                                .count.positive?
      end
      @answers_table << reponses
    end

    @options = []
    max_answers = 0
    survey_options.each do |option|
      answers = SurveyAnswer.where(survey_option_id: option.id)
      max_answers = answers.count if answers.count > max_answers
      opt = { id: option.id, choice: option.choice, count: answers.count, users: [] }
      answers.each do |answer|
        user = User.find(answer.uuid)

        opt[:users] << if answer.uuid.start_with?(UUID_FOR_EXTERNAL_SURVEY)
                         ext_user = InvitationUser.where(module_id: @survey.id, module_tag: 'survey',
                                                         uuid: answer.uuid).first
                         { email: ext_user.email }
                       else
                         { name: user.name, id: user.id, me: (@uuid == user.id) }
                       end
      end
      @options << opt
    end
    max_answers = -1 if max_answers.zero?
    @options.each do |option|
      option[:best_choice] = max_answers == option[:count]
    end
    log_statistics('Survey', 'Survey')

    @user = if current_user
              current_user.name
            else
              InvitationUser.where(module_id: @survey.id, module_tag: 'survey',
                                   uuid: @uuid).first&.email
            end
    @list_messages = Message.where(module_id: @survey.id, module_tag: 'survey').order('created_at DESC')
    @message = Message.new
    @module_tag = 'surveys'
    @module_id = @survey.id
    @url_message = @token ? "/#{@module_tag}/#{@module_id}/new_message?token=#{@token}" : "/#{@module_tag}/#{@module_id}/new_message"

    logger.debug "Answers: #{@answers_table}"
  end

  # GET /surveys/new
  def new
    @survey = Survey.new
    @survey.end_date = DateTime.now + 1.week
  end

  # POST /surveys/1/share
  def share
    remove_users # Remove emails also
    remove_groups

    total1, errors1 = invite_users
    total2, errors2 = invite_groups
    total3, errors3 = invite_emails

    total = total1 + total2 + total3
    errors = errors1 + errors2 + errors3
    respond_to do |format|
      format.html do
        if errors.positive?
          redirect_to surveys_url,
                      errors: "Survey could not send emails to #{errors}/#{total}."
        else
          redirect_to surveys_url,
                      notice: "Survey successfully send #{total} emails."
        end
      end
      format.json do
        head :no_content
      end
    end
  end

  # GET /surveys/1/edit
  def edit; end

  # POST /surveys
  # POST /surveys.json
  def create
    @survey = Survey.new(survey_params)
    @survey.owner_id = @current_user.id
    @survey.uuid = random_uuid

    @survey.end_date = DateTime.strptime(survey_params[:end_date], '%Y-%m-%dT%H:%M')

    options_list = params[:options].nil? ? [] : JSON.parse(params[:options])

    if @survey.end_date < DateTime.now
      redirect_to surveys_url, alert: (I18n.t 'modules.survey.noticeendate').to_s
      return
    end

    if options_list.count < 2
      redirect_to surveys_url, alert: (I18n.t 'modules.survey.noticeoptions').to_s
      return
    end

    if @survey.save
      options_list.each do |o|
        logger.debug "Option: #{o}  "
        option = SurveyOption.new
        option.choice = o
        option.survey_id = @survey.id
        option.save
      end
      redirect_to surveys_path, notice: (I18n.t 'modules.survey.noticecreate').to_s
    else
      render :new
    end
  end

  # PATCH/PUT /surveys/1
  # PATCH/PUT /surveys/1.json
  def update; end

  # DELETE /surveys/1
  # DELETE /surveys/1.json
  def destroy
    if @survey.destroy
      redirect_to surveys_url, notice: (I18n.t 'modules.survey.noticedestroy').to_s
    else
      redirect_to surveys_url, alert: 'Error'
    end
  end

  def new_message
    @token = params[:token]
    @uuid = if @current_user
              @current_user.id
            elsif @token
              @token
            end

    message = Message.new(user_id: @uuid, module_id: @survey.id, module_tag: 'survey',
                          message: params[:message][:message])

    if message.save
      redirect_to survey_path(token: @token)
    else
      render :new
    end
  end

  def export
    send_data generate_csv(@dataset),
              type: 'text/csv; charset=utf-8; header=present',
              disposition: 'attachment; filename=surveys.csv'
  end

  private

  def set_dataset
    archived = params[:archived] == '1'
    now = DateTime.now

    id_survey_invite = InvitationUser.where(module_tag: 'survey', uuid: @current_user.id).distinct.collect(&:module_id)
    id_survey_invite += InvitationGroup.where(module_tag: 'survey',
                                              group_id: @current_user.groups_uuid).distinct.collect(&:module_id)
    surveys = Survey.where('owner_id = ? or id in (?) ', @current_user.id, id_survey_invite)
                    .where("end_date #{archived ? '<' : '>='} ?", now)

    @authors = authors_from_surveys(surveys)

    surveys = surveys_filters_and_order(surveys, index_params)
    @dataset = dataset_pagination(surveys, index_params)
  end

  def generate_csv(dataset)
    CSV.generate do |csv|
      csv << [I18n.t('modules.survey.titlesurvey'),
              I18n.t('modules.survey.descripsurvey'),
              I18n.t('modules.survey.creatorsurvey'),
              I18n.t('modules.survey.enddatesurvey'),
              I18n.t('modules.survey.anonymous_title')]

      dataset[:items].each do |data|
        csv << [data.question,
                data.comment,
                data.owner&.name,
                data.end_date,
                data.anonymous]
      end
    end
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_survey
    @survey = Survey.find(params[:id])
  end

  def set_owner_survey
    return unless @survey.owner_id != @current_user.id

    redirect_to '/surveys', notice: (I18n.t 'modules.survey.noticenotdestroy').to_s
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def survey_params
    params.require(:survey).permit(:end_date, :question, :comment, :allow_multiple, :anonymous)
  end

  def index_params
    params.permit(:author, :order, :query, :page, :items_per_page, :archived)
  end

  def authors_from_surveys(surveys)
    authors = surveys.collect { |p| [p.owner&.name, p.owner_id] if p.owner }
    authors.uniq.compact
  end

  def surveys_filters_and_order(surveys, params)
    surveys = surveys.where(owner_id: params[:author]) if params[:author].present?

    if params[:query].present?
      surveys = surveys.where('question ILIKE ? or comment ILIKE ?',
                              "%#{params[:query]}%",
                              "%#{params[:query]}%")
    end

    if params[:order]&.present?
      order_column, order_direction = params[:order].split(',')
      order_direction = 'DESC' unless %w[ASC DESC].include?(order_direction)
    else
      order_column = 'end_date'
      order_direction = 'DESC'
    end

    surveys.order("#{order_column} #{order_direction}")
  end
end
