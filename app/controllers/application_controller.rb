# frozen_string_literal: true

class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  include ApplicationHelper
  include HttpAcceptLanguage::AutoLocale
  protect_from_forgery with: :exception
  helper_method :current_user
  before_action :set_current_user
  before_action :set_modules
  around_action :switch_locale

  UUID_FOR_EXTERNAL_SURVEY = 'zourit-'
  UUID_FOR_USER_EXTERNAL_ACCES_TO_SERVICES = 'zourit-'

  def switch_locale(&)
    # logger.warn current_user.inspect
    locale = session[:user_locale] || I18n.default_locale
    I18n.with_locale(locale, &)
  end

  def current_user
    session[:user] ? User.new(session[:user].symbolize_keys!) : nil
  end

  def admin_only
    return if authorized_access?(@domain ? @domain.id : nil)

    redirect_to services_path, flash: { alert: (I18n.t 'application.law') }
  end

  def super_admin_only
    return if @current_user&.superadmin

    redirect_to services_path,
                notice: (I18n.t 'application.law').to_s
  end

  def user_only
    return if authorized_access?(@domain ? @domain.id : nil) || (@current_user && (@current_user.id == params[:id]))

    redirect_to services_path(url: (@current_user ? nil : request.path))
  end

  def only_logged_users
    redirect_to services_path(url: request.path) unless @current_user
  end

  def only_authorized_users(service_name)
    user = current_user
    user.populate_granted_modules
    session[:user] = user.to_hash if session[:user][:granted_modules] != user.granted_modules
    return if user&.granted_modules&.include?(service_name)

    redirect_to services_path,
                notice: (I18n.t 'application.law').to_s
  end

  def log_statistics(service_name, instance_name)
    Statistic.create(user_id: current_user&.id,
                     domain_id: current_user&.domain&.id,
                     service_name:,
                     instance_name:)
  rescue StandardError
    logger.error "Can't create statistic for #{instance_name}"
  end

  def dataset_pagination(dataset, params, default_items_per_page = 10)
    page = params[:page].present? ? params[:page].to_i : 1
    count = dataset.count
    items_per_page = params[:items_per_page].to_i
    items_per_page = default_items_per_page if items_per_page < default_items_per_page
    items_per_page = count if items_per_page > count

    start = (page - 1) * items_per_page
    stop = start + items_per_page - 1

    {
      items: dataset[start..stop],
      page:,
      items_per_page:,
      count:
    }
  end

  def only_owner!(owner_id, return_path)
    return if owner_id == @current_user.id

    redirect_to return_path,
                alert: I18n.t('application.law').to_s
  end

  private

  def set_current_user
    @current_user = current_user
  end

  def set_modules
    @modules = current_user&.modules unless turbo_frame_request?
  end
end
