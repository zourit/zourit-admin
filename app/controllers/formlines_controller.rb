class FormlinesController < ApplicationController
  before_action :get_form
  before_action :set_formline, only: %i[show edit update destroy]

  # GET /formlines
  # GET /formlines.json
  def index
    @formlines = Formline.where(form_id: @form.id).order(:order)
  end

  # GET /formlines/1
  # GET /formlines/1.json
  def show; end

  # GET /formlines/new
  def new
    @formline = @form.formlines.build
  end

  # GET /formlines/1/edit
  def edit; end

  # POST /formlines
  # POST /formlines.json
  def create
    @formline = @form.formlines.build(formline_params)

    respond_to do |format|
      if @formline.save
        format.html { redirect_to form_formlines_path(@form), notice: 'Formline was successfully created.' }
        format.json { render :show, status: :created, location: @formline }
      else
        format.html { render :new }
        format.json { render json: @formline.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /formlines/1
  # PATCH/PUT /formlines/1.json
  def update
    respond_to do |format|
      if @formline.update(formline_params)
        format.html { redirect_to form_formline_path(@form), notice: 'Formline was successfully updated.' }
        format.json { render :show, status: :ok, location: @formline }
      else
        format.html { render :edit }
        format.json { render json: @formline.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /formlines/1
  # DELETE /formlines/1.json
  def destroy
    @formline.destroy
    respond_to do |format|
      format.html { redirect_to form_formlines_path(@form), notice: 'Formline was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_formline
    @formline = @form.formlines.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def formline_params
    params.require(:formline).permit(:title, :description, :form_id, :field_type, :value, :order, :show_title)
  end

  def get_form
    @form = Form.find(params[:form_id])
  end
end
