# frozen_string_literal: true

class TicketsController < ApplicationController
  include UsersHelper
  before_action :set_domain
  before_action :set_ticket, only: %i[show edit update destroy new_message]
  before_action :set_instances_ticket, only: %i[new edit update]
  before_action :user_or_superadmin, only: %i[edit update destroy]
  before_action :admin_only
  before_action :set_autorize_show, only: %i[show]

  PROGRESS_BAR_STATUS_ID_MAP = { 1 => '0%', 2 => '50%', 3 => '100%', 4 => '100%' }.freeze

  # GET /tickets
  # GET /tickets.json
  def index
    @archived = params[:archived] == '1'

    tickets = TicketsHelper.get_tickets(@current_user, @archived)

    @ticket_authors = authors_from_tickets(tickets)
    @ticket_instances = instances_from_tickets(tickets)
    @ticket_statuses = statuses_from_tickets(tickets)
    @ticket_categories = categories_from_tickets(tickets)

    tickets = tickets_filters_and_order(tickets, index_params)

    @dataset = dataset_pagination(tickets, index_params)

    if turbo_frame_request?
      render partial: 'tickets', locals: { tickets: }
    else
      render :index
    end
  end

  # GET /tickets/1
  # GET /tickets/1.json
  def show
    @list_messages = Message.where(module_id: @ticket.id, module_tag: 'ticket')
                            .order('created_at DESC')
    @message = Message.new
    @module_id = @ticket.id
    @module_tag = 'tickets'
    @url_message = "/#{@module_tag}/#{@module_id}/new_message"

    @progress_bar_percent = PROGRESS_BAR_STATUS_ID_MAP[@ticket.ticket_status.id]

    @next_statuses = []

    return unless @current_user.id == @ticket.user_id || @current_user.superadmin

    if @ticket.ticket_status_id < 3
      @ticket.ticket_status.next_statuses.scan(/\d+/).map(&:to_i).each do |status|
        ticket_status = TicketStatus.find(status)
        @next_statuses.append [(I18n.t "tickets.status.state_#{ticket_status.id}").upcase,
                               I18n.t("tickets.status.icon_#{ticket_status.id}"), ticket_status.id]
      end
    else
      @next_statuses.append [(I18n.t 'tickets.status.state_5').upcase,
                             I18n.t('tickets.status.icon_5'), 1]
    end
  end

  # GET /tickets/new
  def new
    @ticket = Ticket.new
    # logger.warn 'this is an exception'
  end

  # GET /tickets/1/edit
  def edit; end

  # POST /tickets
  # POST /tickets.json
  def create
    @ticket = Ticket.new(ticket_params)
    @ticket.user_id = @current_user.id
    @ticket.domain_id = @current_user.domain.id
    @ticket.ticket_status_id = 1
    if @ticket.save
      redirect_to @ticket, notice: (I18n.t 'tickets.noticecreate')
      @message = Message.new(user_id: @ticket.user_id,
                             module_id: @ticket.id,
                             module_tag: 'ticket',
                             message: I18n.t('tickets.create_message', id: @ticket.id, title: @ticket.title))
      @message.save
      send_email(@ticket, true, @message.message)
    else
      render :new
    end
  end

  # POST /tickets/1/new_message
  def new_message
    if params[:message] && !%w[3 4].include?(@ticket.ticket_status.id)

      message = Message.new(user_id: current_user.id, module_id: @ticket.id, module_tag: 'ticket',
                            message: params[:message][:message])
      message.save
      send_email(@ticket, false, message.message)

    end
    redirect_to @ticket
  end

  # PATCH/PUT /tickets/1
  def update
    previous_title = @ticket.title
    previous_status = @ticket.ticket_status_id
    if @ticket.update(ticket_params)

      redirect_to @ticket, flash: { notice: (I18n.t 'tickets.noticeupdate') }

      if @ticket.ticket_status_id != previous_status
        status_label = I18n.t("tickets.status.state_#{@ticket.ticket_status.id}").capitalize
        message = I18n.t('tickets.change.status', label: status_label)
        Message.create(user_id: @current_user.id, module_id: @ticket.id, module_tag: 'ticket', message:)
        send_email(@ticket, false, message)
      end

      if @ticket.title != previous_title
        message = I18n.t('tickets.change.title', title: @ticket.title)
        Message.create(user_id: @current_user.id, module_id: @ticket.id, module_tag: 'ticket', message:)
        send_email(@ticket, false, message.message)
      end

    else
      # ERRORS
      render :edit
    end
  end

  # DELETE /tickets/1
  def destroy
    @ticket.destroy
    redirect_to tickets_url, notice: (I18n.t 'tickets.noticedestroy')
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_ticket
    @ticket = Ticket.find(params[:id])
  end

  # Never trust parameters from the scary internet, only  allow the white list through.
  def ticket_params
    params.require(:ticket).permit(:user_id, :ticket_status_id, :ticket_category_id, :instance_id, :domain_id, :title)
  end

  def index_params
    params.permit(:author, :instance, :status, :category, :order, :page, :items_per_page, :archived)
  end

  def authors_from_tickets(tickets)
    authors = tickets.collect { |p| [p.user&.full_name_elipse, p.user_id] if p.user }
    authors.uniq.compact
  end

  def instances_from_tickets(tickets)
    instances = tickets.collect { |p| [p.instance&.name, p.instance_id] if p.instance }
    instances.uniq.compact
  end

  def statuses_from_tickets(tickets)
    statuses = tickets.collect do |p|
      [I18n.t("tickets.status.state_#{p.ticket_status.id}"), p.ticket_status_id] if p.ticket_status
    end
    statuses.uniq.compact
  end

  def categories_from_tickets(tickets)
    categories = tickets.collect { |p| [p.ticket_category&.libelle, p.ticket_category_id] if p.ticket_category }
    categories.uniq.compact
  end

  def tickets_filters_and_order(tickets, params)
    tickets = tickets.where(user_id: params[:author]) if params[:author].present?
    tickets = tickets.where(instance_id: params[:instance]) if params[:instance].present?
    tickets = tickets.where(ticket_status_id: params[:status]) if params[:status].present?
    tickets = tickets.where(ticket_category_id: params[:category]) if params[:category].present?

    if params[:query].present?
      tickets = tickets.where('user_id ILIKE ? or comment ILIKE ?',
                              "%#{params[:query]}%",
                              "%#{params[:query]}%")
    end

    if params[:order]&.present?
      order_column, order_direction = params[:order].split(',')
      order_direction = 'DESC' unless %w[ASC DESC].include?(order_direction)
    else
      order_column = 'tickets.created_at'
      order_direction = 'DESC'
    end

    tickets.order("#{order_column} #{order_direction}")
  end

  # def user_domain_or_superadmin
  # access = (@current_user.domain.id == @ticket.domain_id) || @current_user.superadmin
  # redirect_to tickets_path, notice: (I18n.t 'status.law').to_s unless access
  # end
  def set_domain
    @domain = current_user.domain
  end

  def user_or_superadmin
    access = (@current_user.id == @ticket.user_id) || @current_user.superadmin
    redirect_to tickets_path, notice: (I18n.t 'status.autorize').to_s unless access
  end

  def set_autorize_show
    access = (@current_user.domain.id == @ticket.domain_id) || @current_user.superadmin
    redirect_to tickets_path, notice: (I18n.t 'status.autorize').to_s unless access
  end

  def set_instances_ticket
    @instances_ticket = DomainInfo.where(domain_id: @current_user.domain.id).map(&:instance)
  end

  def send_email(ticket, createboolean, msg)
    url = "https://#{request.host_with_port}/tickets/#{ticket.id}"

    if createboolean
      emails = superadmins_emails

      TicketUserNotifier.email_creation_ticket(@current_user, emails, ticket, url, msg).deliver_now
    else
      emails = ticket.emails_list_ticket
      TicketUserNotifier.email_notif_ticket(@current_user, emails, ticket, url, msg).deliver_now
    end
  end
end
