class SurveyAnswersController < ApplicationController
  before_action :set_survey_answer, only: %i[show edit update destroy]

  # TODO: A enlever

  def index
    @survey_answers = SurveyAnswer.all
  end

  # GET /answers/1
  # GET /answers/1.json

  # TODO: A enlever

  def show; end

  # GET /answers/new
  def new
    @survey_answer = SurveyAnswer.new
  end

  # POST /answers
  # POST /answers.json
  def create
    uuid = params[:uuid]
    survey_id = params[:survey_id]
    survey = Survey.find(survey_id)
    SurveyAnswer.joins(:survey_option)
                .where('survey_options.survey_id': survey.id, uuid: uuid)
                .destroy_all
    if InvitationUser.where(module_id: survey_id, module_tag: 'survey', uuid: uuid).count.positive? || (survey.owner_id == uuid)
      params[:answers]&.each do |option_id|
        survey_answer = SurveyAnswer.new
        survey_answer.survey_option_id = option_id
        survey_answer.uuid = uuid
        survey_answer.save
      end
      respond_to do |format|
        format.html do
          if uuid.start_with?(UUID_FOR_EXTERNAL_SURVEY)
            redirect_to controller: 'surveys', action: 'show', id: survey.id, token: uuid
          else
            redirect_to survey, notice: (I18n.t 'modules.survey.answercreate').to_s
          end
        end
        format.json { render :show, status: :created, location: @survey_answer }
      end
    else
      redirect_to surveys_path, notice: (I18n.t 'application.law').to_s
    end
  end

  # PATCH/PUT /answers/1
  # PATCH/PUT answers/1.json
  def update # TODO: A enlever
    respond_to do |format|
      if @survey_answer.update(survey_answer_params)
        format.html { redirect_to @survey_answer, notice: (I18n.t 'modules.survey.answerupdate').to_s }
        format.json { render :show, status: :ok, location: @survey_answer }
      else
        format.html { render :edit }
        format.json { render json: @survey_answer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /answers/1
  # DELETE /answers/1.json
  def destroy # TODO: A enlever
    @survey_answer.destroy
    respond_to do |format|
      format.html { redirect_to @survey_answer, notice: (I18n.t 'modules.survey.answerdestroy').to_s }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_survey_answer
    @survey_answer = SurveyAnswer.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def survey_answer_params
    params.require(:survey_answer).permit(:survey_option_id, :uuid)
  end
end
