class VisioSettingsController < ApplicationController
  before_action do
    only_authorized_users('BigBlueButton')
  end
  before_action :set_visio, only: %i[index]
  before_action :set_url_local, only: %i[index]
  before_action :set_bbb_session, only: %i[index]
  before_action only: %i[index] do
    only_owner!(@visio.owner_id, visios_path)
  end

  # GET /visios/1/settings
  # GET /visios/1/settings.json
  def index
    if (@visio.owner_id != @current_user.id) &&
       InvitationUser.where(module_id: params[:id], module_tag: 'visio', uuid: @current_user.id).count.zero? &&
       InvitationGroup.where(module_id: params[:id], module_tag: 'visio',
                             group_id: @current_user.groups_uuid).count.zero?
      redirect_to visios_path, notice: (I18n.t 'modules.bigbluebutton.authorizedaccesvisioconference').to_s
    end
    redirect_to services_path, notice: (I18n.t 'modules.bigbluebutton.modulevisible').to_s unless @url_visios
    log_statistics(@sc.domain_info.config_module.name, @sc.domain_info.instance.name)

    display_name = "#{@current_user.first_name} #{@current_user.last_name}"
    begin
      @visio_link = Core::BigBlueButton::Main.start_meeting(@sc, @session, @visio.friendly_id, display_name)
    rescue RestClient::NotAcceptable
      redirect_to visios_path, notice: (I18n.t 'modules.bigbluebutton.visioconferencedoesnotexists').to_s
    end
  end

  # POST /visios/1/settings/generate_viewer_access_code
  # POST /visios/1/settings/generate_viewer_access_code.json
  def generate_viewer_access_code
    Core::BigBlueButton::Main.update_room_settings(@sc, @session, @visio.friendly_id, 'glViewerAccessCode', 'true')
  end

  # POST /visios/1/settings/generate_moderator_access_code
  # POST /visios/1/settings/generate_moderator_access_code.json
  def generate_moderator_access_code
    Core::BigBlueButton::Main.update_room_settings(@sc, @session, @visio.friendly_id, 'glModeratorAccessCode', 'true')
  end

  # POST /visios/1/settings
  # POST /visios/1/settings.json
  def update_setting
    Core::BigBlueButton::Main.update_room_settings(@sc, @session, @visio.friendly_id,
                                                   params[:setting_name],
                                                   params[:setting_value])
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_visio
    @visio = Visio.find(params[:id])
  rescue StandardError => e
    flash.alert = "#{I18n.t('modules.bigbluebutton.dontexist')}|#{e}"
    redirect_to visios_path
  else
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def visio_params
    params.require(:visio).permit(:name, :comment, :date_start, :duration, :number_of_participants,
                                  :owner_id, :url_visios)
  end

  def set_url_local
    @config_module = ConfigModule.where(name: configmodule_name(Core::BigBlueButton)).first
    domaininfo = DomainInfo.where(config_module_id: @config_module.id,
                                  domain_id: current_user.domain.id).first
    @sc = ServiceContext.new(domaininfo, current_user.domain, current_user)
    @url_visios = Core::BigBlueButton::Main.get_login_url(@sc)
  end

  def set_bbb_session
    @session = Core::BigBlueButton::Main.get_session(@sc)
    @settings = Core::BigBlueButton::Main.get_room_settings(@sc, @session, @visio.friendly_id)
  end

  def index_params
    params.permit(:author, :order, :query, :page, :items_per_page, :archived)
  end
end
