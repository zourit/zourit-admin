class PadsController < ApplicationController
  # before_action :reload_current_user, only: %i[new edit]
  before_action do
    only_authorized_users('Etherpad')
  end
  before_action :set_dataset, only: %i[index export]
  before_action :set_pad, only: %i[show edit update destroy archive]
  before_action only: %i[edit update destroy] do
    only_owner!(@pad.owner_id, pads_path)
  end

  # GET /pads
  def index; end

  # GET /pads/1
  # GET /pads/1.json
  def show
    @type_page = 'iframe'
    is_owner = (@pad.owner_id == @current_user.id)
    user_invitation = InvitationUser.where(module_id: @pad.id, module_tag: 'pad',
                                           uuid: @current_user.id).count.positive?
    group_invitation = InvitationGroup.where(module_id: @pad.id, module_tag: 'pad',
                                             group_id: @current_user.groups_uuid).count.positive?
    unless is_owner || user_invitation || group_invitation
      redirect_to pads_path, alert: I18n.t('modules.etherpad.noparticipe')
    end
    @url_embedded = @pad.full_url
    configmodule = ConfigModule.where(name: configmodule_name(Core::Etherpad)).first
    domain_info = DomainInfo.where(domain_id: current_user.domain.id, config_module_id: configmodule.id).first
    log_statistics(configmodule.name, domain_info.instance.name)
  end

  # GET /pads/new
  def new
    @pad = Pad.new
  end

  # GET /pads/1/edit
  def edit; end

  # POST /pads
  # POST /pads.json
  def create
    @pad = Pad.new(pad_params)
    @pad.uuid = random_uuid
    @pad.owner_id = @current_user.id

    configmodule = ConfigModule.where(name: configmodule_name(Core::Etherpad)).first
    domain_info = DomainInfo.where(domain_id: current_user.domain.id, config_module_id: configmodule.id).first
    url_server = domain_info.instance.option_by_name('url')
    @pad.full_url = url_server
    @pad.full_url += '/' unless @pad[-1] == '/'
    @pad.full_url += @pad.uuid

    if @pad.save
      redirect_to @pad, notice: I18n.t('modules.etherpad.noticecreate', pad_name: @pad.name).to_s
    else
      render :new, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /pads/1
  # PATCH/PUT /pads/1.json
  def update
    if @pad.update(pad_params)
      redirect_to pads_path, notice: I18n.t('modules.etherpad.noticeupdate', pad_name: @pad.name).to_s
    else
      render :edit
    end
  end

  # DELETE /pads/1
  # DELETE /pads/1.json
  def destroy
    @pad.destroy
    redirect_to pads_url, notice: I18n.t('modules.etherpad.noticedestroy').to_s
  end

  # POST /pads/1/archive
  def archive
    @pad.archived?(@current_user) ? @pad.unarchive(@current_user) : @pad.archive(@current_user)
    redirect_to pads_url
  end

  def export
    send_data generate_csv(@dataset),
              type: 'text/csv; charset=utf-8; header=present',
              disposition: 'attachment; filename=pads.csv'
  end

  private

  def set_dataset
    id_pad_invite = InvitationUser.where(module_tag: 'pad', uuid: @current_user.id).distinct.collect(&:module_id)
    id_pad_invite += InvitationGroup.where(module_tag: 'pad',
                                           group_id: @current_user.groups_uuid).distinct.collect(&:module_id)

    pads_all = Pad.where('owner_id = ? or pads.id in (?)', @current_user.id,
                         id_pad_invite).distinct
    pads = if index_params[:archived] == '1'
             pads_all.joins(:pad_archives)
                     .where('pad_archives.user_id = ?', @current_user.id)
                     .distinct.to_a
           else
             pads_all.reject { |pad| pad.archived?(@current_user) }
           end

    @authors = authors_from_pads(pads_all)

    pads = pads_filters_and_order(pads, index_params)
    @dataset = dataset_pagination(pads, index_params)
  end

  def generate_csv(dataset)
    CSV.generate do |csv|
      csv << [t('modules.etherpad.padname'),
              t('modules.etherpad.comment'),
              t('modules.etherpad.creator'),
              t('modules.etherpad.created_at')]
      dataset[:items].each do |data|
        csv << [data.name,
                data.comment,
                data.owner&.name,
                data.created_at]
      end
    end
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_pad
    @pad = Pad.find(params[:id])
  rescue StandardError => e
    flash.alert = "#{I18n.t('modules.etherpad.dontexist')}|#{e}"
    redirect_to pads_path
  else
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def pad_params
    params.require(:pad).permit(:name, :comment, :uuid, :owner_id)
  end

  def index_params
    params.permit(:author, :order, :query, :page, :items_per_page, :archived)
  end

  def authors_from_pads(pads)
    authors = pads.collect { |p| [p.owner&.name, p.owner_id] if p.owner }
    authors.uniq.compact
  end

  def pads_filters_and_order(pads, params)
    if params[:author].present?
      pads.select! do |pad|
        pad.owner_id == params[:author]
      end
    end

    if params[:query].present?
      pads.select! do |pad|
        pad.name.downcase.include?(params[:query].downcase) or pad.comment.downcase.include?(params[:query].downcase)
      end
    end

    if params[:order].to_s.empty?
      pads.sort! { |a, b| b.created_at <=> a.created_at }
    else
      order_column, order_direction = params[:order].split(',')
      order_column = 'name' unless %w[name created_at].include?(order_column)
      order_direction = 'DESC' unless %w[ASC DESC].include?(order_direction)
      if order_column == 'name'
        pads.sort! { |a, b| b.name.downcase <=> a.name.downcase } if order_direction == 'ASC'
        pads.sort! { |a, b| a.name.downcase <=> b.name.downcase } if order_direction == 'DESC'
      else
        if order_direction == 'ASC'
          pads.sort! do |a, b|
            b.created_at <=> a.created_at
          end
        end
        if order_direction == 'DESC'
          pads.sort! do |a, b|
            a.created_at <=> b.created_at
          end
        end
      end
    end

    pads
  end
end
