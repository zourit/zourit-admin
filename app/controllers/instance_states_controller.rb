class InstanceStatesController < ApplicationController
  before_action :set_instance_state, only: %i[show edit update destroy]
  before_action :super_admin_only

  # GET /instance_states
  # GET /instance_states.json
  def index
    @instance_states = InstanceState.all
  end

  # GET /instance_states/1
  # GET /instance_states/1.json
  def show; end

  # GET /instance_states/new
  def new
    @instance_state = InstanceState.new
  end

  # GET /instance_states/1/edit
  def edit; end

  # POST /instance_states
  # POST /instance_states.json
  def create
    @instance_state = InstanceState.new(instance_state_params)

    respond_to do |format|
      if @instance_state.save
        format.html { redirect_to @instance_state, notice: (I18n.t 'instance_states.noticecreate').to_s }
        format.json { render :show, status: :created, location: @instance_state }
      else
        format.html { render :new }
        format.json { render json: @instance_state.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /instance_states/1
  # PATCH/PUT /instance_states/1.json
  def update
    respond_to do |format|
      if @instance_state.update(instance_state_params)
        format.html { redirect_to @instance_state, notice: (I18n.t 'instance_states.noticeupdate').to_s }
        format.json { render :show, status: :ok, location: @instance_state }
      else
        format.html { render :edit }
        format.json { render json: @instance_state.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /instance_states/1
  # DELETE /instance_states/1.json
  def destroy
    @instance_state.destroy
    respond_to do |format|
      format.html { redirect_to instance_states_url, notice: (I18n.t 'instance_states.noticedestroy').to_s }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_instance_state
    @instance_state = InstanceState.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def instance_state_params
    params.require(:instance_state).permit(:title, :importance)
  end
end
