class DatesController < ApplicationController
  before_action :only_logged_users

  # GET /dates
  def index
    # TODO: à supprimer
    configmodule = ConfigModule.where(name: 'Framadate').first
    if configmodule&.url
      @url_embedded = configmodule.url
    else
      redirect_to services_path, notice: (I18n.t 'application.noticenoactive').to_s
    end
  end
end
