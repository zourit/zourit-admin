class InvitationsController < ApplicationController
  include InvitationsHelper
  before_action :set_invitation, only: %i[index invite users sendtomails destroy]
  before_action :set_dataset, only: %i[index]
  before_action only: %w[index invite users sendtomails] do
    only_owner!(@module.owner_id, "/#{params[:tag]}")
  end

  def index
    if turbo_frame_request?
      render partial: 'groups/users'
    else
      render :index
    end
  end

  def destroy
    invitationdelect =
      if params[:type_id].split('_').first == 'group'
        InvitationGroup.find(params[:type_id].split('_').last)
      else
        InvitationUser.find(params[:type_id].split('_').last)
      end
    tag = invitationdelect.module_tag
    module_id = invitationdelect.module_id
    invitationdelect.destroy

    redirect_to "/invitations/#{tag}s/#{module_id}",
                notice: I18n.t('invitations.destroy_success')
  end

  def share_invitation_to_email
    type_id = params[:type_id].split('_')
    group = type_id.first == 'group'
    invite = if group
               InvitationGroup.find(type_id.last)
             else
               InvitationUser.find(type_id.last)
             end
    email = defined?(invite.email) ? invite.email : Group.find(invite.group_id).name
    uuid = defined?(invite.uuid) ? invite.uuid : nil
    send_email(invite.module_tag, invite.module_id, uuid, email, group)

    respond_to do |format|
      format.html do
        redirect_to "/invitations/#{invite.module_tag}s/#{invite.module_id}",
                    notice: I18n.t('invitations.send_success')
      end
      format.json { head :no_content }
    end
  end

  def invite
    total1, errors1 = params[@tag][:invitation_users] ? invite_users : [0, 0]
    total2, errors2 = params[@tag][:invitation_groups] ? invite_groups : [0, 0]
    total3, errors3 = params[:invitation_external] ? invite_external : [0, 0]
    total = total1 + total2 + total3
    errors = errors1 + errors2 + errors3
    logger.info "Total: #{total}"
    logger.info "Error: #{errors}"
    respond_to do |format|
      format.html do
        if errors.positive?

          redirect_to "/invitations/#{@tag}s/#{@module.id}",
                      errors: "#{errors}/#{total} " +
                              (I18n.t 'invitations.errormessageshare').to_s
        else
          redirect_back(fallback_location: "/invitations/#{@tag}s/#{@module.id}",
                        notice: ((I18n.t 'invitations.send_success')).to_s)

        end
      end
      format.json do
        head :no_content
      end
    end
  end

  private

  def set_invitation
    @class_module = params[:tag].to_s.capitalize.chop
    @module = eval(@class_module).find(params[:id])
    @tag = params[:tag].to_s.chop
    @groups_share = InvitationGroup.where(module_id: params[:id], module_tag: @tag)
    @users_share = InvitationUser.where(module_id: params[:id], module_tag: @tag)
  end

  def set_dataset
    @invitations_list = []
    @groups_share.each do |group|
      group_ldap = Group.find(group.group_id)
      next unless group_ldap

      @invitations_list << { name: group_ldap.short_name.upcase,
                             email: group_ldap.name,
                             id: group.id,
                             title: 'Groupe',
                             icone: 'users',
                             type: 'group' }
    end
    @users_share.each do |user|
      user_ldap = User.find(user.uuid) if user.internal
      @invitations_list << { name: user.internal ? user_ldap&.full_name_elipse || '' : user&.email,
                             email: user.email,
                             id: user.id,
                             title: (user.internal ? 'Membre' : 'Externe'),
                             icone: (user.internal ? 'user' : 'globe'),
                             type: 'user' }
    end
    @invitations_list.sort! { |a, b| a[:name].downcase <=> b[:name].downcase }

    users = User.list_from_domain(@current_user.domain).sort_by(&:last_name).reject! { |user| user_invitation(user.id) }
    @groups = @current_user.list_groups(@current_user.domain.name, false, true).sort_by(&:name).reject do |group|
      group_invitation(group.id)
    end
    members_ids = @invitations_list.map { |inv| inv[:id] }
    users.reject! do |u|
      members_ids.include?(u.id)
    end
    if params[:exclude]
      users.reject! do |u|
        params[:exclude].split(',').include?(u.id)
      end
    end
    if params[:query]
      users.select! do |u|
        u.name.downcase.include?(params[:query].downcase) or u.email.include?(params[:query].downcase)
      end
    end
    @dataset = dataset_pagination(users, params)
  end

  def send_email(tag, module_id, uuid, email, group)
    domain_id =  @current_user.domain.id
    configmodule = ConfigModule.where(url_local: "/#{tag}s").first
    service_obj = eval(tag.to_s.capitalize).find(module_id)
    domain_info = DomainInfo.where(domain_id:, config_module_id: configmodule.id).first
    logger.warn configmodule.inspect
    logger.warn domain_id
    logger.warn domain_info.inspect
    host = request.host_with_port.to_s
    url = if !uuid && tag == 'visio'
            "#{domain_info.instance.option_by_name('url')}/rooms/#{service_obj.friendly_id}/join"
          elsif !uuid
            "#{domain_info.instance.option_by_name('url')}/#{service_obj.uuid}"
          elsif uuid.start_with?(UUID_FOR_USER_EXTERNAL_ACCES_TO_SERVICES) && group == false
            "https://#{host}#{configmodule.url_local}/#{service_obj.id}?token=#{uuid}"
          else
            "https://#{host}#{configmodule.url_local}/#{service_obj.id}"
          end
    InvitationUserNotifier.invit_email(@current_user, email, service_obj, url, host,
                                       tag).deliver_now
  end

  def invite_users
    users = params[@tag][:invitation_users].split(',')

    return 0, 0 unless users

    troubles = []
    users&.each do |uuid|
      email = User.find(uuid).email
      unless user_invitation(uuid)
        InvitationUser.create(module_id: params[:id], module_tag: @tag, uuid:,
                              email:, internal: true)
        send_email(@tag, params[:id], uuid, email, false)
      end
    end
    [users.count, troubles.count]
  end

  def invite_groups
    groups = params[@tag][:invitation_groups].split(',')
    return 0, 0 unless groups

    troubles = []
    groups&.each do |group_id|
      unless group_invitation(group_id)
        email = Group.find(group_id).name
        InvitationGroup.create(module_id: params[:id], module_tag: @tag, group_id:)

        send_email(@tag, params[:id], group_id, email, true)
      end
    end
    [groups.count, troubles.count]
  end

  def invite_external
    emails = params[:invitation_external].split(",")
    return 0, 0 unless emails

    troubles = []
    emails.each do |email|
      ext_uuid = nil
      next if external_user_invitation(email)

      module_id = params[:id]
      if @tag == 'survey'
        ext_uuid = UUID_FOR_USER_EXTERNAL_ACCES_TO_SERVICES +
                   random_uuid
        InvitationUser.create(module_id:, module_tag: @tag, email:,
                              uuid: ext_uuid, internal: false)
      else
        InvitationUser.create(module_id:, module_tag: @tag,
                              email:, internal: false)
      end
      send_email(@tag, module_id, ext_uuid, email, false)
    end
    [emails.count, troubles.count]
  end
end
