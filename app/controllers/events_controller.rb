class EventsController < ApplicationController
  include StatusHelper

  before_action :only_domain_with_module, only: %i[index show]
  before_action :super_admin_only, only: %i[create update destroy new_message]
  before_action :set_event, only: %i[edit update destroy messages new_message]

  PROGRESS_BAR_STATUS_ID_MAP = { 1 => '0%', 2 => '50%', 3 => '100%', 4 => '100%' }.freeze

  # GET /events
  def index
    instances = get_instances(current_user)
    instances = instances_filters_and_order(instances, instances_params)

    @dataset = {
      items: instances,
      page: 1,
      items_per_page: instances.count,
      count: instances.count
    }

    return unless turbo_frame_request?

    logger.warn('AVEC turbo frame')
    # TODO: line below to remove
    # render partial: 'instances', locals: { instances: }
    render partial: 'instances'
  end

  # GET /events/new
  def new
    @event = Event.new instance_id: params[:id]
    @instance = Instance.find(@event.instance_id)

    @event.start_date = DateTime.now
  end

  # GET /events/1/edit
  def edit
    @event = Event.find(params[:id])
    @instance = Instance.find(@event.instance_id)
  end

  # GET/status/1
  def show
    @instance = Instance.find(params[:id])
    @instances = get_instances(current_user)
    redirect_to services_url, alert: (I18n.t 'events.alertaccess').to_s unless @instances.include?(@instance)

    events = Event.where(instance_id: @instance.id).order(start_date: :desc)
    @dataset = dataset_pagination(events, events_params)
  end

  # POST /events
  def create
    unless current_user&.superadmin
      redirect_to "/status/#{@event.instance_id}",
                  alert: (I18n.t 'events.alertcreate').to_s
    end

    @event = build_new_event
    if @event.save && @event.instance.save
      redirect_to "/status/#{@event.instance_id}",
                  notice: (I18n.t 'events.noticecreate').to_s
    else
      render :new
    end
  end

  # PATCH/PUT /events/1
  def update
    @event.assign_attributes(event_params)
    @event.start_date = DateTime.now if @event.events_state_id == 2
    @event.end_date = DateTime.now if @event.events_state_id >= 3
    @event.save

    message = "Changement de statut : #{I18n.t("events.messages.state_#{@event.events_state_id}").capitalize}"
    Message.create(user_id: @current_user.id, event_id: @event.id, message:)
    logger.info(message)

    if @event.events_state_id >= 3
      instance = @event.instance
      instance.instance_state_id = 1
      instance.save
      logger.info "status of instance <#{instance.name}> switches back to OPERATIONAL"
    end

    if @event.events_state_id >= 3
      redirect_to show_instance_status_path(@event.instance)
    else
      redirect_to show_instance_event_path(@event.instance, @event)
    end
  end

  # DELETE /events/1
  def destroy
    instance = Instance.find(@event.instance_id)
    @event.destroy
    redirect_to "/status/#{instance.id}",
                notice: (I18n.t 'events.noticedestroy').to_s
  end

  # GET /status/1/2
  def messages
    @list_messages = Message.where(event_id: @event.id)
                            .order('created_at DESC')
    @message = Message.new
    @url_message = "/status/#{@event.instance_id}/events/#{@event.id}"
    @progress_bar_percent = PROGRESS_BAR_STATUS_ID_MAP[@event.events_state_id]
  end

  # POST /status/1/2
  def new_message
    if params[:message]
      message = Message.new(user_id: current_user.id,
                            event_id: @event.id,
                            module_tag: 'event',
                            message: params[:message][:message])
      message.save
    end
    redirect_to "/status/#{@event.instance_id}/events/#{@event.id}"
  end

  private

  def build_new_event
    event = Event.new(event_params)
    event.instance_id = params[:id]
    event.start_date = DateTime.strptime(event_params[:start_date], '%Y-%m-%dT%H:%M')
    event.end_date = DateTime.strptime(event_params[:end_date], '%Y-%m-%dT%H:%M') unless event_params[:end_date].empty?
    event.events_state_id = event.start_date <= DateTime.now ? 2 : 1
    event.instance.instance_state_id = event_params[:instance_state_id]
    event
  end

  def update_event_from_params(event)
    start_date = DateTime.strptime(event_params[:start_date], '%m/%d/%Y %l:%M %p')
    end_date = DateTime.strptime(event_params[:end_date], '%m/%d/%Y %l:%M %p')
    event.start_date = start_date
    event.end_date = end_date
    event.message = event_params[:message]
    event.instance_id = event_params[:instance_id]
    event.instance_state_id = event_params[:instance_state_id]
    event
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_event
    @event = Event.find(params[:event_id])
  end

  def instances_params
    params.permit(:name, :comment, :order, :query, :page, :items_per_page)
  end

  def events_params
    params.permit(:name, :comment, :order, :query, :page, :items_per_page)
  end

  def event_params
    params.require(:event).permit(:message, :instance_id, :instance_state_id, :events_state_id, :start_date, :end_date)
  end

  # Si le client n'a pas de modules et n'est pas super admin, on l'empêche d'accéder à /status
  def only_domain_with_module
    if current_user && !current_user.superadmin && DomainInfo.where(domain_id: current_user.domain.id).count.zero?
      redirect_to services_url, notice: (I18n.t 'status.noticemod').to_s
    end
  end

  def instances_filters_and_order(instances, _params)
    instances.sort! { |a, b| a.name.downcase <=> b.name.downcase }
  end
end
