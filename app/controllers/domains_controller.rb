class DomainsController < ApplicationController
  include DomainsHelper
  include UserOptionsHelper

  before_action :set_domain
  before_action :set_dataset, only: %i[index export]
  before_action :set_users_dataset, only: %i[show users_export]
  before_action { admin_only }

  # GET /domains
  # GET /domains.json
  def index; end

  # GET /domains/1/administrators
  # GET /domains/1/administrators.json
  def administrators
    @administrators = @domain.administrators
  end

  # GET /domains/1/:tag
  def show; end

  # GET /domains/new
  def new
    @domain = Domain.new
    @domain.zouritDomainQuota = nil
  end

  # GET /domains/1/edit
  def edit; end

  # POST /domains
  # POST /domains.json
  def create
    domain = Domain.new
    parameters = domain_params
    domain.name = parameters['name']
    domain.zouritDomainQuota = parameters['zouritDomainQuota']
    domain.details.full_name = parameters['full_name']
    domain.details.phone = parameters['phone']
    domain.details.address = parameters['address']
    domain.details.email = parameters['email']
    domain.details.contact = "#{parameters['admin_firstname']} #{parameters['admin_lastname']}"

    logger.info("creating new domain <#{domain.name}>...")
    if domain.save
      user = User.new
      user.first_name = parameters['admin_firstname']
      user.last_name = parameters['admin_lastname']
      user.email = "#{parameters['admin_email']}@#{domain.name}"
      user.admin = true
      user.password = parameters['admin_password']

      user.password_confirmation = parameters['admin_password_confirmation']

      logger.info("creating user admin <#{user.email}> for domain <#{domain.name}>...")

      user.save!
      logger.info("domain <#{domain.name}> created successfully!")

      redirect_to domains_path, flash: { success: I18n.t('domains.create.create_success') }
    else
      logger.error("unable to create domain <#{domain.name}>")
      redirect_to domains_path, alert: I18n.t('domains.create.create_error')
    end
  end

  # PATCH/PUT /domains/1
  def update
    @domain.set_attributed_quota
    @domain.zouritDomainQuota = params['domain']['zouritDomainQuota']
    if @domain.zouritAttributedQuota.to_i <= @domain.zouritDomainQuota.to_i
      if @domain.save
        redirect_to @domain, flash: { success: I18n.t('domain.noticeupdate') }
      else
        render :edit
      end
    else
      redirect_to @domain, alert: I18n.t('domain.errorquota')
    end
  end

  # DELETE /domains/1
  def destroy
    respond_to do |format|
      format.html do
        if @domain.destroy
          redirect_to domains_url, flash: { success: I18n.t('domain.noticedestroy') }
        else
          redirect_to domains_url, flash: { alert: I18n.t('domain.notice_error_destroy') }
        end
      end
      format.json { head :no_content }
    end
  end

  # GET /domains/:id/users/:user_id/new
  def new_user
    @user = User.new
    @domain.set_attributed_quota
    if @domain.zouritQuotaLeft.negative?
      redirect_to domain_path(@domain.id),
                  notice: (I18n.t 'domains.dialog_domain_statistics.overquota')
    end
    @div_options_ids = {}
    @domain.config_modules.each do |mod|
      @div_options_ids[mod.id] = true
    end

    # On retire les modules activés automatiquement du quota restant
    default_quotas_sum = @domain.config_modules.map do |mod|
      meta_option = mod.user_options_quota
      meta_option ? meta_option.module_default_quota : 0
    end.reduce(0, :+)
    @quota_left = @domain.zouritQuotaLeft - default_quotas_sum
  end

  # GET /domains/:id/users/:user_id/edit
  def edit_user
    logger.info("edit domain #{params[:domain_id]}, user #{params[:user_id]}")
    @user = User.find(params[:user_id])
    @domain = @user.domain
    @domain.set_attributed_quota
    @div_options_ids = {}
    @domain.config_modules.each do |mod|
      @div_options_ids[mod.id] = @user.enabled?(mod)
    end

    @quota_left = @domain.zouritQuotaLeft
  end

  # POST /domains/:id/users/:user_id
  def update_user
    user = User.find(params[:user_id])
    res = update_user_helper(user, params)
    if res[:result]
      redirect_to domain_path(user.domain.id), flash: { notice: res[:message] }
    else
      redirect_to edit_domain_user_path(user.domain.id, user.id), flash: { alert: res[:message] }
    end
  end

  def create_user
    res = create_user_helper(params, @domain)
    if res[:result]
      redirect_to domain_path(@domain.id), flash: { success: res[:message] }
    else
      redirect_to new_domain_user_path(@domain.id), flash: { alert: res[:message] }
    end
  end

  def delete_user
    user = User.find(params[:user_id])
    domain = user.domain
    if user.destroy
      redirect_to domain_path(domain.id), flash: { success: I18n.t('domains.delete_user.success', email: user.email) }
    else
      redirect_to domain_path(domain.id), flash: { alert: I18n.t('domains.delete_user.failure', email: user.email) }
    end
  end

  def export
    send_data generate_csv(@dataset),
              type: 'text/csv; charset=utf-8; header=present',
              disposition: 'attachment; filename=domains.csv'
  end

  def users_export
    send_data generate_users_csv(@dataset),
              type: 'text/csv; charset=utf-8; header=present',
              disposition: 'attachment; filename=users.csv'
  end

  private

  def set_dataset
    @categories = DomainCategory.order(:category).collect(&:category)
    @domains = Domain.all(true).sort_by(&:name)

    @domains = domains_filters_and_order(@domains, index_params)
    
    @total_quotas = @domains.map(&:zouritDomainQuota).inject(0, &:+)
    @total_attributed_quota = Domain.all_attributed_quota
    @total_users = User.all.count # @domains.map(&:nbUsersDomain).inject(0, &:+)

    @dataset = dataset_pagination(@domains, index_params)
  end

  def generate_csv(dataset)
    CSV.generate do |csv|
      csv << [t('domains.index.structure'),
              t('domains.index.domain'),
              t('domain_categories.index.category'),
              t('domains.index.attributed_quota'),
              t('domains.dialog_domains_statistics.total_attributed_quota')]

      dataset[:items].each do |data|
        csv << [data.full_name,
                data.name,
                data.details.domain_category&.category,
                data.zouritDomainQuota,
                data.zouritAttributedQuota]
      end
    end
  end

  def set_users_dataset
    @domain.set_attributed_quota
    @domain.set_nb_users
    users_listing = @domain.users
    users_listing.select!(&:admin) if index_params[:admin] == '1'

    users = users_filters_and_order(users_listing, index_params)
    @dataset = dataset_pagination(users, index_params)
  end
  
  

  def generate_users_csv(dataset)
    CSV.generate do |csv|
      csv << ["#{t 'domain.prenom'} - #{t 'domain.nom'}",
              t('domain.email')]

      dataset[:items].each do |data|
        csv << ["#{data.first_name} #{data.last_name}",
                data.email]
      end
    end
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_domain
    @domain = Domain.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def domain_params
    params.require(:domain).permit(:name, :zouritDomainQuota,
                                   :zouritAttribuedQuota, :full_name,
                                   :admin_lastname, :admin_firstname,
                                   :admin_email, :admin_password, :admin_password_confirmation,
                                   :address, :phone, :email)
  end

  def index_params
    params.permit(:category, :order, :query, :page, :items_per_page, :admin)
  end

  def domains_filters_and_order(domains, params)
    if params[:category].present?
      domains.select! do |d|
        d.details.domain_category&.category == params[:category]
      end
    end

    if params[:query].present?
      domains.select! do |d|
        d.name.downcase.include?(params[:query].downcase) or d.details&.full_name&.downcase&.include?(params[:query].downcase)
      end
    end

    if params[:order]
      order_column, order_direction = params[:order].split(',')
      order_column = 'full_name' unless %w[name full_name].include?(order_column)
      order_direction = 'DESC' unless %w[ASC DESC].include?(order_direction)
      if order_column == 'name'
        domains.sort! { |a, b| b.name.downcase <=> a.name.downcase } if order_direction == 'ASC'
        domains.sort! { |a, b| a.name.downcase <=> b.name.downcase } if order_direction == 'DESC'
      else
        if order_direction == 'ASC'
          domains.sort! do |a, b|
            b.full_name.downcase <=> a.full_name.downcase
          end
        end
        if order_direction == 'DESC'
          domains.sort! do |a, b|
            a.full_name.downcase <=> b.full_name.downcase
          end
        end
      end
    else
      domains.sort! { |a, b| a.name.downcase <=> b.name.downcase }
    end

    domains
  end

  def users_filters_and_order(users, params)
    if params[:query].present?
      users.select! do |u|
        u.name.downcase.include?(params[:query].downcase) or u.email.include?(params[:query].downcase)
      end
    end

    if params[:order].to_s.empty?
      users.sort! { |a, b| a.full_name.downcase <=> b.full_name.downcase }
    else
      order_column, order_direction = params[:order].split(',')
      if order_column == 'email'
        users.sort! { |a, b| b.email.downcase <=> a.email.downcase } if order_direction == 'ASC'
        users.sort! { |a, b| a.email.downcase <=> b.email.downcase } if order_direction == 'DESC'
      else
        users.sort! { |a, b| b.full_name.downcase <=> a.full_name.downcase } if order_direction == 'ASC'
        users.sort! { |a, b| a.full_name.downcase <=> b.full_name.downcase } if order_direction == 'DESC'
      end
    end

    users
  end
end
