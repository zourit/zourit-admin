class GroupsController < ApplicationController
  include GroupsHelper
  include UsersHelper

  before_action :set_domain
  before_action :set_group, only: %i[users destroy show update]
  before_action :admin_only

  # GET /domains/1/groups
  # GET /domains/1/groups.json
  def index
    all = params[:all] == 'true'
    user_id = params[:user_id]
    @user_groups_ids = user_id ? User.find(user_id).groups_uuid : []
    @groups = @domain ? list_groups(@domain.name, true, all).sort_by(&:name) : []
    @group = Group.new
    users = @domain.users
    users.sort! { |a, b| a.full_name.downcase <=> b.full_name.downcase }
    if params[:exclude]
      users.reject! do |u|
        params[:exclude].split(',').include?(u.id)
      end
    end
    if params[:query]
      users.select! do |u|
        u.name.downcase.include?(params[:query].downcase) or u.email.include?(params[:query].downcase)
      end
    end
    @dataset = dataset_pagination(users, params)

    if turbo_frame_request?
      render partial: 'users', locals: { users: @domain.users }
    else
      render :index
    end
  end

  # GET /domains/1/groups/1
  # GET /domains/1/groups/1.json
  def show
    users = @domain.users
    users.sort! { |a, b| a.full_name.downcase <=> b.full_name.downcase }
    @members = @group.member.sort! { |a, b| a.full_name.downcase <=> b.full_name.downcase }
    members_ids = @members.map(&:id)
    users.reject! do |u|
      members_ids.include?(u.id)
    end
    if params[:exclude]
      users.reject! do |u|
        params[:exclude].split(',').include?(u.id)
      end
    end
    if params[:query]
      users.select! do |u|
        u.name.downcase.include?(params[:query].downcase) or u.email.include?(params[:query].downcase)
      end
    end
    @dataset = dataset_pagination(users, params)

    if turbo_frame_request?
      render partial: 'users'
    else
      render :show
    end
  end

  # GET /domains/1/groups/1/users.json
  def users; end

  # POST /domains/1/groups
  # POST /domains/1/groups.json
  def create
    group_name = group_params[:name]
    found, payload = mail_exists?("#{group_name}@#{@domain.name}")
    logger.info("group_name=#{group_name}, found=#{found}")
    members = group_params[:member].split(',')
    if members.count.positive?
      users = []
      members.each do |id|
        users << User.find(id)
      end

      @group = Group.new(name: group_name, domain: @domain, member: users)

      if !found && @group.save
        logger.info("création du groupe <#{group_name}>")
        redirect_to @group, notice: (I18n.t 'groups.noticecreate').to_s
      else
        error = I18n.t("groups.error.#{found ? payload : 'technical'}")
        logger.warn("impossible de créer le groupe <#{group_name}> car: #{error}")
        redirect_to groups_path(@domain.id), alert: "#{I18n.t('groups.error.create', group_name:)}|#{error}"
      end
    else
      redirect_to groups_path(@domain.id),
                  flash: { alert: "#{I18n.t('groups.error.create', group_name:)}|#{I18n.t('groups.addusergroup')}" }
    end
  end

  # DELETE /domains/1/groups/1
  # DELETE /domains/1/groups/1.json
  def destroy
    @group.destroy
    respond_to do |format|
      format.html { redirect_to @group, notice: (I18n.t 'groups.noticedestroy').to_s }
      format.json { head :no_content }
    end
  end

  # PATCH/PUT /domains/1/groups/1
  # PATCH/PUT /domains/1/groups/1
  def update
    res = if params[:delete] == '1'
            user = User.find(params[:member])
            user ? @group.del_single_user(user) : { result: false, message: 'no_user' }
          else
            new_members = params[:member].split(',')
            new_members.count.positive? ? @group.add_user(new_members) : { result: false, message: 'no_user' }
          end
    if res[:result]
      redirect_to group_path(@domain.id, @group.id), notice: (I18n.t 'groups.noticeupdate').to_s
    else
      alert = I18n.t('groups.error.update')
      alert += "|#{I18n.t("groups.error.#{res[:message]}")}" if res[:message]
      redirect_to group_path(@domain.id, @group.id), alert:
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_domain
    @domain = Domain.find(params[:id])
  end

  def set_group
    @group = Group.find_with_user(params[:group_id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def group_params
    params.require(:group).permit(:id, :name, :member)
  end
end
