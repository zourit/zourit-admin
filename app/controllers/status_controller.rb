class StatusController < ApplicationController
  include StatusHelper

  # GET /status
  def index
    # Si le client n'a pas de modules et n'est pas super admin, on l'empêche d'accéder à /status
    if current_user && !current_user.superadmin && DomainInfo.where(domain_id: current_user.domain.id).count.zero?
      respond_to do |format|
        format.html { redirect_to services_url, notice: (I18n.t 'status.noticemod').to_s }
        format.json { head :no_content }
      end
      return
    end
    @instances = get_instances(current_user)
  end
end
