class DomainCategoriesController < ApplicationController
  before_action :super_admin_only
  before_action :set_domain_category, only: %i[show edit update destroy]

  # GET /domain_categories.json
  def index
    @domain_categories = DomainCategory.all.order(:category)
  end

  # GET /domain_categories/1.json
  def show; end

  # GET /domain_categories/new
  def new
    @domain_category = DomainCategory.new
  end

  # GET /domain_categories/1/edit
  def edit; end

  # POST /domain_categories or /domain_categories.json
  def create
    @domain_category = DomainCategory.new(domain_category_params)

    respond_to do |format|
      if @domain_category.save
        format.html { redirect_to domain_categories_path, notice: 'Domain category was successfully created.' }
        format.json { render :show, status: :created, location: @domain_category }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @domain_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /domain_categories/1 or /domain_categories/1.json
  def update
    respond_to do |format|
      if @domain_category.update(domain_category_params)
        format.html { redirect_to domain_categories_path, notice: 'Domain category was successfully updated.' }
        format.json { render :show, status: :ok, location: @domain_category }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @domain_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /domain_categories/1 or /domain_categories/1.json
  def destroy
    @domain_category.destroy
    respond_to do |format|
      format.html { redirect_to domain_categories_url, notice: 'Domain category was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_domain_category
    @domain_category = DomainCategory.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def domain_category_params
    params.require(:domain_category).permit(:category)
  end
end
