# frozen_string_literal: true

class ImportController < ApplicationController
  include ImportHelper
  before_action :super_admin_only

  # GET /import
  def launch_import
    file = params['file']
    @for_real = params['dry_run'] == 'false'
    if file
      @messages = import(file, dry_run: !@for_real).split(/\r?\n/)
    else
      redirect_to(services_path, notice: 'Aucun fichier d\'import')
    end
  end
end
