# frozen_string_literal: true

class UsersController < ApplicationController
  skip_before_action :verify_authenticity_token
  include UsersHelper

  before_action :set_user,
                only: %i[show edit update destroy alias options set_options set_password groups]
  before_action :user_only, only: [:show]
  before_action :set_domain, only: [:create]
  before_action :admin_only, only: [:create]

  # GET /users/1
  def show; end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit; end

  def current
    @current_user = current_user
  end

  # POST /users
  def create
    @user = User.new
    params = user_params

    @user.first_name = params['first_name']
    @user.last_name = params['last_name']
    @user.email = "#{params['email']}@#{@domain.name}"
    @user.password = params['password']
    @user.password_confirmation = params['password_confirmation']
    @user.zouritZimbraEmail = params['zouritZimbraEmail'] ? params['zouritZimbraEmail'].casecmp('true').zero? : false
    @user.zouritZimbraTask = params['zouritZimbraTask'] ? params['zouritZimbraTask'].casecmp('true').zero? : false
    @user.zouritZimbraContact = params['zouritZimbraContact'] ? params['zouritZimbraContact'].casecmp('true').zero? : false
    @user.zouritZimbraCalendar = params['zouritZimbraCalendar'] ? params['zouritZimbraCalendar'].casecmp('true').zero? : false
    @user.zouritOwncloud = params['zouritOwncloud'] ? params['zouritOwncloud'].casecmp('true').zero? : false
    @user.admin = params['admin']&.casecmp('true')&.zero?
    @user.zouritZimbraQuota = params['zouritZimbraQuota'] ? params['zouritZimbraQuota'].to_i : 0
    @user.zouritCloudQuota = params['zouritCloudQuota'] ? params['zouritCloudQuota'].to_i * 1_000_000_000 : 1 * 1_000_000_000

    begin
      logger.info("about to create new user <#{@user.name}> with email #{@user.email}")
      @user.save!

      group_ids = groups_params.select { |_, value| value == 'on' }
      groups =  group_ids.keys
      @user.add_groups groups if groups

      logger.info("user <#{@user.name}> created successfully, email = #{@user.email}")
      redirect_to "/domains/#{@domain.id}",
                  flash: {
                    notice: I18n.t('users.noticecreate'),
                    user_id: @user.id,
                    user_name: @user.name
                  }
    rescue StandardError => e
      logger.warn("unable to save this user due to : #{e.message}")
      redirect_to "/domains/#{@domain.id}", alert: e.message
    end
  end

  # PATCH/PUT /users/1
  def update
    # uniquement pour l'utilisateur courrant
    redirect_to services_path and return unless @current_user.id == @user.id

    if params[:user][:password] != params[:user][:password_confirmation]
      redirect_to user_path(@user.id),
                  alert: (I18n.t 'users.problem_password') and return
    end

    unless params[:user][:password].empty? || params[:user][:password] =~ ZouritAdmin::Application::PASSWORD_PATTERN
      redirect_to user_path(@user.id),
                  alert: (I18n.t 'users.problem_password_pattern') and return
    end

    if @user.update(user_params)
      @user.populate_granted_modules
      session[:user] = @user.to_hash
      session[:user_locale] = @user.locale
      if session[:darkmode] != params[:darkmode]
        session[:darkmode] = params[:darkmode]
        @user.set_darkmode(params[:darkmode])
      end
      set_current_user
      redirect_to services_path, flash: { notice: t('.noticeupdate') }
    else
      redirect_to user_path(@user.id), flash: { alert: t('.notice_no_update') }
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    domain_id = @user.domain.id
    @user.destroy
    redirect_to "/domains/#{domain_id}", notice: I18n.t('users.noticedestroy')
  end

  # GET /users/1/alias.json
  def alias
    @alias = Alias.list @user.email
  end

  # POST /alias/create.json
  def create_alias
    @user = User.find(params[:user_id])
    a = Alias.new @user.email, "#{params[:email]}@#{@user.domain.name}"

    respond_to do |format|
      result = a.save
      if result[0]
        format.json { render json: a }
      else
        format.json { render json: result[1], status: :unprocessable_entity }
      end
    end
  end

  # DELETE /alias/delete.json
  def delete_alias
    a = Alias.new params[:user_email], params[:email]
    respond_to do |format|
      if a.destroy
        format.json { head :no_content }
      else
        format.json { render json: (I18n.t 'users.error').to_s, status: :unprocessable_entity }
      end
    end
  end

  # POST /login
  def login
    user = User.authenticate params[:loginName], params[:loginPassword]
    if user
      user.populate_granted_modules
      session[:user] = user
      session[:darkmode] = user.darkmode?
      url = params[:url].to_s.empty? ? '/services' : params[:url]
      redirect_to url, notice: "#{I18n.t 'users.bienvenue'} #{user.name}"
      logger.info("LOGGED IN <#{user.email}>")
    else
      session[:user] = nil
      url = params[:url].to_s.empty? ? '/services' : "/services?url=#{params[:url]}"
      redirect_to url, alert: (I18n.t 'users.problem').to_s
      logger.warn("DENIED user=<#{params[:loginName]}> for url=<#{url}>")
    end
  end

  # GET /logout
  def logout
    @type_page = 'login'
    domain = Domain.find(@current_user&.domain&.id)
    @logouts = []
    domain_infos = DomainInfo.where(domain_id: domain&.id)
    domain_infos.each do |di|
      sc = ServiceContext.new(di, domain, current_user)
      url = evaluate_config_module(di.config_module.name, sc)
      if url
        @logouts << { name: di.config_module.name,
                      url:  }
      end
    end

    session[:user] = nil
    render action: 'logout'
  end

  # GET /users/1/options.json
  def options
    url = "/domains/#{current_user.domain.id}"
    only_for_admin(url)

    @user_options = []
    @user.domain.config_modules.each do |config_module|
      config_module.user_options.each do |mo|
        uo = mo.user_options(@user)
        value = if uo
                  uo.value
                else
                  (mo.field_type == 'Boolean' ? false : '')
                end
        @user_options << { id: mo.id, value:, field_type: mo.field_type }
      end
    end
  end

  # POST /users/1/options
  def set_options
    url = "/domains/#{@user.domain.id}"
    only_for_admin(url)

    group_ids = groups_params.select { |_, value| value == 'on' }
    groups =  group_ids.keys
    logger.info("set groups #{groups}")
    @user.set_groups groups if groups

    result = set_user_options(params['servicesformline'])
    if result[:success]
      redirect_to url, notice: I18n.t('users.set_options.create_options')
    else
      redirect_to url, alert: I18n.t("users.set_options.errors.#{result[:message]}")
    end
  end

  # GET /users/1/groups.json
  def groups
    url = "/domains/#{current_user.domain.id}"
    only_for_admin(url)
    user_groups = @user.groups_uuid
    @groups = []
    @user.domain.groups.each do |group|
      @groups << { id: group.id, name: group.name, member: user_groups.include?(group.id) }
    end
  end

  # POST /users/1/password
  def set_password
    params = user_params

    @user.password = params['password']
    @user.password_confirmation = params['password_confirmation']

    url = "/domains/#{current_user.domain.id}"
    if @user.password == @user.password_confirmation && @user.save!
      redirect_to url, notice: I18n.t('users.set_password.success')
    else
      redirect_to url, alert: I18n.t('users.set_password.error')
    end
  end

  private

  def set_domain
    @domain = Domain.find(user_params[:domain_id])
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
    raise 'user not found' unless @user

    @domain = @user.domain
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    params.require(:user).permit(:domain_id, :password, :password_confirmation, :email, :admin, :first_name, :last_name,
                                 :quota, :zouritZimbraEmail, :zouritZimbraContact,
                                 :zouritZimbraCalendar, :zouritZimbraTask, :zouritZimbraQuota,
                                 :zouritOwncloud, :zouritCloudQuota, :theme, :locale)
  end

  def groups_params
    params.fetch(:groupsids, {}).permit!
  end

  def formline_params
    params.require(:formline).permit!
  end

  def evaluate_config_module(name, _service_context)
    eval("Core::#{name}::Main.get_logout_url(_service_context)")
  end

  def only_for_admin(url)
    redirect_to url, alert: I18n.t('application.law') unless admin_or_superadmin?
  end

  def only_for_admin_or_actual_user(url)
    redirect_to url, alert: I18n.t('application.law') unless admin_or_superadmin? || actual_user?
  end

  def admin_or_superadmin?
    current_user.superadmin || (current_user.admin && current_user.domain.id == @user.domain.id)
  end

  def actual_user?
    current_user.id == @user.id
  end

  def check_quota(parameters)
    meta_options_of_current_domain = @user.domain.config_modules.map(&:meta_options)
    available_modules = meta_options_of_current_domain.map do |mo_array|
      mo_array.select do |mo|
        mo.key.to_sym == MetaOption::ENABLED
      end.first
    end

    enabled_modules = available_modules.select { |am| parameters["option-#{am.id}"] == 'on' }

    quota_used = enabled_modules.map do |em|
      MetaOption.where(key: 'quota', config_module_id: em.config_module_id).map do |mo|
        parameters["option-#{mo.id}"].to_i
      end.reduce(0, :+)
    end.reduce(0, :+)

    total_quota = @user.domain.zouritDomainQuota
    attributed_quota = @user.domain.attributed_quotas_excepted_users(@user.id)

    logger.info("total_quota=#{total_quota}, already_attributed_quota=#{attributed_quota}, quota_used_for_user = #{quota_used}")
    raise 'quota_exceeded' if attributed_quota + quota_used > total_quota
  end

  def set_user_options(parameters)
    logger.info 'check quota and fail if excess'
    check_quota(parameters)

    meta_options = @user.domain.config_modules.map(&:user_options).flatten
    meta_options_enabled = meta_options.select { |mo| mo.key == 'enabled' }
    meta_options_others = meta_options.reject { |mo| mo.key == 'enabled' }

    (meta_options_enabled + meta_options_others).each do |mo|
      value = parameters["option-#{mo.id}"]

      case mo.field_type
      when 'Boolean'
        value = (value ? (value == 'on') : false).to_s
      when 'ArrayOfString'
        value = value.select { |_, v| v.present? }.to_json
      end

      logger.info "set meta_option with value=<#{value}>"
      mo.set_user_option(@user, value)
    end
    { success: true }
  rescue Core::Zimbra::MailExistError => e
    logger.warn("set_user_options interrompu car inconsistence dans la création d'un mail ou d'un alias car #{e.message}")
    { success: false, message: e.message }
  rescue StandardError => e
    if e.message == 'quota_exceeded'
      logger.warn('dépassement de quota')
      { success: false, message: e.message }
    else
      logger.error('cannot set user options properly!', e)
      { success: false, message: 'unknow' }
    end
  end
end
