# frozen_string_literal: true

class ServicesController < ApplicationController
  before_action :only_logged_users, except: :index

  # GET /services
  def index
    @type_page = current_user ? 'home' : 'login'
    @url = params[:url]
    @links = current_user ? Link.where(domain_id: current_user.domain.id, user_id: nil) : nil
    @userlinks = current_user ? Link.where(user_id: current_user.id) : nil

    render action: 'login' unless @current_user
  end

  # GET /services/1
  def show
    @type_page = 'iframe'
    id = params[:id]
    config_module = begin
      ConfigModule.find(id)
    rescue StandardError
      logger.warn("no ConfigModule found for id <#{id}>, keep going anyway!")
      nil
    end

    redirect_to services_path, notice: (I18n.t 'services.noactif').to_s unless config_module

    redirect_to services_path, notice: (I18n.t 'services.noallowed').to_s unless current_user.enabled?(config_module)

    domain = Domain.find(current_user.domain.id)
    domaininfo = DomainInfo.where(
      config_module:,
      domain_id: current_user.domain.id
    ).first

    redirect_to services_path, notice: (I18n.t 'services.nomodule').to_s unless domaininfo&.instance_id

    sc = ServiceContext.new(domaininfo, domain, current_user)
    @url_embedded = evaluate_config_module(config_module.name, sc)
    @title = config_module.name

    log_statistics(sc.domain_info.config_module.name, sc.domain_info.instance.name)
  end

  private

  # TODO: améliorer le eval avec binding plus tard!
  def evaluate_config_module(name, _service_context)
    eval("Core::#{name}::Main.get_login_url(_service_context)")
  end
end
