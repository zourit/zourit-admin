class DomainDetailsController < ApplicationController
  before_action do
    super_admin_only
    set_domain_details
    @icon = 'globe'
  end

  # GET /domains/1/details
  def edit; end

  # PATCH/PUT /links/1
  def update
    @domain.details.attributes = domain_detail_params
    @domain.zouritDomainQuota = params[:domain_detail][:quota]
    if @domain.save
      redirect_to domains_path, flash: { success: I18n.t('domain_details.updated') }
    else
      render :edit
    end
  end

  private

  def set_domain_details
    @domain = Domain.find(params[:id])
    @domain_details = @domain.details
    @domain.set_attributed_quota
    @total_quotas = @domain.zouritDomainQuota
    @total_attributed_quota = @domain.zouritAttributedQuota
  end

  # Only allow a list of trusted parameters through.
  def domain_detail_params
    params.require(:domain_detail).permit(:full_name, :contact, :addess, :phone, :email,
                                          :comment, :bill_to, :billing_address, :domain_category_id)
  end
end
