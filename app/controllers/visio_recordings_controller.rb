class VisioRecordingsController < ApplicationController
  before_action do
    only_authorized_users('BigBlueButton')
  end
  before_action :set_url_local, only: %i[visio_recordings destroy download]
  before_action :set_visio, only: %i[index visio_recordings destroy after_destroy download]
  before_action :set_bbb_session, only: %i[visio_recordings destroy download]
  before_action only: %i[index destroy download] do
    only_owner!(@visio.owner_id, visios_path)
  end

  # GET /visios/1/recordings
  # GET /visios/1/recordings.json
  def index; end

  def visio_recordings
    unless @visio.owner_id == current_user.id
      redirect_to visios_path, notice: (I18n.t 'modules.bigbluebutton.authorizedaccesvisioconference')
    end
    redirect_to services_path, notice: (I18n.t 'modules.bigbluebutton.modulevisible').to_s unless @url_visios
    set_dataset
  end

  # Bidouille : Get affiché après un recording destroy pour éviter le rechargement en boucle infinie
  # de la page index des recordings
  def after_destroy; end

  # DELETE /visios/1/recordings/1
  # DELETE /visios/1/recordings/1.json
  def destroy
    if Core::BigBlueButton::Main.delete_room_recording(@sc, @session, params[:record_id])
      redirect_to visio_recordings_after_destroy_path(@visio),
                  notice: I18n.t('modules.bigbluebutton.successfuldestroyrecording')
    else
      redirect_to visio_recordings_after_destroy_path(@visio),
                  error: I18n.t('modules.bigbluebutton.cannotdeleterecording')
    end
  end

  def download
    set_dataset
    visio_recording = @dataset[:items].select { |recording| recording.record_id == params[:record_id] }.first
    redirect_to visio_recordings_path(@visio) && return unless visio_recording

    video_format = visio_recording.formats.select { |format| format[:recording_type] == 'video' }.first
    redirect_to visio_recordings_path(@visio) && return unless video_format

    video_url = "#{video_format[:url].gsub('/playback', '')}video-0.m4v"
    source = URI(video_url)
    Net::HTTP.start(source.host, source.port, use_ssl: true) do |http|
      req = Net::HTTP::Get.new source
      http.request(req) do |res|
        send_data res.read_body, filename: "#{@visio.name}.m4v", disposition: 'download'
      end
    end
  end

  private

  def set_dataset
    recordings = Core::BigBlueButton::Main.get_room_recordings(@sc, @session, @visio.friendly_id)
    recordings.map! { |recording| VisioRecording.new(recording.deep_symbolize_keys) }
    @dataset = dataset_pagination(recordings, index_params)
    @recordings_processing = Core::BigBlueButton::Main.get_room_recordings_processing(@sc, @session, @visio.friendly_id)
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_visio
    @visio = Visio.find(params[:id])
  rescue StandardError => e
    flash.alert = "#{I18n.t('modules.bigbluebutton.dontexist')}|#{e}"
    redirect_to visios_path
  else
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def visio_params
    params.require(:visio).permit(:name, :comment, :date_start, :duration, :number_of_participants,
                                  :owner_id, :url_visios)
  end

  def set_url_local
    @config_module = ConfigModule.where(name: configmodule_name(Core::BigBlueButton)).first
    domaininfo = DomainInfo.where(config_module_id: @config_module.id,
                                  domain_id: current_user.domain.id).first
    @sc = ServiceContext.new(domaininfo, current_user.domain, current_user)
    @url_visios = Core::BigBlueButton::Main.get_login_url(@sc)
  end

  def set_bbb_session
    @session = Core::BigBlueButton::Main.get_session(@sc)
  end

  def index_params
    params.permit(:author, :order, :query, :page, :items_per_page, :archived)
  end
end
