require 'csv'

module ExportHelper
  def export_domains
    puts 'Export des domaines'
    fichier = Rails.root.join('tmp', 'domains_export.csv')
    domains = Domain.all
    annee_en_cours = Date.today.year
    debut_annee = Date.parse("#{annee_en_cours}-01-01")
    fin_annee = Date.parse("#{annee_en_cours}-12-31")
    CSV.open(fichier, 'wb', col_sep: '|') do |csv|
      csv << ['Catégorie',
              'Domaine',
              'Structure',
              'Adresse',
              'Contact',
              'Courriel',
              'Adresse de facturation',
              'Contact de facturation',
              'Quota',
              'Date de création',
              "Début abo #{annee_en_cours}",
              "Nombre de jours à facturer en #{annee_en_cours}",
              'Commentaires']
      domains.each do |domain|
        debut_abo = domain.details.created_at < debut_annee ? debut_annee : domain.details.created_at.to_date
        nb_jours = (fin_annee - debut_abo).to_i + 1
        puts domain.name
        csv << [domain.details.domain_category&.category,
                domain.name,
                domain.full_name,
                domain.details.address.to_s.gsub(/\r\n/m, ', '),
                domain.details.contact,
                domain.details.email,
                if domain.details.billing_address
                  domain.details.billing_address.to_s.gsub(/\r\n/m,
                                                           ', ')
                else
                  domain.details.address.to_s.gsub(
                    /\r\n/m, ', '
                  )
                end,
                domain.details.bill_to || domain.details.contact,
                domain.zouritDomainQuota,
                domain.details.created_at.strftime('%Y-%m-%d'),
                debut_abo.strftime('%Y-%m-%d'),
                nb_jours,
                domain.details.comment.to_s.gsub(/\r\n/m, ', ')]
      end
      puts 'done'
    end
  end
end
