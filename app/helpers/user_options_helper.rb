module UserOptionsHelper
  def update_user_helper(user, params)
    res = update_profile(user, params[:user])
    return res unless res[:result]

    res = update_groups(user, params[:groups])
    return res unless res[:result]

    update_options(user, params[:servicesformline])
  end

  def create_user_helper(params, domain)
    res = create_profile(params[:user], domain)
    return res unless res[:result]

    update_groups(res[:user], params[:groups])

    update_options(res[:user], params[:servicesformline])
  end

  private

  def create_profile(params, domain)
    return { result: false, message: I18n.t('users.set_options.errors.quota_exceeded') } if new_domain_quota(nil,
                                                                                                             domain, params).negative?

    params['email'] = "#{params['email']}@#{domain.name}"

    # Vérification si un utilisateur existe déjà avec cette adresse email
    user = User.find(params['email'])
    return { result: false, message: I18n.t('users.set_options.errors.user_already_exist') } if user

    # TODO: Vérification si un alias existe déjà avec cette adresse email
    if 1 > 2
      return { result: false,
               message: I18n.t('users.set_options.errors.alias_already_exist') }
    end

    # Vérification si un group existe déjà avec cette adresse email
    if Group.find(params['email'])
      return { result: false,
               message: I18n.t('users.set_options.errors.group_already_exist') }
    end

    user = User.new(params)

    { result: user.update(params), message: 'OK', user: }
  end

  def update_profile(user, params)
    unless check_password(params['password'])
      return { result: false,
               message: I18n.t('activemodel.errors.models.user.attributes.password.too_weak') }
    end

    user.update(params) if user_need_update?(user, params)

    { result: true, message: 'Ok' }
  end

  def update_groups(user, params)
    user.domain.groups(with_users: true).each do |group|
      if params[group.id] == 'true'
        group.add_single_user(user)
      else
        group.del_single_user(user)
      end
    end

    { result: true, message: 'Ok' }
  end

  def check_password(password)
    password.blank? || password =~ ZouritAdmin::Application::PASSWORD_PATTERN
  end

  def user_need_update?(user, params)
    return true if params['password'].length.positive?
    return true if user.first_name != params['first_name']
    return true if user.last_name != params['last_name']
    return true if user.admin != (params['admin'] == 'true')

    false
  end

  def update_options(user, params)
    domain = user.domain
    domain.set_attributed_quota
    new_quota = new_domain_quota(user, domain, params)
    quota_block = domain.zouritQuotaLeft.negative? ? new_quota < domain.zouritQuotaLeft : new_quota.negative?
    return { result: false, message: I18n.t('users.set_options.errors.quota_exceeded') } if quota_block

    result = true
    message = ''
    domain.config_modules.each do |mod|
      mod.meta_options.each do |meta_option|
        param = params["option-#{meta_option.id}"]
        next unless param

        res = meta_option.set_user_option(user, param)
        result &= res[:result]
        message = res[:message] unless res[:result]
        break unless res[:result]
      end
    end

    { result:, message: result ? I18n.t('users.noticeupdate') : I18n.t('users.notice_no_update', error: message) }
  end

  def new_domain_quota(user, domain, params)
    meta_options = MetaOption.where(
      field: 'user',
      field_type: 'Quota',
      config_module_id: domain.config_modules.map(&:id)
    )

    user_new_attributed_quota = meta_options.map do |mo|
      enabled_mo = MetaOption.user_option(mo.config_module_id, 'enabled')
      enabled = params["option-#{enabled_mo.id}"] == 'true'
      enabled ? params["option-#{mo.id}"].to_i : 0
    end.reduce(0, :+)

    domain.zouritDomainQuota - domain.attributed_quotas_excepted_users(user&.id) - user_new_attributed_quota
  end
end
