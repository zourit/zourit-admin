module ApplicationHelper
  ## pour obtenir le nom du module, exemple: Core::Etherpad => Etherpad
  def configmodule_name(module_object)
    raise 'should be a module object' unless module_object.instance_of?(Module)

    module_object.name.demodulize
  end

  def authorized_access?(domain_id = nil)
    return false unless @current_user

    @current_user.superadmin || (@current_user.admin && @current_user.domain.id == domain_id)
  end

  def module_href(mod)
    mod.url_local.to_s.empty? ? service_path(mod.id) : mod.url_local
  end

  def module_href_new_window(mod)
    domain = Domain.find(current_user.domain.id)
    domaininfo = DomainInfo.where(
      config_module: mod,
      domain_id: current_user.domain.id
    ).first
    sc = ServiceContext.new(domaininfo, domain, current_user)
    eval("Core::#{mod.name}::Main.get_login_url(sc)")
  end

  def back_to(uri)
    content_tag(:a, href: uri, class: 'retour', title: I18n.t('actions.return')) do
      content_tag(:i, nil, class: %w[fa fa-arrow-left])
    end
  end

  def random_uuid
    UUIDTools::UUID.random_create.to_s.gsub('-', '')
  end

  def icon(service_name)
    ConfigModule.where(name: service_name)&.first&.font
  end

  PAGE_TITLE_HASH = {
    I18n.t('modules.jitsimeet.visiolist') => 'services.jitsimeet',
    I18n.t('modules.survey.title') => 'services.survey',
    I18n.t('modules.etherpad.padlist') => 'services.etherpad',
    I18n.t('modules.framemo.memolist') => 'services.framemo',
    I18n.t('modules.bigbluebutton.visiolist') => 'services.bigbluebutton'
  }

  def page_title(title)
    PAGE_TITLE_HASH.key?(title) ? I18n.t(PAGE_TITLE_HASH[title]) : title
  end
end
