module EventsHelper
  def banner_status(importance, &)
    content = case importance
              when 'warning' then open_div_for_banner(BORDER_WARNING, HOVER_WARNING,
                                                      BACKGROUND_WARNING)
              when 'danger'
                open_div_for_banner(BORDER_DANGER, HOVER_DANGER, BACKGROUND_DANGER)
              else
                open_div_for_banner(BORDER_SUCCESS, HOVER_SUCCESS, BACKGROUND_SUCCESS)
              end

    content += block_given? ? capture(&) : importance
    content += '</div>'

    concat(content.html_safe)
  end

  private

  BORDER_SUCCESS = 'border-green-400'.freeze
  HOVER_SUCCESS = 'border-green-500'.freeze
  BACKGROUND_SUCCESS = 'bg-green-100'.freeze

  BORDER_WARNING = 'border-orange-400'.freeze
  HOVER_WARNING = 'border-orange-500'.freeze
  BACKGROUND_WARNING = 'bg-orange-100'.freeze

  BORDER_DANGER = 'border-red-400'.freeze
  HOVER_DANGER = 'border-red-500'.freeze
  BACKGROUND_DANGER = 'bg-red-100'.freeze

  def open_div_for_banner(border, hover, background)
    "<div class='w-full mb-2 select-none border-4 text-center p-4 font-medium #{border} #{hover} #{background}'>"
  end
end
