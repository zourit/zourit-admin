module TicketsHelper
  def self.get_tickets(user, archived = false)
    ticket_status_ids = (archived ? [3, 4] : [1, 2])
    if user.superadmin
      Ticket.joins(:ticket_status)
            .where(ticket_statuses: { id: ticket_status_ids })
    else
      Ticket.joins(:ticket_status)
            .where(ticket_statuses: { id: ticket_status_ids },
                   domain_id: user.domain.id)
    end
  end
end
