# FIXME: à déplacer dans lib/... OU models/. ces méthodes ne devraient pas être situées ici car elles sont accessibles depuis les vues!
module DomainsHelper
  class LdapError < StandardError
  end

  LDAP_BASE = YAML.load_file(Rails.root.join('config', 'ldap.yml'))['base']

  def self.included(base)
    base.extend(ClassMethods)
  end

  def domain_from_mail(mail)
    self.class.domain_from_mail mail
  end

  def list_domains(with_quota = true)
    self.class.list_domains(with_quota)
  end

  def export_all_domains
    domains = ''
    list_domains.each do |domain|
      list_users(domain.name).each do |user|
        domains += "#{domain.name}, #{domain.zouritDomainQuota}, #{user.id}, #{user.email}, #{user.zouritZimbraQuota}, #{user.zouritCloudQuota}\n"
      end
    end
    logger.info domains
  end

  def get_administrators(domain)
    base = "ou=people,o=#{domain},#{LDAP_BASE}"
    filter = Net::LDAP::Filter.eq('zouritAdmin', 'TRUE')
    attributes = %w[db entryuuid sn cn displayname mail objectclass]
    results = SUPERLDAP.search(base:, filter:, attributes:)
    admins = []
    results.each do |r|
      admins << User.new(id: r.entryuuid.first,
                         dn: r.dn,
                         first_name: r.cn.first,
                         last_name: r.sn.first,
                         email: r.mail.first,
                         admin: true,
                         zouritZimbraEmail: r.objectclass.include?('zouritZimbraEmail'),
                         zouritZimbraContact: r.objectclass.include?('zouritZimbraContact'),
                         zouritZimbraCalendar: r.objectclass.include?('zouritZimbraCalendar'),
                         zouritZimbraTask: r.objectclass.include?('zouritZimbraTask'))
    end
    admins
  end

  def create_domain(name, zouritDomainQuota)
    logger.info("création du domaine <#{name}> avec quota <#{zouritDomainQuota} Go> ...")
    dn = "o=#{name},#{LDAP_BASE}"
    attr = {
      o: name,
      objectclass: %w[top organization zouritDomain],
      zouritdomainquota: zouritDomainQuota.to_s
    }
    ldap_add_or_fail(dn, attr, 'error domain creation')

    people_dn = 'ou=people,' + dn
    attr = {
      ou: 'people',
      objectclass: %w[top organizationalUnit]
    }
    ldap_add_or_fail(people_dn, attr, 'error people object creation under domain')

    group_dn = 'ou=groups,' + dn
    attr = {
      ou: 'groups',
      objectclass: %w[top organizationalUnit]
    }
    ldap_add_or_fail(group_dn, attr, 'error groups object creation under domain')
    logger.info("création du domaine <#{name}> avec quota <#{zouritDomainQuota} Go> ... SUCCESS")
  end

  def update_domain(domain, replace = {}, add = {}, delete = {})
    dn = "o=#{domain},#{LDAP_BASE}"
    args = []
    replace.each do |key, value|
      args << [:replace, key, value]
    end
    add.each do |key, value|
      args << [:add, key, value]
    end
    delete.each do |key, value|
      args << [:delete, key, value]
    end
    if SUPERLDAP.modify dn:, operations: args
      true
    else
      puts "error domain update : #{SUPERLDAP.get_operation_result.message} : #{args}"
      false
    end
  end

  def destroy_domain(name)
    dn = "o=#{name},#{LDAP_BASE}"
    people = 'ou=people,' + dn
    SUPERLDAP.delete(dn: people)
    groups = 'ou=groups,' + dn
    SUPERLDAP.delete(dn: groups)
    SUPERLDAP.delete(dn:)
  end

  def find_domain_by_name(name)
    find_domain_by_name name
  end

  module ClassMethods
    def find_domain_by_uuid(uuid)
      base = LDAP_BASE
      filter = Net::LDAP::Filter.eq('entryuuid', uuid)
      find_domain base, filter
    end

    def find_domain_by_name(name)
      base = LDAP_BASE
      dn = "o=#{name},#{base}"
      filter = Net::LDAP::Filter.eq('entrydn', dn)
      find_domain base, filter
    end

    def find_domain(base, filter, with_quota = true)
      attributes = with_quota ? %w[o entryuuid zouritDomainQuota] : %w[o entryuuid]
      response = SUPERLDAP.search(base:, filter:, attributes:).first
      domain = nil
      if response
        quota = (begin
          response.zouritdomainquota.first.to_i
        rescue StandardError
          0
        end)
        domain = Domain.new(response.o.first, response.entryuuid.first, quota)
      end
      domain
    end
  end

  private

  def ldap_add_or_fail(dn, attr, message)
    return if SUPERLDAP.add(dn:, attributes: attr)

    raise LdapError, "#{message} : #{SUPERLDAP.get_operation_result.message} : #{dn} #{attr}"
  end

  module ClassMethods
    def list_domains(with_quota = true)
      base = LDAP_BASE
      filter = Net::LDAP::Filter.eq('o', '*')
      attributes = with_quota ? %w[o entryuuid zouritDomainQuota] : %w[o entryuuid]
      result = SUPERLDAP.search(base:, filter:, attributes:)
      domains = []
      logger = SemanticLogger['DomainsHelper::ClassMethods']
      result.each do |r|
        next if r.dn == LDAP_BASE

        d = nil
        if with_quota
          # logger.info("#{r.o.first} with quota:#{r.zouritdomainquota.first}Go")
          d = Domain.new(r.o.first,
                         r.entryuuid&.first,
                         (begin
                           r.zouritdomainquota.first.to_i
                         rescue StandardError
                           0
                         end))
          d.set_attributed_quota
        else
          logger.info("#{r.o.first} no quota")
          d = Domain.new(r.o.first, r.entryuuid)
        end
        d.details = DomainDetail.where(domain_id: d.id).first_or_create
        domains << d
      end
      domains
    end
  end
end
