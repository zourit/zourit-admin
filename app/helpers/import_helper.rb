require 'csv'

module ImportHelper
  def import(file, dry_run: false)
    puts "Importation d'utilisateurs #{dry_run ? '(DRY_RUN)' : ''}"
    fichier = Rails.root.join('tmp', file)
    nb_lignes = 0
    nb_imports = 0
    messages = ''
    CSV.foreach(fichier, col_sep: ';') do |ligne|
      unless nb_lignes.zero?
        user = User.new
        user.first_name = ligne[1]
        user.last_name = ligne[0]
        user.email = ligne[2]
        user.password = ligne[3]
        user.password_confirmation = ligne[3]
        user.zouritZimbraEmail = ligne[4].downcase == 'true'
        user.zouritZimbraTask = ligne[7].downcase == 'true'
        user.zouritZimbraContact = ligne[6].downcase == 'true'
        user.zouritZimbraCalendar = ligne[5].downcase == 'true'
        user.zouritOwncloud = ligne[8].downcase == 'true'
        user.admin = ligne[11].downcase == 'true'
        user.zouritZimbraQuota = ligne[9].to_i
        user.zouritCloudQuota = ligne[10].to_i
        message = "#{nb_lignes} : #{user.name}\n"
        if dry_run
          nb_imports += 1
        elsif user.save
          nb_imports += 1
        else
          message << "#{nb_lignes} ERREUR !\n"
        end
        messages << message
        puts message
      end
      nb_lignes += 1
    end
    messages << "Nombre d'imports : #{nb_imports}/#{nb_lignes - 1}"
  end
end
