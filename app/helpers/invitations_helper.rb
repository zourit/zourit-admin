module InvitationsHelper
  def user_invitation(user_id)
    (InvitationUser.exists?(module_id: @module.id, module_tag: @tag,
                            uuid: user_id) || user_id == @module.owner_id)
  end

  def group_invitation(group_id)
    InvitationGroup.exists?(module_id: @module.id, module_tag: @tag, group_id: group_id)
  end

  def external_user_invitation(user_email)
    InvitationUser.exists?(module_id: @module.id, module_tag: @tag, email: user_email)
  end
end
