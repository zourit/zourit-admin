module GroupsHelper
  LDAP_BASE = YAML.load_file(Rails.root.join('config', 'ldap.yml'))['base']

  def self.included(base)
    base.extend(ClassMethods)
  end

  def list_groups(domain_name, with_users = false, with_tous = false)
    base = "ou=groups,o=#{domain_name},#{LDAP_BASE}"
    filter = Net::LDAP::Filter.eq('cn', '*')
    attributes = %w[cn member entryuuid description]
    result = SUPERLDAP.search(base:, filter:, attributes:)
    groups = []
    result&.each do |r|
      users = []
      if with_users
        r.member.each do |u|
          users << User.find_by_dn(u)
        end
      end
      unless !r.entryuuid.first || ((r.cn.first == "tous@#{domain_name}") && !with_tous)
        begin
          groups << Group.new(id: r.entryuuid.first, name: r.cn.first, member: users.compact, dn: r.dn,
                              description: r.description.first)
        rescue StandardError
          groups << Group.new(id: r.entryuuid.first, name: r.cn.first, member: users.compact, dn: r.dn)
        end
      end
    end
    groups
  end

  def create_group(dn, member, description = '-')
    members = []
    member.each do |m|
      members << m.dn
    end
    attr = {
      ou: 'groups',
      objectclass: %w[top groupOfNames],
      description:,
      member: members
    }
    if SUPERLDAP.add(dn: dn + ",#{LDAP_BASE}", attributes: attr)
      true
    else
      logger.error "error groups object creation under domain_name : #{SUPERLDAP.get_operation_result.message}"
      false
    end
  end

  def update_group(dn, replace = {}, add = {}, delete = {})
    args = []
    replace.each do |key, value|
      args << [:replace, key, value]
    end
    add.each do |key, value|
      args << [:add, key, value]
    end
    delete.each do |key, value|
      args << [:delete, key, value]
    end
    p args
    if SUPERLDAP.modify dn:, operations: args
      true
    else
      puts "error update : #{SUPERLDAP.get_operation_result.message}"
      false
    end
  end

  def destroy_group(dn)
    p dn
    SUPERLDAP.delete(dn:)
  end

  module ClassMethods
    def find_group_by_uuid(uuid, with_users = false)
      base = LDAP_BASE
      filter = Net::LDAP::Filter.eq('entryuuid', uuid)
      find_group base, filter, with_users
    end

    def find_group_by_dn(dn, with_users = false)
      base = dn
      filter = nil
      find_group base, filter, with_users
    end

    def find_group_by_cn(cn, with_users = false)
      domain_name = cn.split('@')[1]
      base = "cn=#{cn},ou=groups,o=#{domain_name},#{LDAP_BASE}"
      filter = nil
      find_group base, filter, with_users
    end

    def find_group(base, filter, with_users)
      attributes = %w[cn dn member entryuuid description]
      rs = SUPERLDAP.search(base:, filter:, attributes:)
      r = rs ? rs.first : nil
      return unless r

      users = []
      if with_users
        r.member.each do |u|
          user = User.find_by_dn(u)
          users << user if user
        end
      end

      begin
        logger.info r.inspect
        logger.info r.description.first
        Group.new id: r.entryuuid.first,
                  dn: r.dn,
                  name: r.cn.first,
                  description: r.description.first,
                  member: users
      rescue StandardError
        # Si le champs description n'existe pas
        Group.new id: r.entryuuid.first,
                  dn: r.dn,
                  name: r.cn.first,
                  member: users
      end
    end
  end
end
