module ChartHelper
  def chart(id, type, data, options = {}, trace: true)
    graph = <<-GRAPH
        <div class='w-full h-full' #{"x-init='$nextTick(() => {showGraphModale_#{id}()})'" if trace}>
            <canvas id='#{id}'></canvas>
        </div>
        <script>
            function showGraphModale_#{id}() {
                let chartStatus = Chart.getChart('#{id}')
                if (chartStatus != undefined) {
                    chartStatus.destroy();
                }
                new Chart(document.getElementById('#{id}'), {
                    type: '#{type}',
                    data: #{data},
                    options: #{options},
                })
            }
        </script>
    GRAPH

    graph.html_safe
  end
end
