# frozen_string_literal: true

module UsersHelper
  LDAP_BASE = YAML.load_file(Rails.root.join('config', 'ldap.yml'))['base']

  def self.included(base)
    base.extend(ClassMethods)
  end

  def mail_exists?(mail)
    self.class.mail_exists?(mail)
  end

  def list_users(domain_name)
    self.class.list_users(domain_name)
  end

  def list_all_users
    self.class.list_all_users
  end

  def list_aliases_of_domain_users(domain_users)
    self.class.list_aliases_of_domain_users(domain_users)
  end

  def create_user(mail, firstname, lastname, password, objectclasses, zouritadmin, zouritZimbraQuota, zouritCloudQuota,
                  _zouritPeertubeInstance)
    found, payload = mail_exists?(mail)
    if found
      errors.add(:email, payload)
      raise "mail #{mail} already existing"
    end

    dn = "mail=#{mail},ou=people,o=#{domain_name_from_mail(mail)},dc=zourit,dc=re"
    attr = {
      cn: firstname,
      sn: lastname,
      mail:,
      displayname: "#{firstname} #{lastname}",
      userpassword: Net::LDAP::Password.generate(:sha, password),
      objectclass: objectclasses,
      zouritadmin: zouritadmin ? 'TRUE' : 'FALSE',
      zouritsuperadmin: 'FALSE',
      zouritzimbraquota: zouritZimbraQuota.to_s,
      zouritcloudquota: zouritCloudQuota.to_s
    }
    unless SUPERLDAP.add(dn:, attributes: attr)
      raise "Erreur de création de l'utilisateur #{mail} dans LDAP : #{dn} #{attr}"
    end

    logger.info "Utilisateur #{mail} créé dans LDAP"
    true
  end

  def update_user(mail, replace = {}, add = {}, delete = {})
    dn = "mail=#{mail},ou=people,o=#{domain_name_from_mail(mail)},#{LDAP_BASE}"
    args = []
    replace.each do |key, value|
      args << [:replace, key, value]
    end
    add.each do |key, value|
      args << [:add, key, value]
    end
    delete.each do |_key, value|
      args << [:delete, objectclass, value]
    end
    if SUPERLDAP.modify dn:, operations: args
      logger.info "LDAP user #{mail} update #{args} : #{SUPERLDAP.get_operation_result.message}"
      true
    else
      logger.info args
      logger.error "ERROR LDAP user #{mail} update : #{SUPERLDAP.get_operation_result.message}"
      false
    end
  end

  def destroy_user(dn)
    SUPERLDAP.delete(dn:)
  end

  def domain_from_mail(mail)
    self.class.domain_from_mail mail
  end

  def domain_name_from_mail(mail)
    self.class.domain_name_from_mail mail
  end

  def cn_by_mail(mail)
    self.class.cn_by_mail mail
  end

  def list_user_groups(user, with_users = false, with_tous = false)
    base = "ou=groups,o=#{user.domain.name},#{LDAP_BASE}"
    filter = Net::LDAP::Filter.eq('member', user.dn)
    attributes = %w[cn member entryuuid]
    result = SUPERLDAP.search(base:, filter:, attributes:)
    groups = []
    result&.each do |r|
      users = []
      if with_users
        r.member.each do |u|
          users << User.find_by_dn(u)
        end
      end
      unless !r.entryuuid.first || ((r.cn.first == "tous@#{domain.name}") && !with_tous)
        groups << Group.new(id: r.entryuuid.first, name: r.cn.first, member: users, dn: r.dn)
      end
    end
    groups
  end

  def superadmins
    filter = Net::LDAP::Filter.eq('zouritsuperadmin', 'TRUE')
    attributes = %w[db entryuuid sn cn displayname mail objectclass zouritadmin zouritsuperadmin
                    zouritzimbraquota zouritcloudquota zourittheme, zouritpeertubeinstance]
    result = SUPERLDAP.search(base: LDAP_BASE, filter:, attributes:)
    create_users_array_from_ldap_result(result)
  end

  def superadmins_emails
    superadmins.collect(&:email)
  end

  def create_users_array_from_ldap_result(result)
    self.class.create_users_array_from_ldap_result(result)
  end

  module ClassMethods
    def mail_exists?(mail)
      domain = domain_name_from_mail(mail)
      domain_users = list_users(domain)
      return [true, :domain_users_include] if domain_users.map(&:email).include?(mail)

      domain_groups = GroupsController.helpers.list_groups(domain, false, true).map(&:name)
      return [true, :domain_groups_include] if domain_groups.include?(mail)

      domain_aliases = list_aliases_of_domain_users(domain_users)
      return [true, :domain_aliases_include] if domain_aliases.include?(mail)

      [false, nil]
    end

    def list_aliases_of_domain_users(domain_users)
      aliases = []
      domain_users.each do |du|
        aliases += du.aliases
      end
      aliases
    end

    def find_user_by_mail(mail)
      base = "ou=people,o=#{domain_name_from_mail(mail)},#{LDAP_BASE}"
      filter = Net::LDAP::Filter.eq('mail', mail)
      find_user base, filter
    end

    def find_user_by_uuid(uuid)
      base = LDAP_BASE
      filter = Net::LDAP::Filter.eq('entryuuid', uuid)
      find_user base, filter
    end

    def find_user_by_dn(dn)
      base = dn
      filter = nil
      find_user base, filter
    end

    def find_user(base, filter)
      attributes = %w[db entryuuid sn cn displayname mail userpassword objectclass zouritadmin
                      zouritsuperadmin zouritzimbraquota zouritcloudquota zourittheme zouritpeertubeinstance]
      rs = SUPERLDAP.search(base:, filter:, attributes:)
      r = rs.first if rs
      return unless r

      User.new id: r.entryuuid.first,
               dn: r.dn,
               first_name: r.cn.first,
               last_name: r.sn.first.upcase,
               email: r.mail.first,
               theme: (begin
                 r.zourittheme.first.to_i
               rescue StandardError
                 ZouritAdmin::Application::DEFAULT_THEME
               end),
               admin: r.zouritadmin.first == 'TRUE',
               superadmin: r.zouritsuperadmin.first == 'TRUE',
               zouritZimbraEmail: r.objectclass.include?('zouritZimbraEmail'),
               zouritZimbraContact: r.objectclass.include?('zouritZimbraContact'),
               zouritZimbraCalendar: r.objectclass.include?('zouritZimbraCalendar'),
               zouritZimbraTask: r.objectclass.include?('zouritZimbraTask'),
               zouritOwncloud: r.objectclass.include?('zouritOwncloud'),
               zouritPeertube: r.objectclass.include?('zouritPeertube'),
               zouritZimbraQuota: (begin
                 r.zouritZimbraQuota.first.to_i
               rescue StandardError
                 0
               end),
               zouritCloudQuota: (begin
                 r.zouritCloudQuota.first.to_i
               rescue StandardError
                 0
               end),
               zouritPeertubeInstance: (begin
                 r.zouritPeertubeInstance.first.to_s
               rescue StandardError
                 ''
               end)
    end

    def domain_from_mail(mail)
      Domain.find(domain_name_from_mail(mail))
    end

    def domain_name_from_mail(mail)
      mail&.split('@')&.[](1)
    end

    def cn_by_mail(mail)
      "mail=#{mail},ou=people,o=#{domain_name_from_mail(mail)},#{LDAP_BASE}"
    end

    def list_users(domain_name)
      base = "ou=people,o=#{domain_name},#{LDAP_BASE}"
      filter = Net::LDAP::Filter.eq('mail', "*@#{domain_name}")
      attributes = %w[db entryuuid sn cn displayname mail objectclass zouritadmin
                      zouritsuperadmin zouritzimbraquota zouritcloudquota zouritPeertubeInstance]
      result = SUPERLDAP.search(base:, filter:, attributes:)
      create_users_array_from_ldap_result(result)
    end

    def list_all_users
      filter = Net::LDAP::Filter.eq('objectclass', 'inetorgperson') & Net::LDAP::Filter.eq('objectclass', 'zourit')
      attributes = %w[db entryuuid sn cn displayname mail objectclass zouritadmin
                      zouritsuperadmin zouritzimbraquota zouritcloudquota zouritPeertubeInstance]
      result = SUPERLDAP.search(base: LDAP_BASE, filter:, attributes:)
      create_users_array_from_ldap_result(result)
    end

    def create_users_array_from_ldap_result(result)
      users = []
      result&.each do |r|
        users << User.new(id: r.entryuuid.first,
                          dn: r.dn,
                          first_name: r.cn.first,
                          last_name: r.sn.first.upcase,
                          email: r.mail.first,
                          admin: r.zouritadmin.first == 'TRUE',
                          superadmin: r.zouritsuperadmin.first == 'TRUE',
                          quota: 0,
                          theme: begin
                            r.zourittheme.first.to_i
                          rescue StandardError
                            1
                          end,
                          zouritZimbraEmail: r.objectclass.include?('zouritZimbraEmail'),
                          zouritZimbraContact: r.objectclass.include?('zouritZimbraContact'),
                          zouritZimbraCalendar: r.objectclass.include?('zouritZimbraCalendar'),
                          zouritZimbraTask: r.objectclass.include?('zouritZimbraTask'),
                          zouritOwncloud: r.objectclass.include?('zouritOwncloud'),
                          zouritPeertube: r.objectclass.include?('zouritPeertube'),
                          zouritZimbraQuota: (begin
                            r.zouritzimbraquota.first.to_i
                          rescue StandardError
                            0
                          end),
                          zouritCloudQuota: (begin
                            r.zouritcloudquota.first.to_i
                          rescue StandardError
                            0
                          end),
                          zouritPeertubeInstance: (begin
                            r.zouritPeertubeInstance.first.to_s
                          rescue StandardError
                            ''
                          end))
      end
      users
    end
  end
end
