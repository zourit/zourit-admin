module StatusHelper
  def get_instances(current_user)
    instances = []
    if current_user && !current_user.superadmin
      DomainInfo.where(domain_id: current_user.domain.id).each do |info|
        instances << Instance.find(info.instance_id)
      end
      instances.sort_by!(&:name)
    else # Si on est superadmin ou pas connecté on affiche toutes les instances
      instances = Instance.order(:name).to_a
    end
  end
end
