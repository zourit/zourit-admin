# frozen_string_literal: true

class ModuleBase
  include SemanticLogger::Loggable

  def self.get_logout_url(service_context); end
  def self.create_domain(_domain_info, _domain); end
  def self.destroy_domain(_domain_info, _domain); end

  def self.get_login_url(service_context)
    service_context.domain_info.instance.option_by_name('url')
  end

  def self.build_instance_options(instance)
    params = {}
    instance.instance_options.each do |io|
      params[io.meta_option.key.to_sym] = io.value
    end
    params
  end

  def self.update_user_option_plus_enabled(meta_option, user, old_value, new_value, config_module)
    if meta_option.key.to_sym == MetaOption::ENABLED
      if new_value == 'true'
        # On se sert de l'existance de la ligne useroption avec la clé enabled pour indiquer
        # que l'utilisateur est actif sur le module
        return false unless create_user(user)

        logger.info { "#{config_module.name} => Activation de l'utilisateur #{user.email}" }
      else
        logger.info { "#{config_module.name} => Suppression de l'utilisateur #{user.email}" }

        result, message, output = destroy_user(user, meta_option)
        return [result, message, output] unless result

        [destroy_user_options(user, meta_option)]
      end
    elsif enabled?(user, meta_option.config_module)
      logger.info('update_user_option_plus_enabled')
      update_user_option(meta_option, user, old_value, new_value)
    end
  end

  def self.create_user(_user)
    [true, '', '']
  end

  def self.destroy_user(_user, _meta_option)
    [true, '', '']
  end

  def self.destroy_user_options(user, meta_option)
    meta_options_ids = MetaOption.user_options(meta_option.config_module_id).map(&:id)
    UserOption.where(user_id: user.id, meta_option_id: meta_options_ids).destroy_all
  end

  def self.update_user_option(meta_option, user, old_value, new_value); end

  # Permet d'obtenir rapidement le statut enable pour un module et un user
  def self.enabled?(user, mod)
    user.enabled_from_user_options?(mod)
  end

  def self.update_user(_user, _old_user)
    [true, '', '']
  end

  def self.min_quota
    0
  end

  def self.default_quota
    1
  end

  def self.change_locale(_user, _locale)
    [true, '', '']
  end

  def self.destroyable?
    true
  end
end
