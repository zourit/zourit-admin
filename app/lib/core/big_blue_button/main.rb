# frozen_string_literal: true

module Core
  module BigBlueButton
    class Main < ModuleBase
      # url de BigBlueButton
      def self.get_login_url(service_context)
        service_context.domain_info.instance.option_by_name('url')
                       .gsub('http://', '')
                       .gsub('https://', '')
      end

      def self.get_session(service_context)
        url = "https://#{get_login_url(service_context)}"
        signin_page = RestClient.get(
          "#{url}/signin",
          {
            user_agent: 'Zourit'
          }
        )
        csrf_token = Nokogiri::HTML(signin_page).at('meta[name="csrf-token"]')['content']
        cookie = signin_page.headers[:set_cookie][0]

        session = RestClient.post(
          "#{url}/api/v1/sessions.json",
          {
            session: {
              email: service_context.domain_info.instance.option_by_name('username'),
              password: service_context.domain_info.instance.option_by_name('password'),
              extend_session: false
            },
            token: ''
          }.to_json,
          {
            accept: 'application/json',
            content_type: 'application/json',
            cookie:,
            user_agent: 'Zourit',
            X_CSRF_TOKEN: csrf_token
          }
        )
        {
          cookie: session.headers[:set_cookie][0],
          csrf_token:,
          user_id: JSON.parse(session.body)['data']['id']
        }
      end

      def self.get_rooms_list(service_context, session)
        url = "https://#{get_login_url(service_context)}"

        rooms_list = RestClient.get(
          "#{url}/api/v1/rooms.json",
          {
            accept: 'application/json',
            content_type: 'application/json',
            user_agent: 'Zourit',
            cookie: session[:cookie],
            X_CSRF_TOKEN: session[:csrf_token]
          }
        )
        JSON.parse(rooms_list.body)['data']
      end

      def self.create_room(service_context, session, room_name)
        url = "https://#{get_login_url(service_context)}"
        created_room = RestClient.post(
          "#{url}/api/v1/rooms.json",
          {
            room: {
              name: room_name,
              user_id: session[:user_id]
            },
            user_id: session[:user_id]
          }.to_json,
          {
            accept: 'application/json',
            content_type: 'application/json',
            user_agent: 'Zourit',
            cookie: session[:cookie],
            X_CSRF_TOKEN: session[:csrf_token]
          }
        )
        JSON.parse(created_room.body)['data']
      end

      def self.edit_room(service_context, session, friendly_id, room_name)
        url = "https://#{get_login_url(service_context)}"
        edited_room = RestClient.patch(
          "#{url}/api/v1/rooms/#{friendly_id}.json",
          {
            room: {
              name: room_name,
              user_id: session[:user_id]
            },
            user_id: session[:user_id]
          }.to_json,
          {
            accept: 'application/json',
            content_type: 'application/json',
            user_agent: 'Zourit',
            cookie: session[:cookie],
            X_CSRF_TOKEN: session[:csrf_token]
          }
        )
        JSON.parse(edited_room.body)['data']
      end

      def self.start_meeting(service_context, session, friendly_id, display_username)
        url = "https://#{get_login_url(service_context)}"
        start_url = RestClient.post(
          "#{url}/api/v1/meetings/#{friendly_id}/start.json",
          { name: display_username }.to_json,
          {
            accept: 'application/json',
            content_type: 'application/json',
            referer: "http://#{Rails.application.routes.default_url_options[:host]}:#{Rails.application.routes.default_url_options[:port]}/end_visio",
            user_agent: 'Zourit',
            cookie: session[:cookie],
            X_CSRF_TOKEN: session[:csrf_token]
          }
        )
        start_url = JSON.parse(start_url.body)['data']
        # Return the URL after the first redirection if BBB is behind a Scalelite as a proxy
        begin
          RestClient::Request.execute(method: :get, url: start_url, max_redirects: 0)
        rescue RestClient::ExceptionWithResponse => e
          start_url = e.response.headers[:location]
        end
        start_url
      end

      def self.delete_room(service_context, session, friendly_id)
        url = "https://#{get_login_url(service_context)}"
        RestClient.delete(
          "#{url}/api/v1/rooms/#{friendly_id}.json",
          {
            accept: 'application/json',
            content_type: 'application/json',
            user_agent: 'Zourit',
            cookie: session[:cookie],
            X_CSRF_TOKEN: session[:csrf_token]
          }
        )
      end

      def self.get_room_recordings(service_context, session, friendly_id)
        url = "https://#{get_login_url(service_context)}"
        recordings_data = RestClient.get(
          "#{url}/api/v1/rooms/#{friendly_id}/recordings.json",
          {
            accept: 'application/json',
            content_type: 'application/json',
            user_agent: 'Zourit',
            cookie: session[:cookie],
            X_CSRF_TOKEN: session[:csrf_token]
          }
        )
        JSON.parse(recordings_data.body)['data']
      end

      def self.delete_room_recording(service_context, session, record_id)
        url = "https://#{get_login_url(service_context)}"
        RestClient.delete(
          "#{url}/api/v1/recordings/#{record_id}.json",
          {
            accept: 'application/json',
            content_type: 'application/json',
            user_agent: 'Zourit',
            cookie: session[:cookie],
            X_CSRF_TOKEN: session[:csrf_token]
          }
        )
      end

      def self.get_room_recordings_processing(service_context, session, friendly_id)
        url = "https://#{get_login_url(service_context)}"
        recordings_data = RestClient.get(
          "#{url}/api/v1/rooms/#{friendly_id}/recordings_processing.json",
          {
            accept: 'application/json',
            content_type: 'application/json',
            user_agent: 'Zourit',
            cookie: session[:cookie],
            X_CSRF_TOKEN: session[:csrf_token]
          }
        )
        JSON.parse(recordings_data.body)['data']
      end

      def self.get_room_settings(service_context, session, friendly_id)
        url = "https://#{get_login_url(service_context)}"
        settings_data = RestClient.get(
          "#{url}/api/v1/room_settings/#{friendly_id}.json",
          {
            accept: 'application/json',
            content_type: 'application/json',
            user_agent: 'Zourit',
            cookie: session[:cookie],
            X_CSRF_TOKEN: session[:csrf_token]
          }
        )
        JSON.parse(settings_data.body)['data']
      end

      def self.update_room_settings(service_context, session, friendly_id, setting_name, setting_value)
        url = "https://#{get_login_url(service_context)}"
        settings_data = RestClient.patch(
          "#{url}/api/v1/room_settings/#{friendly_id}.json",
          {
            settingName: setting_name,
            settingValue: setting_value
          }.to_json,
          {
            accept: 'application/json',
            content_type: 'application/json',
            user_agent: 'Zourit',
            cookie: session[:cookie],
            X_CSRF_TOKEN: session[:csrf_token]
          }
        )
        JSON.parse(settings_data.body)['data']
      end

      def self.destroy_user(user, _meta_option)
        [Visio.where(owner_id: user.id).destroy_all, '', '']
      end
    end
  end
end
