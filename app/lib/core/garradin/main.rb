# frozen_string_literal: true

module Core
  module Garradin
    class Main < ModuleBase
      # url de login de garradin
      def self.get_login_url(service_context)
        fqdn_minus_http = service_context.domain_info.instance.option_by_name('fqdn')
                                         .gsub('http://', '').gsub('https://', '')
        "https://#{service_context.domain.name.tr('.', '-')}.#{fqdn_minus_http}/admin/"
      end

      # url de logout de garradin
      def self.get_logout_url(service_context)
        url = ''
        if service_context.domain_info&.instance
          url = get_login_url(service_context)
          url += 'logout.php'
        end
        url
      end

      def self.destroyable?
        false
      end
    end
  end
end
