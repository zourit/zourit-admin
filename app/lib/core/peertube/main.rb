# frozen_string_literal: true

module Core
  module Peertube
    class Main < ModuleBase
      def self.create_user(user)
        instance = user.domain.get_module_instance('Peertube')
        access_tokens = login_as_admin(instance)
        user_exist = does_this_peertube_user_exist?(user.email, instance, access_tokens)

        if user_exist
          [enable_peertube_authentication_for(user, instance), '', '']
        else
          [create_new_peertube_user(user, instance, access_tokens), '', '']
        end
      end

      def self.create_new_peertube_user(user, instance, access_tokens)
        # https://docs.joinpeertube.org/api-rest-reference.html#operation/addUser
        username_or_email = user.email.gsub('@', '_')

        logger.info("Peertube - Creation of the #{username_or_email} user")
        url = instance.option_by_name('url_api')

        # Request for creating a new user
        RestClient.post(
          "#{url}/users",
          {
            email: user.email,
            password: 'fg464g6er4g6rAAeg!re&',
            role: 2,
            username: username_or_email,
            videoQuota: 1_000_000,
            videoQuotaDaily: 0
          },
          {
            Authorization: "#{access_tokens['token_type']} #{access_tokens['access_token']}"
          }
        )

        use_ldap_to_authenticate_the_user(username_or_email, instance)
        enable_peertube_authentication_for(user, instance)
        # set_or_update_video_quota_of_the_user is inside update_user_option
      end

      def self.destroy_user(user, _meta_option)
        username = user.email.gsub('@', '_')

        logger.info("Peertube - Disabling this user : #{username}'. This user still in LDAD's BDD.")

        disable_peertube_authentication_for(user)

        instance = user.domain.get_module_instance('Peertube')
        access_tokens = login_as_admin(instance)
        [delete_all_peertube_user_video(username, instance, access_tokens), '', '']
      end

      # This code work well and it remove peertube's user but we can't recreate it again after
      # def self.destroy_peertube_user(user, _meta_option)
      #   # https://docs.joinpeertube.org/api-rest-reference.html#operation/delUser
      #   logger.info("Peertube - deletion of the #{user.email.gsub('@', '_')} user")

      #   instance = user.domain.get_module_instance('Peertube')
      #   url = instance.option_by_name('url_api')

      #   access_tokens = login_as_admin(instance)
      #   user_id = getuser_id(user.email, instance, access_tokens)

      #   RestClient.delete(
      #     "#{url}/users/#{user_id}",
      #     { Authorization: "#{access_tokens['token_type']} #{access_tokens['access_token']}" }
      #   )
      # end

      def self.set_or_update_video_quota_of_the_user(username_or_email, instance, access_tokens, quota_new_value)
        logger.info("Peertube - Set or update video quota of #{username_or_email}")

        url = instance.option_by_name('url_api')
        user_id = get_user_id(username_or_email, instance, access_tokens)
        quota_new_value = quota_new_value.to_i * (1024**3)

        response2 = RestClient::Request.execute(
          method: :put,
          url: "#{url}/users/#{user_id}",
          payload: { videoQuota: quota_new_value, videoQuotaDaily: quota_new_value },
          headers: { Authorization: "#{access_tokens['token_type']} #{access_tokens['access_token']}" }
        )
      end

      def self.update_user_option(meta_option, _user, _old_value, _new_value)
        case meta_option.key.to_sym
        # when MetaOption::USER
        #   create_user(_user)
        when MetaOption::QUOTA
          username_or_email = _user.email.gsub('@', '_')
          instance = _user.domain.get_module_instance('Peertube')
          access_tokens = login_as_admin(instance)

          [set_or_update_video_quota_of_the_user(username_or_email, instance, access_tokens, _new_value), '', '']
        end
      end

      def self.get_user_id(username_or_mail, instance, access_tokens)
        # https://github.com/rest-client/rest-client/issues/397
        url = instance.option_by_name('url_api')
        response = RestClient.get("#{url}/users",
                                  { Authorization: "#{access_tokens['token_type']} #{access_tokens['access_token']}",
                                    params: { search: username_or_mail } })
        user_id = JSON.parse(response.body)['data'][0]['id']
      end

      def self.logout; end

      def self.does_this_peertube_user_exist?(username_or_mail, instance, access_tokens)
        url = instance.option_by_name('url_api')
        response = RestClient.get("#{url}/users",
                                  { Authorization: "#{access_tokens['token_type']} #{access_tokens['access_token']}",
                                    params: { search: username_or_mail } })
        total = JSON.parse(response.body)['total']
        user_exist = total == 1
      end

      def self.delete_all_peertube_user_video(user_mail, instance, access_tokens)
        logger.info("Peertube - Deleting all peertube videos of #{user_mail}")

        url = instance.option_by_name('url_api')
        name = user_mail.gsub('@', '_')

        # Listing the videos of an account
        response = RestClient::Request.execute(
          method: :get,
          url: "#{url}/accounts/#{name}/videos",

          payload: { filter: 'all' },
          headers: { Authorization: "#{access_tokens['token_type']} #{access_tokens['access_token']}" }
        )
        response_json = JSON.parse(response)

        # Deleting all the videos of an account
        video_number = response_json['total']
        video_number.times do |i|
          uuid = response_json['data'][i]['uuid']
          logger.info("Deleting this video : #{uuid}")
          RestClient.delete(
            "#{url}/videos/#{uuid}",
            { Authorization: "#{access_tokens['token_type']} #{access_tokens['access_token']}" }
          )
        end
      end

      def self.login_as_admin(instance)
        # https://github.com/rest-client/rest-client
        # https://docs.joinpeertube.org/api-rest-reference.html#operation/getOAuthClient

        logger.info('Log in as an Peertube\'s administrator')

        # Peertube's admin
        url = instance.option_by_name('url_api')
        username = instance.option_by_name('username')
        password = instance.option_by_name('password')

        # You need to retrieve a client id and secret before logging in.
        response = RestClient.get("#{url}/oauth-clients/local")
        res = JSON.parse(response.body)

        # With your client id and secret, you can retrieve an access and refresh tokens.
        parameter = { client_id: res['client_id'], client_secret: res['client_secret'], grant_type: 'password',
                      response_type: 'code', username:, password: }
        response = RestClient.post "#{url}/users/token", parameter
        access_token = JSON.parse(response.body)
      end

      # LDAP

      def self.enable_peertube_authentication_for(user, instance)
        logger.info("Peertube - Enabling ldap authentication for #{user.email}")
        user.zouritPeertube = true
        user.zouritPeertubeInstance = instance.id.to_s
        user.save!
      end

      def self.disable_peertube_authentication_for(user)
        logger.info("Peertube - Disabling ldap authentication for #{user.email}")

        # user.zouritPeertube = false
        user.zouritPeertubeInstance = 'nil'
        user.save!
      end

      def self.use_ldap_to_authenticate_the_user(username_or_email, instance)
        # https://docs.joinpeertube.org/api-rest-reference.html#operation/putUser
        logger.info("Peertube - Use ldap to authenticate the user #{username_or_email}")

        url = instance.option_by_name('url_api')
        access_tokens = login_as_admin(instance)
        user_id = get_user_id(username_or_email, instance, access_tokens)

        RestClient.put(
          "#{url}/users/#{user_id}",
          { pluginAuth: 'peertube-plugin-auth-ldap' },
          { Authorization: "#{access_tokens['token_type']} #{access_tokens['access_token']}" }
        )
      end
    end
  end
end
