# frozen_string_literal: true

module Core
  module Nextcloud
    include SemanticLogger::Loggable

    class Main < ModuleBase
      def self.get_logout_url(service_context)
        get_login_url(service_context)
      end

      def self.destroy_domain(_domain_info, _domain)
        # TODO: suppression de tous les dossiers utilisateurs du domaine
      end

      def self.create_user(user)
        instance_name = user.domain.get_domain_info('Nextcloud').instance.name
        [user.update(zouritOwncloud: true, zouritTheme: instance_name), '', '']
      end

      def self.destroy_user(user, _meta_option)
        output, exit_code = nextcloud_ssh user.domain.get_module_instance('Nextcloud'),
                                          "ldap:reset-user #{user.id} -y"
        unless exit_code.zero?
          logger.warn("unable to destroy user <#{user.id}>, email <#{user.email}> due to: #{output}")
        end
        user.update(zouritOwncloud: false)
        [output, exit_code]
      end

      def self.update_user_option(meta_option, user, _old_value, new_value)
        logger.info("update_user_option Nextcloud, #{meta_option.key}, #{user.name}, #{new_value}")
        case meta_option.key.to_sym
        when MetaOption::QUOTA
          [user.update(zouritCloudQuota: new_value), '', '']
        end
      end

      def self.change_locale(user, locale)
        logger.info("change locale <#{locale}> in Nextcloud for user <#{user.name}, #{user.email}, #{user.id}>")
        output, exit_code = nextcloud_ssh user.domain.get_module_instance('Nextcloud'),
                                          "user:setting #{user.id} core lang #{locale}"
        return if exit_code.zero?

        # raise "cannot change locale <#{locale}> to user <#{user}> : #{output}"
        logger.error "cannot change locale <#{locale}> to user <#{user}> : #{output}"
        ['', 1]
      end

      # TODO: factorize ssh_exec! with Zimbra module too!!!
      # this method is able to handle exit code properly, returns stdout if success,
      # else a mix of stdout+stderr meanwhile the exit code
      def self.ssh_exec!(host, user, command)
        stdout_data = ''
        stderr_data = ''
        exit_code = 0
        begin
          Net::SSH.start(host, user, {
                           auth_methods: %w[publickey],
                           keys: [ZOURIT_SSH_PRIVATE_KEY],
                           use_agent: false
                         }) do |ssh|
            ssh.open_channel do |channel|
              logger.info("execute ssh command on #{user}@#{host}: #{command}")
              channel.exec(command) do |_ch, success|
                abort "FAILED: couldn't execute command (ssh.channel.exec)" unless success
                channel.on_data { |_ch, data| stdout_data += data }
                channel.on_extended_data { |_ch, _type, data| stderr_data += data }
                channel.on_request('exit-status') { |_ch, data| exit_code = data.read_long }
              end
            end
            ssh.loop
            stdout_data = stdout_data.split.join(' ')
            if exit_code.zero?
              [stdout_data, 0]
            else
              stderr_data = stderr_data.blank? ? stdout_data : stderr_data.split.join(' ')
              logger.warn("failure detected, exit code=#{exit_code} : #{stderr_data}")
              [stderr_data, exit_code]
            end
          end
        rescue StandardError => e
          [e.message, 100]
        end
      end

      def self.nextcloud_ssh(instance, command)
        instance_options = build_instance_options(instance)
        full_command = "sudo -u #{instance_options[:command_run_as]} --
            #{instance_options[:command_occ]}
            #{command}
            ".split.join(' ')
        ssh_exec!(instance_options[:ssh_host], instance_options[:ssh_user], full_command)
      end
    end
  end
end
