# frozen_string_literal: true

module Core
  module Zimbra
    class MailExistError < StandardError; end

    class Main < ModuleBase
      include SemanticLogger::Loggable
      include UsersHelper

      def self.create_domain(domain_info, domain)
        logger.info "Création du domaine #{domain.name} dans Zimbra"
        success = zimbra_cd domain_info, domain
        if success
          logger.info "Création de la liste de diffusion tous@#{domain.name}"
          zimbra_cdl_tous domain_info, domain

          logger.info "Préparation de la clé d'authentification de #{domain.name}"
          success, value = get_preauthkey(domain_info, domain)

          if success
            logger.info "Preauthkey = #{value}"

            meta_option = MetaOption.where(
              key: 'authkey',
              field: 'domain',
              config_module_id: domain_info.config_module_id
            ).first

            domain_option = DomainOption.create(
              meta_option_id: meta_option.id,
              value:,
              domain_info_id: domain_info.id
            )
            success = domain_option.valid?
            unless success
              logger.error("DomainOption invalid! meta_option_id: #{meta_option.id} domain_info_id: #{domain_info.id}")
            end

          end
        end
        success
      end

      def self.min_quota
        1 # On oblige 1 Go minimum pour zimbra
      end

      def self.destroy_domain(domain_info, domain)
        logger.info "Suppresion de la liste de diffusion tous@#{domain.name}"
        list = Group.find("tous@#{domain.name}") || Group.new(name: "tous@#{domain.name}")
        zimbra_ddl list
        logger.info "Suppression du domaine #{domain.name} de Zimbra"
        zimbra_dd domain_info, domain.name
      end

      def self.create_user(user)
        logger.info "Création de l'utilisateur #{user.email} dans Zimbra"
        zimbra_ca user
      end

      def self.destroy_user(user, _meta_option)
        logger.info "Suppression de l'utilisateur #{user.email} de Zimbra"
        result, message, = zimbra_da user
        if !result && message.include?('NO_SUCH_ACCOUNT')
          logger.warn "User account doesn't exist on Zimbra : Ok we can go on"
          result = true
          message = ''
        end
        [result, message, '']
      end

      def self.update_user(user, old_user)
        requete = ''
        if (user.first_name != old_user.first_name) || (user.last_name != old_user.last_name)
          requete += "displayName \"#{user.first_name} #{user.last_name}\" givenName \"#{user.last_name}\" "
        end
        if requete.empty?
          true
        else
          logger.info "Mise à jour de l'utilisateur #{user.email} dans Zimbra"
          zimbra_ma user, requete
        end
      end

      def self.update_user_option(meta_option, user, old_value, new_value)
        logger.info "update_user_option Zimbra, #{meta_option.key}, #{user.name}, old_value=#{old_value}, new_value=#{new_value} ##"

        case meta_option.key.to_sym
        when MetaOption::QUOTA
          requete = "zimbraMailQuota \"#{new_value.to_i * (1024**3)}\" "
          zimbra_ma user, requete
        when :calendar
          requete = "zimbraFeatureCalendarEnabled \"#{new_value.to_s.upcase}\" "
          requete += 'zimbraFeatureGroupCalendarEnabled "TRUE" '
          zimbra_ma user, requete
        when :task
          requete = "zimbraFeatureTasksEnabled \"#{new_value.to_s.upcase}\" "
          zimbra_ma user, requete
        when :contact
          requete = "zimbraFeatureContactsEnabled \"#{new_value.to_s.upcase}\" "
          zimbra_ma user, requete
        when :aliases
          old_aliases = begin
            JSON.parse(old_value).map { |_, v| v }
          rescue StandardError
            {}
          end
          new_aliases = begin
            JSON.parse(new_value).map { |_, v| v }
          rescue StandardError
            {}
          end

          if old_aliases.any?
            to_delete = old_aliases - (old_aliases & new_aliases)
            to_delete.each do |a|
              complete_alias = "#{a}@#{user.domain.name}"
              logger.info("Remove alias : #{complete_alias}")
              zimbra_ralias(user.email, complete_alias)
            end

            to_add = new_aliases - (old_aliases & new_aliases)
          else
            to_add = new_aliases
          end
          if to_add.count.positive?
            to_add.each do |a|
              complete_alias = "#{a}@#{user.domain.name}"
              found, payload = mail_exists?(complete_alias)
              if found
                logger.warn("impossible de créer l'alias <#{complete_alias}> car: #{payload}")
                return [false, I18n.t('modules.zimbra.mailExistError', email: complete_alias), '']
              end

              logger.info("Create alias : #{complete_alias}")
              zimbra_aalias(user.email, complete_alias)
            end
          else
            [true, '', '']
          end
        end
      end

      def self.enabled?(user, mod)
        user.enabled_from_user_options?(mod)
      end

      def self.change_locale(user, locale)
        requete = "zmprov ma #{user.email} zimbraPrefLocale #{locale}"
        success, message, output = zimbra_ssh user.domain.get_module_instance('Zimbra'), requete
        [success, message, output]
      end

      def self.test_ssh
        logger.info 'test connexion SSH avec clef publique...'

        options = {
          auth_methods: %w[publickey],
          keys: [ZOURIT_SSH_PRIVATE_KEY],
          use_agent: false
        }

        Net::SSH.start('mithril.re', 'pvincent', options) do |ssh|
          output = ssh.exec!('ls -la')
          logger.info "SSH Output <= #{output}"
        end
      end

      # ACCOUNT -----------------------------------------------
      # create account
      def self.zimbra_ca(user)
        requete = "zmprov ca #{user.email} \"\" displayName \"#{user.first_name} #{user.last_name}\" "
        requete += "givenName \"#{user.last_name}\" "
        requete += 'zimbraFeatureMailEnabled "TRUE" '
        requete += 'zimbraPrefSkin "zourit" '
        requete += 'zimbraPrefIncludeSharedItemsInSearch "TRUE" '
        requete += 'zimbraPrefShowSearchString "TRUE" '
        requete += 'zimbraPrefShowSelectionCheckbox "TRUE" '
        requete += 'zimbraPrefColorMessagesEnabled "TRUE" '
        requete += 'zimbraPrefAutoAddAddressEnabled "TRUE" '
        requete += zimbra_features(user)
        zimbra_ssh user.domain.get_module_instance('Zimbra'), requete
      end

      # delete account
      def self.zimbra_da(user)
        requete = "zmprov da #{user.email}"
        zimbra_ssh user.domain.get_module_instance('Zimbra'), requete
      end

      # modify account
      def self.zimbra_ma(user, req = nil)
        requete = "zmprov ma #{user.email} "
        requete += req || zimbra_features(user)
        zimbra_ssh user.domain.get_module_instance('Zimbra'), requete
      end

      # FIXME: reuse ssh_exec!(host, user, command) in Nextcloud module

      def self.zimbra_ssh(instance, requete)
        if instance && requete
          options = {
            auth_methods: %w[publickey],
            keys: [ZOURIT_SSH_PRIVATE_KEY],
            use_agent: false
          }
          instance_options = build_instance_options(instance)
          begin
            Net::SSH.start(instance_options[:ssh_host],
                           instance_options[:ssh_user], options) do |ssh|
              logger.info requete
              output = ssh.exec!(I18n.transliterate(requete))
              success = !(output.to_s.include? 'ERROR')
              message = success ? '' : "Zimbra: #{output}"
              logger.error output unless success
              return success, message, output
            end
          rescue StandardError => e
            logger.error "unable to connect through SSH: <#{instance_options[:ssh_user]}@#{instance_options[:ssh_host]}> with key <#{options[:keys][0]}>",
                         e
            [false, "Zimbra Error: #{e}", '']
          end
        else
          [true, '', '']
        end
      end

      def self.zimbra_features(user)
        features = ''
        features += user.zouritZimbraTask ? 'zimbraFeatureTasksEnabled "TRUE" ' : 'zimbraFeatureTasksEnabled "FALSE" '
        features += user.zouritZimbraContact ? 'zimbraFeatureContactsEnabled "TRUE" ' : 'zimbraFeatureContactsEnabled "FALSE" '
        features += user.zouritZimbraCalendar ? 'zimbraFeatureCalendarEnabled "TRUE" ' : 'zimbraFeatureCalendarEnabled "FALSE" '
        features += "zimbraMailQuota \"#{user.zouritZimbraQuota.to_i * 1024 * 1024 * 1024}\" "
        features
      end

      # DOMAIN -----------------------------------------------

      # create domain
      def self.zimbra_cd(domain_info, domain)
        requete = "zmprov cd #{domain.name} zimbraAuthMech ldap zimbraAuthLdapSearchBase dc=zourit,dc=re zimbraAuthLdapSearchFilter mail=%n zimbraAuthLdapURL ldap://#{SUPERLDAP.host}:#{SUPERLDAP.port}"
        success, = zimbra_ssh domain_info.instance, requete
        success
      end

      # delete domain
      def self.zimbra_dd(domain_info, domain)
        requete = "zmprov dd #{domain}"
        success, message, output = zimbra_ssh domain_info.instance, requete
        success
      end

      def self.get_preauthkey(domain_info, domain)
        requete = "zmprov generateDomainPreAuthKey #{domain.name}"
        success, _, output = zimbra_ssh domain_info.instance, requete
        [success, output.to_s.gsub('preAuthKey: ', '').chomp]
      end

      # DISTRIBUTION LIST -----------------------------------------------

      # création d'une liste de diffusion
      def self.zimbra_cdl(group)
        requete = "zmprov cdl #{group.name}"
        zimbra_ssh group.domain.get_module_instance('Zimbra'), requete
      end

      # create dynamic liste "tous"
      def self.zimbra_cdl_tous(domain_info, domain)
        dc = 'ou=people,'
        domain.name.split('.').each do |d|
          dc += "dc=#{d},"
        end
        dc.chop!

        requete = "zmprov cddl tous@#{domain.name} memberURL \
    'ldap:///#{dc}??sub?(&(zimbraAccountStatus=active)\
    (!(zimbraIsSystemResource=TRUE))(!(zimbraIsSystemAccount=TRUE))\
    (!(zimbraIsExternalVirtualAccount=TRUE)))' zimbraIsACLGroup FALSE"

        zimbra_ssh domain_info.instance, requete
      end

      # ajouter users à une liste
      def self.zimbra_adlm(list, users)
        requete = "zmprov adlm #{list.name} #{users}"
        zimbra_ssh list.domain.get_module_instance('Zimbra'), requete
      end

      # retirer users à une liste
      def self.zimbra_rdlm(list, users)
        requete = "zmprov rdlm #{list.name} #{users}"
        logger.info users
        zimbra_ssh list.domain.get_module_instance('Zimbra'), requete
      end

      # suppression d'une liste de diffusion
      def self.zimbra_ddl(list)
        requete = "zmprov ddl #{list.name}"
        zimbra_ssh list.domain.get_module_instance('Zimbra'), requete
      end

      # ajouter une nouvelle ressource
      def self.zimbra_ccr(resource)
        displayname = resource.name.split('@')[0]
        requete = "zmprov ccr #{resource.name} #{SecureRandom.hex(8)} displayName #{displayname} zimbraCalResType #{resource.type}"
        zimbra_ssh resource.get_domain.get_module_instance('Zimbra'), requete
      end

      def self.zimbra_dcr(resource)
        requete = "zmprov dcr #{resource.name}"
        zimbra_ssh resource.get_domain.get_module_instance('Zimbra'), requete
      end

      # ALIAS ==============================
      # Lister les alias d'un compte
      def self.zimbra_galias(email)
        requete = "zmprov ga #{email} | grep zimbraMailAlias"
        domain = Domain.find(email.split('@')[1])
        result, message, output = zimbra_ssh(domain.get_module_instance('Zimbra'), requete)
        aliases = []
        if output && !output.include?('ERROR:')
          output.split("\n").each do |a|
            a.gsub!('zimbraMailAlias: ', '')
            aliases << a
          end
        end
        aliases
      end

      # Lister les alias d'un compte
      def self.zimbra_galiases(instance_id)
        requete = "zmprov sa -v '(|(objectClass=zimbraAccount))'  | egrep '^(# name |zimbraMailAlias)'"
        _, _, output = zimbra_ssh(Instance.find(instance_id), requete)
        user = nil
        user_aliases = {}
        aliases = []
        if output && !output.include?('ERROR:')
          lines = output.split("\n")
          lines.each do |line|
            if line.start_with? '# name'
              user_aliases[user] = aliases if aliases.count.positive?
              user = line.gsub!('# name ', '')
              aliases = []
            else
              aliases << line.gsub('zimbraMailAlias: ', '')
            end
          end
        else
          logger.error(output)
        end
        user_aliases[user] = aliases if aliases.count.positive?
        user_aliases
      end

      # Ajouter un alias à un compte
      def self.zimbra_aalias(email, alias_email)
        domain = Domain.find(email.split('@')[1])
        requete = "zmprov aaa #{email} #{alias_email}"
        zimbra_ssh domain.get_module_instance('Zimbra'), requete
      end

      # Supprimer un alias d'un compte
      def self.zimbra_ralias(email, alias_email)
        domain = Domain.find(email.split('@')[1])
        requete = "zmprov raa #{email} #{alias_email}"
        zimbra_ssh domain.get_module_instance('Zimbra'), requete
      end
      # FIN ALIAS ==========================

      # url de logout
      def self.get_logout_url(service_context)
        url = get_module_url service_context
        url += '/?loginOp=logout' if url
        url
      end

      # url de pré-authentification
      def self.get_login_url(service_context)
        url = get_module_url service_context
        key = get_authkey service_context.domain_info
        if key
          time = (Time.now.to_f * 1000).to_i
          account = service_context.user.email
          preauth = compute_preauth(account, time, key)
          url += "/service/preauth?account=#{account}&expires=0&timestamp=#{time}&preauth=#{preauth}"
        end
        url
      end

      # jeton de pré-autjentication
      def self.compute_preauth(name, time_st, authkey)
        plaintext = "#{name}|name|0|#{time_st}"
        key = authkey

        hmacd = OpenSSL::HMAC.new(key, OpenSSL::Digest.new('SHA1'))
        hmacd.update(plaintext)
        hmacd.to_s
      end

      def self.get_authkey(domain_info)
        option_id = MetaOption.where(field: 'domain', key: 'authkey').first.id
        DomainOption.where(meta_option_id: option_id, domain_info_id: domain_info.id).first.value
      end

      # Récupérer l'id du module
      def self.get_module_id
        config_module = ConfigModule.where(name: 'Zimbra').first
        config_module ? config_module.id : nil
      end

      def self.get_module_url(service_context)
        service_context.domain_info.instance ? service_context.domain_info.instance.option_by_name('url') : nil
      end

      def self.get_domain_datas(domain)
        domainInfo = get_domaininfo domain
        domainInfo ? eval(domainInfo.data) : nil
      end

      def self.get_domaininfo(domain)
        domain_id = domain.id
        domainInfo = DomainInfo.where(domain_id:, config_module_id: get_module_id).first
      end
    end
  end
end
