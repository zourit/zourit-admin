# frozen_string_literal: true

module Core
  module Survey
    class Main < ModuleBase
      def self.destroy_user(user, _meta_option)
        [::Survey.where(owner_id: user.id).destroy_all, '', '']
      end
    end
  end
end
