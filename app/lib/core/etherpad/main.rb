# frozen_string_literal: true

module Core
  module Etherpad
    class Main < ModuleBase
      def self.destroy_user(user, _meta_option)
        res = Pad.where(owner_id: user.id).destroy_all
        [res, '', '']
      end
    end
  end
end
