json.extract! form, :id, :title, :description, :owner_id, :end_date, :created_at, :updated_at
json.url form_url(form, format: :json)
