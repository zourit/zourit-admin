json.extract! formline, :id, :title, :description, :form_id, :type, :value, :order, :show_title, :created_at, :updated_at
json.url formline_url(formline, format: :json)
