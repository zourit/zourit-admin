json.extract! domain_category, :id, :category, :created_at, :updated_at
json.url domain_category_url(domain_category, format: :json)
