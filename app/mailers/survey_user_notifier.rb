class SurveyUserNotifier < ApplicationMailer
  def invit_email(user, to, survey, url, host)
    @from = user
    @survey = survey
    @url = url
    @host = host
    mail(from: "#{ZouritAdmin::Application::NOTIFICATION_SENDER['name']} <#{ZouritAdmin::Application::NOTIFICATION_SENDER['email']}>",
         reply_to: "#{@from.name} <#{@from.email}>",
         bcc: to,
         subject: 'Invitation à un sondage')
  end
end
