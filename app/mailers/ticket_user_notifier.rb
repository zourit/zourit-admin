class TicketUserNotifier < ApplicationMailer
  def email_creation_ticket(user, to, ticket, url, _messages)
    @from = user
    @ticket = ticket
    @url = url
    mail(from: "#{ZouritAdmin::Application::NOTIFICATION_SENDER['name']} <#{ZouritAdmin::Application::NOTIFICATION_SENDER['email']}>",
         reply_to: "#{@from.name} <#{@from.email}>",
         bcc: to,
         subject: I18n.t('tickets.create_message', id: @ticket.id, title: @ticket.title))
  end

  def email_notif_ticket(user, to, ticket, url, message)
    @from = user
    @ticket = ticket
    @url = url
    @message = message
    mail(from: "#{ZouritAdmin::Application::NOTIFICATION_SENDER['name']} <#{ZouritAdmin::Application::NOTIFICATION_SENDER['email']}>",
         reply_to: "#{@from.name} <#{@from.email}>",
         bcc: to,
         subject: I18n.t('tickets.email_notification', id: @ticket.id, from: @from.name))
  end
end
