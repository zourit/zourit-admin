class VisioUserNotifier < ApplicationMailer
  def invit_email(user, to, visio, url, host)
    @from = user
    @visio = visio
    @url = url
    @host = host
    mail(from: "#{ZouritAdmin::Application::NOTIFICATION_SENDER['name']} <#{ZouritAdmin::Application::NOTIFICATION_SENDER['email']}>",
         reply_to: "#{@from.name} <#{@from.email}>",
         bcc: to,
         subject: 'Invitation à une visioconférence')
  end
end
