class InvitationUserNotifier < ApplicationMailer
  def invit_email(user, to, invitation, url, host, tag)
    @tag = tag
    @from = user
    @invitation = invitation
    @url = url
    @host = host
    attachments.inline['logo-zourit.png'] = File.read("#{Rails.root}/app/assets/images/commun/logo-zourit.png")
    mail(from: "#{ZouritAdmin::Application::NOTIFICATION_SENDER['name']} <#{ZouritAdmin::Application::NOTIFICATION_SENDER['email']}>",
         reply_to: "#{@from.name} <#{@from.email}>",
         to:,
         subject: (I18n.t "invitation.notifier.#{@tag}.subjet").to_s)
  end
end
