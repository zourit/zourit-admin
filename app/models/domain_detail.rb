class DomainDetail < ApplicationRecord
  belongs_to :domain_category, optional: true
  self.primary_key = :domain_id

  def domain
    Domain.find(domain_id)
  end

  def quota
    domain.zouritDomainQuota
  end
end
