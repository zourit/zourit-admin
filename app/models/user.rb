# frozen_string_literal: true

# this is not a standard ActiveRecord model!
# it acts as an ActiveModel and provides underneath calls to LDAP

# < ApplicationRecord
# we can't use  before_action :at_least_one_domain_admin, on: [ :create, :update, :destroy ]

class User
  extend ActiveModel::Naming
  include ActiveModel::Conversion
  include ActiveModel::Validations
  include UsersHelper
  include DomainsHelper
  include GroupsHelper
  include SemanticLogger::Loggable

  attr_accessor :first_name, :last_name, :email, :admin, :superadmin,
                :password, :password_confirmation,
                :zouritZimbraQuota, :zouritCloudQuota,
                :zouritZimbraEmail, :zouritZimbraContact, :zouritZimbraCalendar,
                :zouritZimbraTask, :zouritOwncloud, :theme, :zouritPeertube, :zouritPeertubeInstance,
                :granted_modules, :cloud_instance, :locale

  attr_reader :dn, :id

  # Ces validation sont exécuter au moment de la création d'un user mais pas de sa modification (pas ActiveRecord)
  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP, message: :email_only }
  validates_confirmation_of :password
  validates_presence_of :password_confirmation

  validates :first_name, presence: true, length: { in: 1..256 }
  validates :password, presence: true, length: { in: 1..256 }
  validates :email, presence: true, length: { in: 1..256 }
  

  def initialize(params = {})
    @id = params.fetch(:id, nil)
    @dn = params.fetch(:dn, nil)

    self.first_name = params.fetch(:first_name, nil)
    self.last_name = params.fetch(:last_name, nil)
    self.last_name = last_name.capitalize unless last_name.nil?

    self.email = params.fetch(:email, nil)
    self.email = email.downcase if email
    self.admin = params.fetch(:admin, false)
    self.theme = params.fetch(:theme, ZouritAdmin::Application::DEFAULT_THEME)
    self.granted_modules = params.fetch(:granted_modules, [])
    self.locale = params.fetch(:locale, I18n.default_locale)
    self.superadmin = params.fetch(:superadmin, false)
    self.zouritZimbraQuota = params.fetch(:zouritZimbraQuota, 0)
    self.zouritZimbraTask = params.fetch(:zouritZimbraTask, false)
    self.zouritZimbraEmail = params.fetch(:zouritZimbraEmail, false)
    self.zouritZimbraContact = params.fetch(:zouritZimbraContact, false)
    self.zouritZimbraCalendar = params.fetch(:zouritZimbraCalendar, false)
    self.zouritOwncloud = params.fetch(:zouritOwncloud, false)
    self.zouritCloudQuota = params.fetch(:zouritCloudQuota, 0)
    self.zouritPeertube = params.fetch(:zouritPeertube, false)
    self.zouritPeertubeInstance = params.fetch(:zouritPeertubeInstance, '')
    self.granted_modules = params.fetch(:granted_modules, [])
  end

  def persisted?
    id.present?
  end

  def new_record?
    !persisted?
  end

  def aliases
    # FIXME: attention ici on utilise un string 'Liste des alias' pour retrouver la métaoption, comment éviter ça ?
    aliases_list_metaoption = MetaOption.where(description: 'Liste des alias').first

    alias_hash_for_user = UserOption.where(user_id: id, meta_option: aliases_list_metaoption).first
    return [] unless alias_hash_for_user # pas d'alias, on retourne vide

    domain_name = domain_name_from_mail email
    aliases = alias_hash_for_user.value # JSON hash
    aliases_hash = begin
      JSON.parse(aliases)
    rescue StandardError
      {}
    end
    aliases_hash.map do |_, value|
      "#{value}@#{domain_name}"
    end
  end

  def domain
    domain_from_mail email
  end

  def self.find(opts)
    user = UUID.validate(opts) ? find_user_by_uuid(opts) : find_user_by_mail(opts)
    if user
      user.theme = UserInfo.where(user_id: user.id).first&.theme || 'theme3'
      user.locale = UserInfo.where(user_id: user.id).first&.locale || I18n.default_locale
      user.cloud_instance = user.domain&.get_domain_info('Nextcloud')&.instance&.name
    end
    user
  end

  def self.find_by_dn(dn)
    find_user_by_dn dn
  end

  def self.find_by_uuid(uuid)
    find_user_by_uuid uuid
  end

  def self.list_from_domain(domain)
    list_users(domain.name)
  end

  def self.all
    list_all_users
  end

  def update(params)
    logger.info "user <#{email}> about to update with params #{params}"

    old_user = User.find(id)

    params.each do |key, value|
      @reste = 0
      domain.set_attributed_quota
      @reste_quotadomain = domain.zouritDomainQuota - domain.zouritAttributedQuota
      case key.to_sym
      when :first_name
        self.first_name = value
      when :last_name
        self.last_name = value
      when :admin
        self.admin = (value == 'true')
      when :zouritCloudQuota
        @reste += value.to_i - (zouritCloudQuota.to_i / 1024 / 1024 / 1024)
        self.zouritCloudQuota = (value.to_i * (1024**3))
      when :zouritOwncloud
        self.zouritOwncloud = value
      when :zouritPeertube
        self.zouritPeertube = value
      when :zouritPeertubeInstance
        self.zouritPeertubeInstance = value
      when :theme
        self.theme = value
      when :locale
        self.locale = value
      end

      case key
      when 'password'
        if value.length.positive?
          @password = value
        else
          logger.info('Mot de passe non enregistré !')
        end
      when 'password_confirmation'
        if value.length > 0
          @password_confirmation = value
        else
          logger.info('Mot de passe non enregistré !')
        end
      end
    end
    if @reste <= @reste_quotadomain
      logger.info 'ok pour les quotas, on sauvegarde via LDAP'
      return false unless save!
      return false unless update_theme
      return false if locale != old_user&.locale && !set_locale(locale)

      update_user_in_modules old_user
    else
      false
    end
  end

  def save!
    if !password&.blank? && !password =~ ZouritAdmin::Application::PASSWORD_PATTERN
      errors.add(:password, ZouritAdmin::Application::PASSWORD_POLICY)
      return false
    end

    self.cloud_instance = domain&.get_domain_info('Nextcloud')&.instance&.name

    if dn # Modification d'utilisateur
      opts = { sn: last_name,
               cn: first_name,
               mail: email,
               objectclass: objectclass_array,
               zouritadmin: admin ? 'TRUE' : 'FALSE',
               zouritzimbraquota: zouritZimbraQuota.to_s,
               zouritcloudquota: zouritCloudQuota.to_s,
               displayName: "#{first_name} #{last_name}",
               zourittheme: cloud_instance.to_s }
      opts[:zouritPeertubeInstance] = zouritPeertubeInstance.to_s if zouritPeertube
      opts[:userpassword] = Net::LDAP::Password.generate(:sha, password) if password
      update_user email, opts
    elsif create_user(email, first_name, last_name, password,
                      objectclass_array, admin, zouritZimbraQuota,
                      zouritCloudQuota, zouritPeertubeInstance)
      # raise errors.full_messages[0] unless valid?

      complete(email)
      domain = Domain.find(domain_name_from_mail(email))
      groupe = Group.find_with_user("tous@#{domain.name}")
      if groupe
        groupe.add_user [id]
      else
        groupe = Group.new(name: "tous@#{domain.name}", domain:, member: [self])
        groupe.save
      end
    else
      false
    end
  end

  def update_theme
    ui = UserInfo.where(user_id: @id).first_or_create
    ui.theme = theme
    ui.save
  end

  def darkmode?
    UserInfo.where(user_id: @id)&.first&.darkmode
  end

  def set_darkmode(mode)
    ui = UserInfo.where(user_id: @id).first_or_create
    ui.darkmode = mode
    ui.save
  end

  def complete(email)
    u = User.find(email)
    @id = u.id
    @dn = u.dn
  end

  def update_user_in_modules(_old_user)
    domain.config_modules.each do |mod|
      eval("Core::#{mod.name}::Main.update_user(self,_old_user)") if enabled?(mod)
    rescue StandardError => e
      logger.info "unable to update user in module #{mod}", e
      return false
    end
  end

  def update_locale_in_modules(_user, _locale)
    domain.config_modules.each do |mod|
      eval("Core::#{mod.name}::Main.change_locale(_user,_locale)") if enabled?(mod)
    rescue StandardError => e
      logger.error "unable to change locale in module #{mod}", e
      errors.add(:locale, "fail in module #{mod}")
      return false
    end
  end

  def destroy
    # delete_from_tous_group
    domain_infos.each do |di|
      meta_option = di.config_module.user_options_enabled
      eval("Core::#{di.config_module.name}::Main.update_user_option_plus_enabled(meta_option, self, 'true','false', di.config_module)")
    end
    logger.info { "UserOptions => Supression de l'utilisateur #{email}" }
    user_options.destroy_all
    logger.info { "LDAP => Suppression de l'utilisateur #{email}" }
    destroy_user @dn
  end

  def delete_from_tous_group
    domain = Domain.find(domain_name_from_mail(email))
    groupe = Group.find_with_user("tous@#{domain.name}")
    groupe&.del_user [id]
  end

  def objectclass_array
    array = %w[top inetOrgPerson zourit]
    array << 'zouritZimbraEmail' if zouritZimbraEmail
    array << 'zouritZimbraTask' if zouritZimbraTask
    array << 'zouritZimbraContact' if zouritZimbraContact
    array << 'zouritZimbraCalendar' if zouritZimbraCalendar
    array << 'zouritOwncloud' if zouritOwncloud
    array << 'zouritPeertube' if zouritPeertube
    array
  end

  def self.authenticate(mail, password)
    ldap = Net::LDAP.new
    ldap.host = SUPERLDAP.host
    ldap.port = SUPERLDAP.port
    ldap.auth cn_by_mail(mail), password
    ldap.bind ? find(mail) : nil
  end

  def name
    "#{first_name} #{last_name}"
  end

  def full_name
    "#{last_name.upcase} #{first_name}"
  end

  def full_name_elipse
    fullname = full_name
    fullname = "#{fullname[0..30]}..." if fullname.length > 30
    fullname
  end

  def domain_infos
    domain.domain_infos.select { |di| enabled?(di.config_module) }
  end

  def instances
    domain_infos.map(&:instance)
  end

  def add_groups(groups_ids)
    groups_ids.each do |group_id|
      group = Group.find_with_user(group_id)
      group.add_user [id]
    end
  end

  def set_groups(new_groups_ids)
    old_groups = list_user_groups(self)
    old_groups.each do |g|
      logger.info "group <#{g.id}> <#{g.name}>"
    end

    old_group_ids = old_groups.map(&:id)

    groups_id_todelete = old_group_ids - new_groups_ids
    logger.info("groups_id_todelete = #{groups_id_todelete}")
    groups_id_todelete.each do |group_id|
      group = Group.find_with_user(group_id)
      group.del_user [id]
      logger.info("user #{id} delete from group #{group_id}")
    end

    groups_id_toadd = new_groups_ids - old_group_ids
    logger.info("groups_id_toadd = #{groups_id_toadd}")
    groups_id_toadd.each do |group_id|
      group = Group.find_with_user(group_id)
      group.add_user [id]
      logger.info("user #{id} add from group #{group_id}")
    end
  end

  def groups
    list_user_groups self, with_users = false, with_tous = true
  end

  def groups_uuid
    groups.collect(&:id)
  end

  def enabled?(mod)
    return false unless mod

    bind_call = get_binding_for_helper_module_call(mod)
    bind_call.eval("Core::#{mod.name}::Main.enabled?(self, mod)")
  end

  def enabled_from_user_options?(mod)
    meta_option = MetaOption.where(key: MetaOption::ENABLED, field: 'user',
                                   config_module_id: mod.id).first
    return false unless meta_option

    user_option = UserOption.where(user_id: id, meta_option_id: meta_option.id).first
    return false unless user_option

    user_option.value.to_s.downcase == 'true'
  end

  def user_option_value(option)
    user_option = UserOption.where(user_id: id, meta_option_id: option.id).first
    return nil unless user_option

    user_option.value
  end

  def user_options
    UserOption.where(user_id: id)
  end

  def modules
    populate_granted_modules if granted_modules.empty?
    ConfigModule.select { |mod| granted_modules.include?(mod.name) }.sort { |a, b| a.order <=> b.order }
  end

  def populate_granted_modules
    mods = ConfigModule.select { |mod| enabled?(mod) }.sort { |a, b| a.order <=> b.order }
    self.granted_modules = mods.collect(&:name)
  end

  def attributed_quotas
    meta_options_ids = MetaOption.where(field: 'user',
                                        field_type: 'Quota',
                                        config_module_id: modules.map(&:id)).map(&:id)
    UserOption.where(user_id: id, meta_option_id: meta_options_ids).map do |uo|
      uo.value.to_i
    end.reduce(0, :+)
  end

  def to_hash
    {
      id: @id,
      dn: @dn,
      first_name:,
      last_name:,
      email:,
      admin:,
      theme:,
      superadmin:,
      zouritZimbraQuota:,
      zouritZimbraTask:,
      zouritZimbraEmail:,
      zouritZimbraContact:,
      zouritZimbraCalendar:,
      zouritOwncloud:,
      zouritCloudQuota:,
      zouritPeertube:,
      zouritPeertubeInstance:,
      granted_modules:
    }
  end

  def get_locale
    UserInfo.where(user_id: id)&.first&.locale || I18n.default_locale
  end

  def set_locale(locale)
    ui = UserInfo.where(user_id: id).first_or_create
    ui.locale = locale
    ui.save
    update_locale_in_modules(self, locale)
  end

  private

  def get_binding_for_helper_module_call(mod)
    # si vous plantez ici, c'est que le module est mal écrit (méthode manquante)
    binding
  end
end
