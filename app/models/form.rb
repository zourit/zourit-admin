class Form < ApplicationRecord
  has_many :formlines, dependent: :destroy
end
