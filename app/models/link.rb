class Link < ApplicationRecord
  validates :url, format: { with: %r{\A(http|https)://.*\.[^.]+\z} }
  validates :title, presence: true
  validates :resume, presence: true
  validates :url, presence: true
  validates :color, presence: true
  validates :symbol, presence: true
  # validates :global_link, presence: true
end
