class InstanceState < ApplicationRecord
  has_many :instances
  has_many :instance_stories
end
