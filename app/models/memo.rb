class Memo < ApplicationRecord
  has_many :invitation_groups
  has_many :invitation_users
  has_many :memo_archives, dependent: :destroy
  before_destroy :delete_invitations

  def invitation_users
    InvitationUser.where(module_tag: 'memo', module_id: id)
  end

  def invitation_groups
    InvitationGroup.where(module_tag: 'memo', module_id: id)
  end

  def delete_invitations
    invitation_users.destroy_all
    invitation_groups.destroy_all
  end

  def owner
    User.find(owner_id)
  end

  def external_url(domain)
    mod = ConfigModule.where(name: 'Framemo').first
    url_server = mod.instance_for_domain(domain).option_by_name('url')
    url_embedded = url_server
    url_embedded += '/' unless url_server[-1] == '/'
    url_embedded += uuid

    url_embedded
  end

  def archived?(user)
    memo_archives.where(user_id: user.id).count.positive?
  end

  def archive(user)
    MemoArchive.where(memo_id: id, user_id: user.id).first_or_create
  end

  def unarchive(user)
    memo_archives.where(user_id: user.id).destroy_all
  end
end
