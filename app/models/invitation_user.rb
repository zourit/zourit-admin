class InvitationUser < ApplicationRecord
  belongs_to :visio, optional: true
  belongs_to :survey, optional: true
  belongs_to :pad, optional: true
end
