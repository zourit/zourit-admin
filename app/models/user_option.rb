class UserOption < ApplicationRecord
  belongs_to :meta_option

  def user
    User.find(user_id)
  end
end
