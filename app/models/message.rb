class Message < ApplicationRecord
  def user_external
    user = InvitationUser.where(uuid: user_id).first
    user
  end

  def user
    user = User.find(user_id)
    user
  end
end
