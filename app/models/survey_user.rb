class SurveyUser < ApplicationRecord
  belongs_to :survey
  validates :email, presence: true, allow_blank: false
end
