class Pad < ApplicationRecord
  has_many :invitation_groups
  has_many :invitation_users
  has_many :pad_archives, dependent: :destroy
  validates :name, presence: true
  before_destroy :delete_invitations

  def invitation_users
    InvitationUser.where(module_tag: 'pad', module_id: id)
  end

  def invitation_groups
    InvitationGroup.where(module_tag: 'pad', module_id: id)
  end

  def delete_invitations
    invitation_users.destroy_all
    invitation_groups.destroy_all
  end

  def owner
    User.find(owner_id)
  end

  def archived?(user)
    pad_archives.where(user_id: user.id).count.positive?
  end

  def archive(user)
    PadArchive.where(pad_id: id, user_id: user.id).first_or_create
  end

  def unarchive(user)
    pad_archives.where(user_id: user.id).destroy_all
  end
end
