class DomainInfo < ApplicationRecord
  belongs_to :config_module
  belongs_to :instance
  has_many :domain_options, dependent: :destroy

  after_create :create_domain
  before_destroy :destroy_domain

  def domain
    Domain.find(domain_id)
  end

  private

  def create_domain
    call_method_via_eval :create_domain
  end

  def destroy_domain
    call_method_via_eval :destroy_domain
  end

  def call_method_via_eval(method_name)
    domain = Domain.find(domain_id)
    mod = ConfigModule.find(config_module_id)

    # ici on utilise la fonction `eval` avec appel d'une fonction binding (__FILE__, __LINE__)
    bind_call = get_binding_for_helper_module_call(domain)
    bind_call.eval("Core::#{mod.name}::Main.#{method_name} self, domain")
  end

  def get_binding_for_helper_module_call(domain)
    # si vous plantez ici, c'est que le module est mal écrit (méthode manquante)
    binding
  end
end
