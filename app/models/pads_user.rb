class PadsUser < ApplicationRecord
  belongs_to :pad
  validates :mail, presence: true, allow_blank: false
end
