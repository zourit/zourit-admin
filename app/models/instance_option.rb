class InstanceOption < ApplicationRecord
  belongs_to :instance
  belongs_to :meta_option
end
