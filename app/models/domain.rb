# frozen_string_literal: true

class Domain
  extend ActiveModel::Naming
  include ActiveModel::Conversion
  include DomainsHelper
  include UsersHelper
  include GroupsHelper
  include SemanticLogger::Loggable

  attr_accessor :name, :zouritDomainQuota, :zouritAttributedQuota,
                :zouritQuotaLeft, :nbUsersDomain, :details
  attr_reader :id

  def initialize(name = nil, id = nil, zouritDomainQuota = 0, zouritAttributedQuota = 0, nbUsersDomain = 0)
    self.name = name
    @id = id
    self.zouritDomainQuota = zouritDomainQuota
    self.zouritAttributedQuota = zouritAttributedQuota
    self.nbUsersDomain = nbUsersDomain
    self.details = DomainDetail.new
  end

  def persisted?
    false
  end

  def self.find(opts)
    domain = UUID.validate(opts) ? find_domain_by_uuid(opts) : find_domain_by_name(opts)
    domain&.details = DomainDetail.where(domain_id: domain.id).first
    domain
  end

  def self.all(with_quota = true)
    list_domains with_quota
  end

  def administrators
    get_administrators name
  end

  def save
    if @id
      # FIXME: ici il s'agit de la modification d'un domaine, j'ai l'impression qu'on ne passe jamais par ici !!!
      update_domain(name, zouritdomainquota: zouritDomainQuota)
    else # create domain
      create_domain name, zouritDomainQuota
      @id = Domain.find(name).id
      details.domain_id = @id
    end
    details.save
  rescue LdapError => e
    logger.error('unable to create domain', e)
    false
  end

  # Suppression du domaine
  def destroy
    logger.info("Suppression des utilisateurs du domaine <#{name}>...")
    list_users(name).each(&:destroy)

    logger.info("Suppresion des groupes du domaine <#{name}>...")
    list_groups(name, false, true).each(&:destroy)

    logger.info("Suppression du domaine <#{name}> pour chaque module...")
    success = true
    DomainInfo.where(domain_id: id).each do |di|
      module_name = di.config_module.name
      success &&= eval("Core::#{module_name}::Main.destroy_domain di, self")
    end

    logger.info("Suppression finale du domaine <#{name}>")
    # TODO: à vérifier si la ligne ci-dessous s'exécute bien... suspiscion de bug ici
    success &&= DomainInfo.where(domain_id: id).destroy_all
    success &&= details.destroy

    success ? destroy_domain(name) : false
  end

  # Calcul du quota déjà attribué
  def set_attributed_quota
    self.zouritAttributedQuota = attributed_quotas
    self.zouritQuotaLeft = zouritDomainQuota - zouritAttributedQuota
  end

  def set_nb_users
    self.nbUsersDomain = users.count
  end

  def attributed_quotas_excepted_users(excepted_user_id)
    meta_options_ids = MetaOption.where(field: 'user',
                                        field_type: 'Quota',
                                        config_module_id: config_modules.map(&:id))
    users_ids = users.map(&:id)
    users_ids = users_ids.excluding(excepted_user_id) if excepted_user_id
    UserOption.where(meta_option_id: meta_options_ids, user_id: users_ids).map do |uo|
      uo.value.to_i
    end.reduce(0, :+)
  end

  def attributed_quotas
    meta_options_ids = MetaOption.where(field: 'user',
                                        field_type: 'Quota',
                                        config_module_id: config_modules.map(&:id))
    UserOption.where(meta_option_id: meta_options_ids, domain_id: id).map do |uo|
      uo.value.to_i
    end.reduce(0, :+)
  end

  def self.all_attributed_quota
    meta_options_ids = MetaOption.where(field: 'user',
                                        field_type: 'Quota')
    UserOption.where(meta_option_id: meta_options_ids).map do |uo|
      uo.value.to_i
    end.reduce(0, :+)
  end

  def get_domain_info(module_name)
    mod = ConfigModule.where(name: module_name).first
    DomainInfo.where(domain_id: id, config_module_id: mod.id).first
  end

  def get_module_instance(module_name)
    di = get_domain_info(module_name)
    di ? di.instance : false
  end

  def domain_infos
    DomainInfo.where(domain_id: id)
  end

  def instances
    DomainInfo.where(domain_id: id).map(&:instance)
  end

  def config_modules
    DomainInfo.where(domain_id: id).map(&:config_module).sort { |a, b| a.order <=> b.order }
  end

  def users
    User.list_from_domain(self)
  end

  def groups(with_users: false, with_tous: false)
    list_groups(name, with_users, with_tous)
  end

  def full_name
    details.full_name || name
  end

  def address
    details.address
  end

  def phone
    details.phone
  end

  def email
    details.email
  end

  def admin_lastname; end
  def admin_firstname; end
  def admin_email; end
  def admin_password; end
  def admin_password_confirmation; end

  private

  def get_binding(object)
    binding
  end
end
