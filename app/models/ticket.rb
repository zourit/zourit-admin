class Ticket < ApplicationRecord
  belongs_to :ticket_status
  belongs_to :ticket_category
  belongs_to :instance, optional: true

  def user
    User.find(user_id)
  end

  def emails_list_ticket
    uuid_list = Message.select(:user_id).where(module_id: id, module_tag: 'ticket').distinct
    email_list = []
    uuid_list.each do |uuid|
      email_list << uuid.user.email if uuid.user && uuid.user_id != @current_user
    end
    email_list
  end
end
