class Visio < ApplicationRecord
  has_many :invitation_groups
  has_many :invitation_users
  has_many :visio_archives, dependent: :destroy
  before_destroy :delete_invitations

  validates_presence_of :date_start

  def invitation_users
    InvitationUser.where(module_tag: 'visio', module_id: id)
  end

  def invitation_groups
    InvitationGroup.where(module_tag: 'visio', module_id: id)
  end

  def delete_invitations
    invitation_users.destroy_all
    invitation_groups.destroy_all
  end

  def update(service_context, session, params)
    Core::BigBlueButton::Main.edit_room(service_context, session, friendly_id, params[:name]) if params[:name] != name
    super(params)
    send_configuration(service_context, session)
  end

  def destroy(service_context = nil, session = nil)
    begin
      Core::BigBlueButton::Main.delete_room(service_context, session, friendly_id) if service_context
    rescue RestClient::NotFound, RestClient::NotAcceptable
      logger.error "Erreur lors de la suppression de la visio ayant pour friendly_id #{friendly_id}. Elle n'existe pas sur BBB."
    end
    super()
  end

  def owner
    User.find(owner_id)
  end

  def archived?(user)
    visio_archives.where(user_id: user.id).count.positive?
  end

  def archive(user)
    VisioArchive.where(visio_id: id, user_id: user.id).first_or_create
  end

  def unarchive(user)
    visio_archives.where(user_id: user.id).destroy_all
  end

  def send_configuration(service_context, session)
    if moderator_approval_previously_changed?
      logger.info "moderator_approval change to #{moderator_approval}"
      Core::BigBlueButton::Main.update_room_settings(service_context, session, friendly_id, 'guestPolicy',
                                                     moderator_approval)
    end
    if anyone_can_start_previously_changed?
      logger.info "anyone_can_start change to #{anyone_can_start}"
      Core::BigBlueButton::Main.update_room_settings(service_context, session, friendly_id, 'glAnyoneCanStart',
                                                     anyone_can_start)
    end
    if anyone_join_as_moderator_previously_changed?
      logger.info "anyone_join_as_moderator change to #{anyone_join_as_moderator}"
      Core::BigBlueButton::Main.update_room_settings(service_context, session, friendly_id, 'glAnyoneJoinAsModerator',
                                                     anyone_join_as_moderator)
    end
    if mute_on_start_previously_changed?
      logger.info "mute_on_start change to #{mute_on_start}"
      Core::BigBlueButton::Main.update_room_settings(service_context, session, friendly_id, 'muteOnStart',
                                                     mute_on_start)
    end
    if allow_recording_previously_changed?
      logger.info "allow_recording change to #{allow_recording}"
      Core::BigBlueButton::Main.update_room_settings(service_context, session, friendly_id, 'record',
                                                     allow_recording)
    end

    true
  end
end
