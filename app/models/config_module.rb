# frozen_string_literal: true

# Pour gérer les modules
class ConfigModule < ApplicationRecord
  has_many :domain_infos
  has_many :tickets
  has_many :meta_options
  has_many :instances, dependent: :destroy

  def instance_options
    filter_meta_options :instance
  end

  def domain_options
    filter_meta_options :domain
  end

  def user_options
    filter_meta_options :user
  end

  def user_options_enabled
    filter_meta_options(:user).select { |mo| mo.key.to_sym == MetaOption::ENABLED }.first
  end

  def user_options_quota
    filter_meta_options(:user).select { |mo| mo.key == 'quota' }.first
  end

  def instance_for_domain(domain)
    DomainInfo.where(domain_id: domain.id, config_module_id: id)&.first&.instance
  end

  private

  def filter_meta_options(symbol)
    meta_options.select { |mo| mo.field == symbol.to_s }
  end
end
