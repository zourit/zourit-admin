class TicketCategory < ApplicationRecord
    has_many :tickets, dependent: :destroy
end
