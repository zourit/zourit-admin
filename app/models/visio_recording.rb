# this is not a standard ActiveRecord model!
# it acts as an ActiveModel and the data is not stored. Data is fetched every time from BigBlueButton Greenlight.

class VisioRecording
  extend ActiveModel::Naming
  include ActiveModel::Conversion

  attr_reader :id, :record_id, :name, :created_at, :recorded_at, :duration,
              :number_of_participants, :visibility, :formats

  def initialize(params = {})
    @id = params.fetch(:id, nil)
    @record_id = params.fetch(:record_id, nil)
    @name = params.fetch(:name, nil)
    @created_at = params.fetch(:created_at, nil).to_datetime
    @recorded_at = params.fetch(:recorded_at, nil).to_datetime
    @duration = params.fetch(:length, nil)
    @number_of_participants = params.fetch(:participants, nil)
    @visibility = params.fetch(:visibility, nil)
    @formats = params.fetch(:formats, nil)
  end
end
