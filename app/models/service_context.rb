class ServiceContext
  attr_accessor :domain_info, :domain, :user

  def initialize(domain_info, domain, user)
    self.domain_info = domain_info
    self.domain = domain
    self.user = user
  end
end
