class VisiosUser < ApplicationRecord
  belongs_to :visio
  validates :mail, presence: true, allow_blank: false
end
