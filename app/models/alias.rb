class Alias
  attr_accessor :email, :user_email

  def initialize(user_email, email)
    self.email = email
    self.user_email = user_email
  end

  def save
    Core::Zimbra::Main.zimbra_aalias user_email, email
  end

  def destroy
    Core::Zimbra::Main.zimbra_ralias user_email, email
  end

  def self.list(user_email)
    aliases = []
    brut = Core::Zimbra::Main.zimbra_galias user_email
    brut.each do |a|
      aliases << Alias.new(user_email, a)
    end
    aliases
  end

  def self.create(opts)
    if opts[:user_email] && opts[:email]
      Core::Zimbra::Main.zimbra_aalias opts[:user_email], opts[:email]
    else
      false
    end
  end

  def self.exist?(email)
    first_part = email.split('@').first
    cm = ConfigModule.where(name: 'Zimbra')
    mo = MetaOption.user_option(cm.id, 'aliases')
    domain = Domain.find(email.split('@').last)
    exist = false
    UserOption.where(domain_id: domain.id, meta_option_id: mo.id).each do |uo|
      aliases = uo.value.to_hash if uo.value
      aliases.each do |_k, v|
        exist = true if v == first_part
      end
    end
    exist
  end

  def domain
    Domain.find(email.split('@')[1])
  end
end
