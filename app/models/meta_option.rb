# frozen_string_literal: true

class MetaOption < ApplicationRecord
  belongs_to :config_module

  INSTANCE = 'instance'
  DOMAIN = 'domain'
  USER = 'user'
  ENABLED = :enabled
  QUOTA = :quota

  def set_user_option(user, value)
    # On ne lance la mise à jour que si il s'agit d'un field Enabled ou bien si l'option enabled du module est à true pour user
    return { result: true } unless key.to_sym == MetaOption::ENABLED || user.enabled?(config_module)

    user_option = UserOption.where(user_id: user.id, domain_id: user.domain.id, meta_option_id: id)
                            .first_or_initialize

    old_value = user_option.value
    # Si la valeur n'a pas changé on ne fait rien OU si c'était vide et la valeur est "false" en chaine de caractères
    return { result: true } if old_value == value || (old_value.nil? && value == 'false')

    logger.info "user_option for module <#{config_module.name}> has changed from <#{old_value}> to <#{value.inspect}>"

    # ici on utilise la fonction `eval` avec appel d'une fonction binding (__FILE__, __LINE__)
    bind_call = get_binding_for_helper_module_call(user, old_value, value)
    success, message, = bind_call.eval("Core::#{config_module.name}::Main.update_user_option_plus_enabled(self, user, old_value, value, config_module)")
    return { result: false, message: } unless success

    # sauvegarde si tout s'est bien passé à la fin
    { result: user_option.update!(value:) }
  end

  def user_options(user)
    UserOption.where(user_id: user.id, meta_option_id: id).first
  end

  def module_min_quota
    bind_call = get_binding_for_module
    bind_call.eval("Core::#{config_module.name}::Main.min_quota")
  end

  def module_default_quota
    bind_call = get_binding_for_module
    bind_call.eval("Core::#{config_module.name}::Main.default_quota")
  end

  class << self
    # useful shortcut for instance option
    def instance_option(config_module_id, key)
      option_of_type(INSTANCE, config_module_id, key)
    end

    # useful shortcut for domain option
    def domain_option(config_module_id, key)
      option_of_type(DOMAIN, config_module_id, key)
    end

    # useful shortcut for user option
    def user_option(config_module_id, key)
      option_of_type(USER, config_module_id, key)
    end

    # useful shortcut for user options
    def user_options(config_module_id)
      options_of_type(USER, config_module_id)
    end

    def option_of_type(type, config_module_id, key)
      where(field: type, config_module_id:, key:).first
    end

    def options_of_type(type, config_module_id)
      where(field: type, config_module_id:)
    end
  end

  private

  def get_binding_for_helper_module_call(user, old_value, value)
    # si vous plantez ici, c'est que le module est mal écrit (méthode manquante)
    binding
  end

  def get_binding_for_module
    # si vous plantez ici, c'est que le module est mal écrit (méthode manquante)
    binding
  end
end
