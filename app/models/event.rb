class Event < ApplicationRecord
  belongs_to :instance
  belongs_to :events_state
  has_many :messages

  def duration
    TimeDifference.between(start_date, end_date).humanize if end_date
  end
end
