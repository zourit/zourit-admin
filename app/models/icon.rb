# frozen_string_literal: true

class Icon
  extend ActiveModel::Naming
  include ActiveModel::Conversion
  include SemanticLogger::Loggable

  attr_accessor :name

  def initialize(name)
    self.name = name
  end
end
