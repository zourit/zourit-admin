class UserTheme < ApplicationRecord
  validates :user_id, :theme, presence: true
end
