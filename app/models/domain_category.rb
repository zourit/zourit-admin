class DomainCategory < ApplicationRecord
  validates :category, presence: true
end
