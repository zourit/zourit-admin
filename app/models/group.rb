class Group
  extend ActiveModel::Naming
  include ActiveModel::Conversion
  include GroupsHelper
  include SemanticLogger::Loggable

  attr_accessor :member, :name, :domain, :description
  attr_reader :dn, :id

  def initialize(params = {})
    @id = params.fetch(:id, nil)
    @dn = params.fetch(:dn, nil)
    self.member = params.fetch(:member, [])
    self.name = params.fetch(:name, nil)
    self.domain = params.fetch(:domain, nil)
    self.description = params.fetch(:description, '')
    if name&.include?('@')
      self.domain = Domain.find(name.split('@')[1])
    else
      self.name += "@#{domain.name}" unless domain.nil?
    end
  end

  def persisted?
    false
  end

  def self.find(opts)
    UUID.validate(opts) ? find_group_by_uuid(opts) : find_group_by_cn(opts)
  end

  def self.find_with_user(opts)
    UUID.validate(opts) ? find_group_by_uuid(opts, true) : find_group_by_cn(opts, true)
  end

  def update(member)
    self.member = member
  end

  def save
    nc_instance = domain.get_module_instance('Nextcloud')
    description = nc_instance ? nc_instance.name : '-'
    if id
      # si id déjà existant modification
      members = []
      member.compact!
      member.each do |m|
        members << m.dn
      end
      opts = { member: members, description: }
      update_group dn, opts
      logger.info("modification du groupe <#{name}>")
    elsif member.count.positive?
      # sinon nouvelle création de groupe
      @dn = "cn=#{self.name},ou=groups,o=#{domain.name}"
      if create_group(dn, member, description)
        if Core::Zimbra::Main.zimbra_cdl self
          logger.info("nouveau groupe <#{name}>")
          Core::Zimbra::Main.zimbra_adlm self, members_to_string
        else
          logger.warn("impossible crée groupe <#{name}> car problème de Zimbra")
          false
        end
      else
        logger.warn("impossible crée groupe <#{name}> car retour négatif de LDAP")
        false
      end
    else
      logger.warn 'Erreur de création : il doit au moins y avoir un utilisateur dans le groupe'
      false
    end
  end

  def add_single_user(user)
    if member?(user)
      true
    else
      member << user unless member?(user)
      Core::Zimbra::Main.zimbra_adlm(self, user.email) unless self.name.include?('tous@')
      save
    end
  end

  def add_user(ids)
    users_email = ''
    ids.each do |id|
      user = User.find_by_uuid id
      member << user unless member?(user)
      users_email += "#{user.email} "
    end
    Core::Zimbra::Main.zimbra_adlm(self, users_email) unless self.name.include?('tous@')
    { result: save }
  end

  def del_single_user(user)
    return { result: false, message: 'no_empty' } if member.count < 2

    if member?(user)
      member.each do |m|
        member.delete(m) if m.id == user.id
      end
      Core::Zimbra::Main.zimbra_rdlm self, user.email unless self.name.include?('tous@')
      { result: save }
    else
      { result: true }
    end
  end

  def del_user(ids)
    users_email = ''
    ids.each do |id|
      user = User.find_by_uuid id
      users_email += "#{user.email} "
      member.each do |m|
        member.delete(m) if m.id == id
      end
    end
    Core::Zimbra::Main.zimbra_rdlm self, users_email unless self.name.include?('tous@')
    save
  end

  def destroy
    Core::Zimbra::Main.zimbra_ddl self
    destroy_group dn
  end

  def member?(user)
    is_member = false
    member.each do |m|
      is_member = true if m && (m.id == user.id)
    end
    is_member
  end

  def members_to_string
    string = ''
    member.each do |m|
      string += "#{m.email} "
    end
    string
  end

  def short_name
    name.split('@').first
  end
end
