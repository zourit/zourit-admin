class Survey < ApplicationRecord
  has_many :survey_options, dependent: :destroy
  has_many :invitation_groups
  has_many :invitation_users
  has_many :messages
  before_destroy :delete_invitations, :delete_messages

  def number_participant
    options_survey = SurveyOption.where(survey_id: id).distinct
    number_participant = 0
    options_survey.each do |opt|
      number_participant += SurveyAnswer.where(survey_option_id: opt.id).distinct.count
    end
    number_participant
  end

  def invitation_users
    InvitationUser.where(module_tag: 'survey', module_id: id)
  end

  def invitation_groups
    InvitationGroup.where(module_tag: 'survey', module_id: id)
  end

  def messages
    Message.where(module_tag: 'survey', module_id: id)
  end

  def delete_invitations
    invitation_users.destroy_all
    invitation_groups.destroy_all
  end

  def delete_messages
    messages.destroy_all
  end

  def already_participated(uuid)
    options_survey = SurveyOption.where(survey_id: id).distinct
    SurveyAnswer.where(uuid:, survey_option_id: options_survey.ids).distinct.first
  end

  def name
    question
  end

  def owner
    User.find(owner_id)
  end
end
