# My Custom colorized formatter
class BasicFormatter < SemanticLogger::Formatters::Color
  ANSI_REVERSED_WARNING = "\e[0;30;43m".freeze
  ANSI_REVERSED_ERROR = "\e[1;30;41m".freeze
  ANSI_GRAY = "\e[90m".freeze
  # Return the complete log level name in uppercase

  def initialize
    super(time_format: '%H:%M:%S',
          color_map: ColorMap.new(
            debug: ANSI_GRAY,
            info: SemanticLogger::AnsiColors::GREEN,
            warn: SemanticLogger::AnsiColors::YELLOW
          ))
    @time_format = nil if File.exist?(File.join(Rails.root, 'Procfile.dev'))
  end

  def time
    "#{color}#{format_time(log.time)}#{color_map.clear}" if time_format
  end

  def message
    return unless log.message

    prefix = "#{color}--#{color_map.clear}"

    case log.level
    when :info
      message = log.message
      message = message&.rstrip if log.name == 'Rails' && message.starts_with?('Completed')
      if log.name == 'Rails' && message.starts_with?('Started')
        message = message.split('for')[0]
        puts '' if Rails.env.development?
      end
      if log.name == 'Rails' || log.name == 'ActionView::Base'
        "#{prefix} #{ANSI_GRAY}#{message}#{color_map.clear}"
      else
        "#{prefix} #{SemanticLogger::AnsiColors::WHITE}#{message}#{color_map.clear}"
      end
    when :warn
      "#{prefix} #{ANSI_REVERSED_WARNING}#{log.message}#{color_map.clear}"
    when :error, :fatal
      "#{prefix} #{ANSI_REVERSED_ERROR}#{log.message}#{color_map.clear}"
    else
      "#{prefix} #{color}#{log.message}#{color_map.clear}"
    end
  end

  def tags; end

  def process_info
    fname = file_name_and_line
    "#{color}[#{fname}]#{color_map.clear}" if fname
  end

  def exception
    return unless log.exception

    root_path = Rails.root.to_s
    backtrace = log.exception.backtrace.select do |line|
      line.starts_with?(root_path)
    end

    if backtrace.count.positive?
      "-- #{ANSI_REVERSED_ERROR}#{log.exception.class}#{color_map.clear} #{color}#{log.exception.message}#{color_map.clear}#{SemanticLogger::AnsiColors::WHITE}\n\t#{backtrace.join("\n\t")}#{color_map.clear}\n\n"
    else
      "-- #{ANSI_REVERSED_ERROR}#{log.exception.class}: #{log.exception.message}#{color_map.clear}\n\n"
    end
  end

  def call(log, logger)
    self.color = color_map[log.level]
    self.log = log
    self.logger = logger

    if @time_format
      [time, level, process_info, tags, named_tags, duration, name, message, payload, exception].compact.join(' ')
    else
      [tags, named_tags, duration, name, message, payload, exception].compact.join(' ')
    end
  end
end
