# frozen_string_literal: true

ONE_GIGA_BYTE = 10**9

namespace :user_options do
  desc 'Populate User Options from ldap'
  task populate_from_ldap: :environment do
    domains = Domain.all
    i = 1
    domains.each do |domain|
      puts "#{i}/#{domains.count} #{domain.name}"
      i += 1
      domain.users.each do |user|
        domain.config_modules.each do |cm|
          case cm.name
          when 'Zimbra'
            populate_zimbra(user, cm.id)
          when 'Nextcloud'
            populate_nextcloud(user, cm.id)
          else
            populate_others(user, cm.id)
          end
        end
      end
    end
  end

  desc 'Populate UserOption Domain_id'
  task populate_domain_id: :environment do
    puts 'Mise à jour du domain_id dans les UserOptions'
    user_options = UserOption.where(domain_id: nil)
    bar = ProgressBar.new(user_options.count, :counter, :bar, :percentage, :elapsed, :eta)
    user_options.each do |uo|
      bar.increment!
      user = User.find(uo.user_id)
      next unless user&.domain

      uo.domain_id = user.domain.id
      uo.save
    end
    puts 'Suppression des options sans utilisateurs'
    UserOption.where(domain_id: nil).destroy_all
  end
end

def populate_zimbra(user, config_module_id)
  domain_id = user.domain.id
  options = { enabled: user.zouritZimbraEmail ? 'true' : 'false',
              quota: user.zouritZimbraQuota,
              calendar: user.zouritZimbraCalendar ? 'true' : 'false',
              contact: user.zouritZimbraContact ? 'true' : 'false',
              task: user.zouritZimbraTask ? 'true' : 'false' }
  return unless options[:enabled] == 'true'

  options.each do |key, value|
    meta_option = MetaOption.user_option(config_module_id, key)
    user_option = UserOption.where(user_id: user.id, meta_option_id: meta_option.id)
                            .first_or_initialize
    user_option.update!(value:, domain_id:)
  end
end

def populate_nextcloud(user, config_module_id)
  domain_id = user.domain.id
  options = { enabled: user.zouritOwncloud ? 'true' : 'false',
              quota: user.zouritCloudQuota }
  return unless options[:enabled] == 'true'

  options.each do |key, value|
    meta_option = MetaOption.user_option(config_module_id, key)
    user_option = UserOption.where(user_id: user.id, meta_option_id: meta_option.id)
                            .first_or_initialize
    val = (key == :quota ? value / ONE_GIGA_BYTE : value)
    user_option.update!(value: val, domain_id:)
  end
end

def populate_others(user, config_module_id)
  domain_id = user.domain.id
  meta_option = MetaOption.user_option(config_module_id, 'enabled')
  user_option = UserOption.where(user_id: user.id, meta_option_id: meta_option.id)
                          .first_or_initialize
  user_option.update!(value: 'true', domain_id:)
end
