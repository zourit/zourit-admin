require 'csv'

namespace :alias do
  desc 'Populate aliases from Zimbra server'
  task populate: :environment do
    puts 'Populate aliases from Zimbra server'
    config_module = ConfigModule.where(name: 'Zimbra').first
    meta_option = MetaOption.where(key: 'aliases', config_module_id: config_module.id).first
    Instance.where(config_module_id: config_module.id).each do |zimbra|
      puts "populate from Zimbra instance <#{zimbra.name}>"
      user_aliases = Core::Zimbra::Main.zimbra_galiases zimbra.id
      user_aliases.each do |email, aliases|
        puts "found for email <#{email}> : #{aliases}"
        hash = {}
        aliases.each_with_index { |el, idx| hash.merge!({ idx => el.split('@').first }) }
        user = User.find(email)
        next unless user

        domain_id = user.domain.id
        user_option = UserOption.where(user_id: user.id, domain_id:,
                                       meta_option_id: meta_option.id).first_or_initialize
        user_option.update!(value: hash.to_json, domain_id:)
      end
    end
  end

  desc 'Populate aliases from CSV'
  task populate_from_csv: :environment do
    puts 'Populate aliases from CSV file'
    config_module = ConfigModule.where(name: 'Zimbra').first
    meta_option = MetaOption.where(key: 'aliases', config_module_id: config_module.id).first

    hash = {}
    CSV.foreach('tmp/aliases.csv', col_sep: '|') do |row|
      if hash[row[1]]
        count = hash[row[1]].count
        h_alias = Hash[count, row[0]]
        hash[row[1]].merge! h_alias
      else
        h_alias = Hash[0, row[0]]
        hash[row[1]] = h_alias
      end
    end

    i = 1
    count = hash.count
    hash.each do |email, aliases|
      puts "#{i}/#{count}"
      i += 1
      user = User.find(email)
      next unless user

      domain_id = user.domain.id
      user_option = UserOption.where(user_id: user.id, domain_id:,
                                     meta_option_id: meta_option.id).first_or_initialize
      user_option.update!(value: aliases.to_json, domain_id:)
    end
  end
end
