# frozen_string_literal: true

require 'securerandom'
require "#{Rails.root}/app/helpers/user_options_helper"
include UserOptionsHelper # rubocop:disable Style/MixinUsage

namespace :user do
  desc 'Reset user password for emergency (ie: suspicion of spam)'
  task :reset, [:email] => :environment do |_, args|
    email = args[:email]
    unless email
      puts 'an email is expected, please define one like so: `rails user:reset[user@zourit.net]`'
      exit 1
    end
    user = User.find_user_by_mail(email)
    unless user
      puts "no user found with expected email <#{email}>"
      exit 2
    end

    puts "Reset password for user <#{email}> ..."

    new_pass = nil
    loop do
      new_pass = SecureRandom.hex(3) + SecureRandom.hex(3).upcase
      break if check_password(new_pass)
    end

    user.password = new_pass
    user.password_confirmation = new_pass
    user.save!
    puts "a new password has been generated: #{new_pass}"
  end
end
