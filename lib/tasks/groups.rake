# frozen_string_literal: true

namespace :groups do
  desc 'Populate description with nextcloud instance name'
  task populate_description: :environment do
    Domain.all.each do |domain|
      domain.groups(with_users: true, with_tous: true).each do |group|
        puts "#{group.name} : #{group.description}"
        group.save if group.description.empty?
      end
    end
  end
end
