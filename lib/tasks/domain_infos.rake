namespace :domain_infos do
  desc 'Enable all module for all domains'
  task enable_all_modules: :environment do
    puts 'Please run this script carefully, specialy in production mode ! It will activate all modules for all domains with the last instance'
    puts 'Do you really want to run this script ? (y/n)'
    response = STDIN.gets.chomp
    if response == 'y'
      instances = Instance.all
      domains = Domain.all
      i = 1
      domains.each do |domain|
        puts "#{i}/#{domains.count} #{domain.name}"
        i += 1
        instances.each do |instance|
          domain_info = DomainInfo.where(domain_id: domain.id,
                                         config_module_id: instance.config_module_id)
                                  .first_or_initialize
          domain_info.update!(instance_id: instance.id)
        end
      end
    end
  end
end
