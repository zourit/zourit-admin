# frozen_string_literal: true

# List of all free Font Awesome icons class names v5.0.1
# https://gist.github.com/mohamdio/982653e3a8ae35f892f13c5ef0ef9b58

namespace :links do
  desc 'Rename font-awesome icons in the database (Example: film to fas fa-film)'
  task fix_icon_names: :environment do
    regex = /\Afa. fa-.*\z/
    Link.all.each do |link|
      if link.symbol.nil? || link.symbol.empty? || link.symbol.blank?
        link.symbol = 'fas fa-link'
        link.save(validate: false)
      elsif !link.symbol.match?(regex)
        link.symbol = "fas fa-#{link.symbol}"
        link.save(validate: false) # False because some URL are invalid
      end
    end
  end

  desc 'Add https to links that do not have it'
  task fix_url: :environment do
    regex = %r{\A(http|https)://.*\z}
    Link.all.each do |link|
      if !link.url.nil? && !link.url.empty? && !link.url.match?(regex)
        link.url = "https://#{link.url}"
        link.save(validate: false) # False because some data in the database are inconsistent
      end
    end
  end
end
