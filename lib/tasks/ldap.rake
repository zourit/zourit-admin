namespace :ldap do
  desc "TODO"
  task migrate: :environment do

	  dir = Rails.root.join('db', 'migrate_ldap')
	  Dir.glob(dir.join('*.rb')) do |rb_file|
	    file_array = File.basename(rb_file, ".rb").split("_")
	    number = file_array[0]
	    mod = file_array[1]
	    require File.absolute_path(rb_file)
		  include mod.constantize
		  change
	  end
  end

end
