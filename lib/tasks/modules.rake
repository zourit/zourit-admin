# frozen_string_literal: true

namespace :modules do
  desc 'Populate Meta Options from module configurations'
  task populate: :environment do
    directories = ['app/lib/core']
    directories.each do |dir|
      puts "=> integrating directory <#{dir}>"
      Dir.foreach(Rails.root.join(dir)) do |folder|
        file = File.join(Rails.root.join(dir, folder), 'config.yml')
        if File.exist?(file)
          puts "-> loading file <#{file}>"
          config = YAML.load_file(file)
          module_id = populate_config_module(config, dir.include?('core'))
          populate_meta_option(config, module_id, 'instance')
          populate_meta_option(config, module_id, 'domain')
          populate_meta_option(config, module_id, 'user')
        end
      end
    end
  end

  desc "Ajout d'une instance à tous les domaines"
  task add_instance_to_all_domains: :environment do
    add_instance_to_all_domains
  end

  desc "Activation d'un module pour tous les utilisateurs"
  task activate_module_for_all_users: :environment do
    activate_module_for_all_users
  end
end

def populate_config_module(config, core)
  config_name = config['name']
  config_module = ConfigModule.where(name: config_name).first \
                  ||
                  ConfigModule.new(name: config_name)

  config_module.update!(config.merge(core:))
  puts "\t Module <#{config_module.id}> entitled '#{config_name}' ... updated"
  config_module.id
end

def populate_meta_option(config, module_id, meta)
  meta_datas = config["#{meta}_datas"]
  return unless meta_datas

  meta_datas.each do |k, val|
    meta_option = MetaOption.where(config_module_id: module_id,
                                   field: meta,
                                   key: k).first \
                  ||
                  MetaOption.new(config_module_id: module_id,
                                 field: meta,
                                 key: k)
    meta_option.update(val)
    puts "\t * adds meta_option of type <#{meta}> named <#{k}:#{meta_option.field_type}>"
  end
end

def add_instance_to_all_domains
  puts "Ajout d'une instance à tous les domaines"
  puts '==='
  puts 'Veuillez choisir le service concerné :'
  configmodules = ConfigModule.all.order(:id)
  configmodules.each do |mod|
    puts "(#{mod.id}) #{mod.name}"
  end
  module_id = gets.chomp.to_i
  puts 'Veuillez choisir une instance :'
  Instance.where(config_module_id: module_id).each do |instance|
    puts "(#{instance.id}) #{instance.name}"
  end
  instance_id = gets.chomp.to_i
  instance = Instance.find(instance_id)
  return unless instance

  Domain.all.each do |domain|
    if DomainInfo.where(config_module_id: module_id, domain_id: domain.id).count.positive?
      puts "#{domain.name} : Service déjà actif pour ce domaine"
    elsif DomainInfo.create(config_module_id: module_id, domain_id: domain.id, instance_id:)
      puts "#{domain.name} : Ajout de l'instance pour ce domaine"
    else
      puts "#{domain.name} : Erreur lors de l'ajout de l'instance"
    end
  end
end

def activate_module_for_all_users
  puts "Ajout d'un service à tous les utilisateur"
  puts '==='
  puts 'Veuillez choisir le service concerné :'
  configmodules = ConfigModule.all.order(:id)
  configmodules.each do |mod|
    puts "(#{mod.id}) #{mod.name}"
  end
  module_id = gets.chomp.to_i
  cm = ConfigModule.find(module_id)
  return unless cm

  User.all.each do |user|
    cm.user_options_enabled.set_user_option(user, 'true')
    puts "Activation pour l'utilisateur #{user.email}"
  end
end
